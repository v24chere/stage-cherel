# Smart-Home Visualisation et Analyse

## Introduction

Le but du projet est de prédire les activités d'une personne à l'aide de capteurs domotiques (interupteur, débit, ouverture porte, présence sol ...).
Mais sans de caméra pour éviter d'être trop intrusif.

Une application importante est l'aide aux personnes en perte d'indépendence. Il est plus facile d'offrir une aide adaptée si l'on connait l'activité en cours. 
De plus, reconnaître l'analyse d'activités permet aussi de repérer des anomalies.

## Git clone projet Unity
Le projet Unity situé dans /visualisation possède des noms/chemins de fichier très long pour Windows assurez vous d'avoir les longpath activé :
 Win + R, tapez regedit, puis appuyez sur Entrée.    
 aller à HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\FileSystem    
 Double-cliquez sur LongPathsEnabled et changez la valeur à 1.     
 Redémarrer    
De même si vous avez des erreurs lors de git commit ou git add du type "file name too long" il faut lancer la commande suivante :   
git config --system core.longpaths true      
Pour modifier la configuration de git il faut les droits de modification sur le dossier (soit ouvrir la fenêtre de commande en administrateur soit aller modifier les droits de C:/Program Files/Git/etc/gitconfig ; clique droit propriétés onglet securité)

## Données de départ

Des données ont été colléctés sur 9 sujets au cours de 57 expériences dans un studio connecté pour un total de 519 activités avec 13 capteurs par activités en moyenne.

La figure ci-dessous détaille le nombre d'expériences par type d'activité dans notre base de donnée : 
![](screenshots/activity_par_type_clean.png)

Les données brutes regroupent chronologiquement des changements d'etat de capteurs.
Ces donnes ont été traitées et mises en forme vous pouvez retrouver dans le dossier IOT

![](screenshots/traitement_iot.png)

Dans les données traitées les changements d'état ont donc un nom de capteur clair, un nom d'etat unifiée (on/off ou open/close) et une activitée associée.
## Visualisation

Dans un premier temps un projet unity a été fait pour produire une visualisation mp4 de ces données .csv .
Les icônes/le sol s'allument quand le capteur correspondant s'active. 


![](screenshots/extrait_exemple_visualisation.png "extrait d'une visualisation")

Dans le frame exemple présenté dans la figure, vous pouvez voir que la lumière, le sol et le robinet de la salle de bain sont activés tandis que l'activité est "bathe"

Le temps est présent pour la synchronisation mais on n'as pas d'ancrage sur l'horaire du scénario (par exemple il y a des scenarios de matinée réalisé à 15h)

Pour plus de détails, voir le README du dossier visualisation.

## Analyse - Grandes Lignes
(-> regardez scripts/README.md pour des infos plus techniques sur les differents scripts)

L'input de départ est une liste de capteurs.

L'objecif est de découper cette liste en séquences qui correspondent chacune à une activité à déterminer. exemple: 

![](screenshots/liste_capteur_to_segment_etc.png)

Nous allons donc procéder en deux parties.
D'abord découper la liste de capteurs en segments qui devraient correspondre chacun à une seule activité.
Puis fournir ces segments à un classifieur qui doit associer à chaque segment une activité.

### Segmentation

Pour segmenter une liste de capteurs nous allons avoir une logique binaire :
Une suite de capteurs correspond à **UNE** ou **PLUSIEURS** activitée-s. Nous allons entrainer une IA à faire cette distinction.

Avec la liste de tout les capteurs d'une Experience l'IA va former une sous-liste qui grandit capteurs par capteurs. 
Dès que l'IA indique qu'elle observe **PLUSIEURS** activités, on forme un segment avec tout les capteurs, sauf le dernier qui correspond au début d'une nouvelle activité, 
On enregistre ce segment, on le retire de la sous liste.   
L'IA devrait retourner à la prediction **UNE** activitée on continuer d'ajouter depuis la liste en entrée et
Dès que l'IA indique qu'elle observe **PLUSIEURS** activités ...   
C'est le même procédé en boucle jusqu'à la fin de la liste en entrée.

![](screenshots/illustration_segmentation.png)

### Classification

Une fois que les segments de capteurs ont été produits, on les passe à une IA entrainée à classifier des segments de capteurs en activités.

## Analyse - preparations des données

### Segmentations

Chaque activitée peut être suivie par la majoritée des activitées ainsi pour apprendre à reconnaitre les changements d'activités on peut s'entrainer non pas uniquement sur les 
changements d'activitées faits lors des experiences mais n'importe quel combinaison de deux activités cohérentes. 

Avec nos 519 activités c'est plus de 200 000 combinaisons potentielles.    
La definition de coherent prise est assez basique : pas de paires avec deux fois la même activitées enter_home toujours en debut de paire et leave_home en fin.   
Mais on peut faire plus complexe.

En cas réel l'analyse de sequences ne commence pas toujours en debut d'activée (si la segementation n'as pas bien coupée precedement) j'ai donc coupée 0-2 capteurs au debut des paires de maniére aléatoires parmis ces combinaisons.

### Classification

Pour la classification c'est plus compliqué d'augmenter les données.

Comme notre segmentation ne sera pas, a priori, parfaite, il peut être judicieux de s'entraîner sur des segments auxquels nous coupons/ajoutons des bouts aux extrémités

## Analyse - résultats

### Segmentations

Pour la segmentation dans un premier temps on peut tester un cas plus simple. Cas où les données test sont similaires aux données train : des paires d'activités et leur suites de capteurs mis bout à bout.
Dans ce cas il n'y as pas de problème de décalage cumulé voire de changement d'activité non detecté. Mais même dans ce cas "faciles" les resultats sont mediocres et les IA place le changement d'activitée
environ 4 capteurs trop tôt ou tard (sans preference pour un côté ou l'autre) avec une moyenne de 13 capteurs par activitées ça fait 30% de données manquantes ou parasite en plus.

![](screenshots/gif_segmentation_simple.gif)

Le cas réel ou dynamique car les segments ne sont pas prédeterminés mais construits en fonction des réponses de l'IA. En pratique on attends 3 true d'affilée pour valider un changement d'activité ( et donc un reset de la liste de capteurs à observer).
Ce cas est plus dur à cause des décalages cumulés voir des non détections de changment d'activité qui viennent perturber les analyses suivantes.

![](screenshots/gif_segment_dynamique.gif)

Comme metriques j'ai regardée dans chaque segment produit l'activité majoritaire et à quel point elle l'était.
Ainsi

### Classification

De même, dans un premier temps, tester le cas simple consiste à prendre les segments parfaits obtenus grâce aux labels des données.
Dans ce cas nous avons une precision d'environ 80% aussi bien LSTM que SVM, les cas problèmatiques étant les activitées 
se resemblant fortement (cook/wash ; read/watch_tv).

Ces premiers tests mettent en lumière les points de détections difficiles : les activitées dans les mêmes pièces (read/watch_tv ; bathe/go_to_toilets ; cook/wash_dishes).
C'est particuliérement aggravée pour read qui est sous-representée dans la base de données par rapport à watch_tv.

Enfin le test final consiste à évaluer les segments fournis par l'IA. Ces segments n'étant pas parfaits, il faut donc développer une nouvelle métrique que simplement "pourcentage d'activitées correctement prédites".   
Nous allons regarder le recouvrement individuel des capteurs


