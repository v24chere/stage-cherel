# Scripts pour l'Analyse des données

## Getting started

j'utilise python 3.12.3.final.0
Si vous voulez faire comme moi ouvrez un project PyCharm en mode virtual env et clonez ce git dans un dossier à l'interieur. mais libre à vous d'utiliser l'environnement IDE python de votre choix.

Les valeurs par défauts des scripts sont pensées pour être lancées depuis le répertoire principale.
Penser à bien écrire vos paths si vous faites autrement.

(*** devant une fonction signifit qu'elle est prevue)
(xxx                   -                      en cours/buggé)
(+   fonctionne mais on veut ajouter des fonctionnalitées (par exemple d'autre modèle pour les ia)

input de départ : IOT : dossier de csv contenant une liste de capteurs
                (format date|                   capteur_name|   capteur_id|           capteur_etat|activity  ;
                ex: 2021-07-06 11:51:24.850237	camera_start	xaal-lab.hmi.basic.state	ON	   sleep)

## Traiter les données

    "python ./scripts/mise_en_forme/scrap_data -i path/to/ce/dossier -o path/to/output.csv"
-> lit le dossier et écrit les résultats dans output.csv sous le       (fonctionnalité de graph et petite statistique partiellement implementé)
    format experience | nombre_de_capteurs | activité | capteur1 |capteur2 | ... 
    ex s1_Ep2.csv	6	sleep	presure_bed_OFF	presure_bed_ON	floor_bedroom_ON	floor_bedroom_OFF	floor_bedroom_ON	presure_bed_OFF
    il y a quelques options de statistique / graphe à décommenter si on le souhaite

/!\ j'ai enlevé la nuance entre eat lunch/dinner/breakfast, idem avec cook , j'ai retiré take medecine car c'est unique et j'ai combiné sleep et sleep_in_bed

cet output peut être mis en forme par:
    
    +"python ./scripts/mise_en_forme/form_data -i path/to/scrap_result -o path/to/output.csv -n 3 -x 20"
-> vas filtrer les activités avec 2 ou moins capteurs et reduire à 20 les activitées avec plus de 20 activitées

    "python ./scripts/mise_en_forme/form_pair_data -i path/to/scrap_result -o path/to/output.csv -n 3 -x 40 -m 10"
-> vas filtrer les activités avec 2 ou moins capteurs et reduire à 40 les activitées avec plus de 20 activitées
puis regarder les activitées consecutives et enumerer proggressivement les capteurs qui s'active en labelisant quand on contient les capteurs d'une ou 2 activitées:

    ex : s1_Ep21.csv    1	go_to_toilets	None	wc_fush_ON
         s1_Ep21.csv    2	go_to_toilets	None	wc_fush_ON	wc_fush_OFF
         s1_Ep21.csv    3	go_to_toilets	None	wc_fush_ON	wc_fush_OFF	tap_batheroom_ON
         s1_Ep21.csv    4	go_to_toilets	None	wc_fush_ON	wc_fush_OFF	tap_batheroom_ON	tap_batheroom_OFF
         s1_Ep21.csv    5	go_to_toilets	sleep_in_bed	wc_fush_ON	wc_fush_OFF	tap_batheroom_ON	tap_batheroom_OFF	light_bedroom_ON

## Modèle IA

Enfin ces données mise en formes vont être utilisées respectivement par
    
    +"python ./scripts/ia_classifier_SVM_data -i path/to/form_result -o path/to/output -m nom_du_model -s num_du_sujet_a_test"
vas entrainer une IA (pour l'instant que SVM) sur tout le monde sauf num_du_sujet_a_test
puis faire des prédictions qu'il va enregistrer dans path/to/output/classe_s{num_du_sujet_a_test}_{accuracy arondi au % pres}.csv

    ***"python ./scripts/ia_segment_data -i path/to/form_pair_result -o path/to/output -m nom_du_model"

## Traitement resultat

    "python ./scripts/fuse_conf_mat_data.py 












note :
