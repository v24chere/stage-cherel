import argparse
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import random
from sklearn.feature_extraction.text import TfidfVectorizer
#from sklearn.model_selection import train_test_split
from sklearn import svm
from sklearn.metrics import accuracy_score

from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import confusion_matrix
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
import torch
from torch.utils.data import DataLoader, Dataset

import torch.nn as nn
import torch.optim as optim
import ast
from pathlib import Path

class SensorDataset(Dataset):
    def __init__(self, X, y):
        self.X = X
        self.y = y

    def __len__(self):
        return len(self.X)

    def __getitem__(self, idx):
        return self.X[idx], self.y[idx]
class LSTMModel(nn.Module):
    def __init__(self, vocab_size, embedding_dim, hidden_dim, output_dim):
        super(LSTMModel, self).__init__()
        self.embedding = nn.Embedding(vocab_size, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim, hidden_dim, batch_first=True)
        self.fc = nn.Linear(hidden_dim, output_dim)

    def forward(self, x):
        embedded = self.embedding(x)
        _, (hidden, _) = self.lstm(embedded)
        output = self.fc(hidden.squeeze(0))
        return output

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui s'entraine sur le fichier en args.input sauf pour args.sujet qu'il test")
    parser.add_argument('-i', "--input",default="./output/form_test.csv", type=str,
                        help="path du fichier produit par scrap")
    parser.add_argument('-o', "--output",default="./output/LSTM_res/", type=str,
                        help="path/name du fichier a produire")
    parser.add_argument('-x', "--max", default=20, type=int,
                        help="max de capteurs par activity inclus, par defaut 20 ou moins, il faut le même que celui utilisé pour form_data")
    parser.add_argument('-e', "--epoch", default=10, type=int,
                        help="nombre d'epoch")
    parser.add_argument('-s', "--subject", default="10", type=str,
                        help="subject qu'on garde comme test dans la repartition de donnée")
    parser.add_argument('-d', "--dynam", default="", type=str,
                        help="dossier des segments dynamique")

    # parser.add_argument("--replace",type=bool,default=False, help="si on change le csv d'entrée au lieu de creer une copie dans output")

    args = parser.parse_args()

    #scrap_header =["exp","num_sensors","activity"]+[f"sensor{i}" for i in range(0,args.max)]
    X_train = []
    Y_train = []
    ID_train=[]
    X_test=[]
    Y_test =[]
    ID_test=[]
    dfb = pd.read_csv(args.input,delimiter="\t",header=0)

    start=0
    num_ligne=dfb["exp"].size

    for i in range(num_ligne):
        exp=dfb['exp'][i]
        activity=dfb['activity'][i]
        num_cap=int(dfb["num_sensors"][i])
        subject=exp.split('_Ep')[0]
        if subject == ("s"+args.subject):
            x_out = X_test
            y_out = Y_test
            id_out= ID_test
        else:
            x_out = X_train
            y_out = Y_train
            id_out = ID_train

        list_sensor=[]
        for cap in range(num_cap):
            list_sensor.append(dfb[f"sensors{cap}"][i])
        x_out.append(list_sensor)
        y_out.append(activity)
        id_out.append(exp)

    label_encoder = LabelEncoder()
    Y_train_e = label_encoder.fit_transform(Y_train)
    Y_test_e = label_encoder.transform(Y_test)


    X_train_str=[" ".join(cap) for cap in X_train]
    X_test_str = [" ".join(cap) for cap in X_test]

    tokenizer = Tokenizer(filters='')
    tokenizer.fit_on_texts(X_train + X_test)
    X_train_seq = tokenizer.texts_to_sequences(X_train_str)
    X_test_seq = tokenizer.texts_to_sequences(X_test_str)

    X_train_pad = pad_sequences(X_train_seq, padding='post')
    X_test_pad = pad_sequences(X_test_seq, padding='post')

    X_train_tensor = torch.tensor(X_train_pad, dtype=torch.long)
    y_train_tensor = torch.tensor(Y_train_e, dtype=torch.long)
    X_test_tensor = torch.tensor(X_test_pad, dtype=torch.long)
    y_test_tensor = torch.tensor(Y_test_e, dtype=torch.long)

    train_dataset = SensorDataset(X_train_tensor, y_train_tensor)
    test_dataset = SensorDataset(X_test_tensor, y_test_tensor)

    train_loader = DataLoader(train_dataset, batch_size=32, shuffle=True)
    test_loader = DataLoader(test_dataset, batch_size=32, shuffle=False)

    vocab_size = len(tokenizer.word_index) + 1
    embedding_dim = 128
    hidden_dim = 256
    output_dim = len(label_encoder.classes_)

    model = LSTMModel(vocab_size, embedding_dim, hidden_dim, output_dim)

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model.to(device)

    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=0.001)

    num_epochs = args.epoch

    for epoch in range(num_epochs):
        model.train()
        for X_batch, y_batch in train_loader:
            X_batch, y_batch = X_batch.to(device), y_batch.to(device)

            optimizer.zero_grad()
            outputs = model(X_batch)
            loss = criterion(outputs, y_batch)
            loss.backward()
            optimizer.step()

        print(f'Epoch [{epoch + 1}/{num_epochs}], Loss: {loss.item():.4f}')

    model.eval()
    results = []
    all_preds = []
    all_labels = []
    correct = 0
    total = 0
    with torch.no_grad():
        for X_batch, y_batch in test_loader:

            X_batch, y_batch = X_batch.to(device), y_batch.to(device)
            outputs = model(X_batch)
            _, predicted = torch.max(outputs.data, 1)
            total += y_batch.size(0)
            correct += (predicted == y_batch).sum().item()

            all_preds.extend(predicted.cpu().numpy())
            all_labels.extend(y_batch.cpu().numpy())

            for i in range(len(X_batch)):
                sensor_sequence = ' '.join(tokenizer.sequences_to_texts([X_batch[i].cpu().numpy()])[0].split())
                results.append({
                    'sensors': sensor_sequence,
                    'predictions': label_encoder.inverse_transform([predicted[i].cpu().numpy()])[0],
                    'activity': label_encoder.inverse_transform([y_batch[i].cpu().numpy()])[0]
                })


    os.makedirs(args.output +f"epos{num_epochs}",exist_ok=True)

    data = pd.DataFrame(results)
    data.to_csv(args.output +f"epos{num_epochs}/"+ "classe_s" + args.subject + f"_epos{num_epochs}_{100 * correct / total:.0f}.csv",
                sep='\t', encoding='utf-8', header=False, index=False)

    print(f'Accuracy: {100 * correct / total:.2f}%')
    #cm = confusion_matrix(all_labels, all_preds)
    #print(label_encoder.classes_)
    #print(cm)
    #cm_df = pd.DataFrame(cm, index=label_encoder.classes_, columns=label_encoder.classes_)
    #cm_df.to_csv(args.output+f"epos{num_epochs}/"+"classe_s"+args.subject+f"_epos{num_epochs}_confus_mat.csv")

    if args.dynam != "":
        dyn = pd.read_csv(args.dynam, delimiter="\t", header=0)
        X_test_dyn = []
        Y_test_dyn = []
        ID_test_dyn = []
        num_ligne = dyn["exp"].size

        for i in range(num_ligne):
            exp = dyn["exp"][i]
            activities = dyn['activities'][i]
            num_cap = int(dyn["num_sensors"][i])
            subject = exp.split('_Ep')[0]
            if subject == ("s" + args.subject):
                list_sensor = []
                for cap in range(num_cap):
                    list_sensor.append(dyn[f"sensors{cap}"][i])
                X_test_dyn.append(list_sensor)
                Y_test_dyn.append(exp+"_and_"+activities)
                #ID_test_dyn.append(exp)

        #Y_test_dyn_e = label_encoder.transform(Y_test_dyn)

        X_test_dyn_str = [" ".join(cap) for cap in X_test_dyn]

        X_test_dyn_seq = tokenizer.texts_to_sequences(X_test_dyn_str)

        X_test_dyn_pad = pad_sequences(X_test_dyn_seq, padding='post')

        X_test_dyn_tensor = torch.tensor(X_test_dyn_pad, dtype=torch.long)
        #y_test_dyn_tensor = torch.tensor(Y_test_dyn_e, dtype=torch.long)

        test_dyn_dataset = SensorDataset(X_test_dyn_tensor, Y_test_dyn)

        test_dyn_loader = DataLoader(test_dyn_dataset, batch_size=32, shuffle=False)

        results_dyn = [["exp","num_sensors","predictions","precision","activities"]+[f"sensors{i}" for i in range(50)]]

        correct_total=0
        total_cap=0
        precision_moyen=0
        num_above_50=0
        num_above_0 = 0
        total_segment=0

        with torch.no_grad():
            for X_batch, y_batch in test_dyn_loader:

                X_batch= X_batch.to(device)
                outputs = model(X_batch)
                _, predicted = torch.max(outputs.data, 1)

                for i in range(len(X_batch)):
                    sensor_sequence = (tokenizer.sequences_to_texts([X_batch[i].cpu().numpy()])[0].split())
                    prediction=label_encoder.inverse_transform([predicted[i].cpu().numpy()])[0]
                    list_act=ast.literal_eval(y_batch[i].split("_and_")[1])
                    correct=0
                    for j in range(len(list_act)):
                        if list_act[j]==prediction:
                            correct+=1
                    precision=correct/len(list_act)
                    results_dyn.append([y_batch[i].split("_and_")[0],len(sensor_sequence),prediction,precision,list_act]+[sensor for sensor in sensor_sequence])
                    correct_total+=correct
                    total_cap+=len(sensor_sequence)
                    precision_moyen+=precision
                    total_segment+=1
                    if precision>=0.5:
                        num_above_50+=1
                    if precision > 0.0:
                        num_above_0 += 1

        print(f'Recouvrement global correct: {100 * correct_total / total_cap:.2f}%')
        print(f'Recouvrement moyen correct des segments: {100 * precision_moyen / total_segment:.2f}%')
        print(f'Pourcentage de segment recouvert correctement à au moins 50%: {100 * num_above_50 / total_segment:.2f}%')
        print(f'Pourcentage de segment recouvert correctement à plus de 0%: {100 * num_above_0 / total_segment:.2f}%')

        #print(results_dyn)
        os.makedirs(args.output + f"dyn/epos{num_epochs}", exist_ok=True)
        data_dyn = pd.DataFrame(results_dyn)
        data_dyn.to_csv(
            args.output + f"dyn/epos{num_epochs}/" + "dyn_classe_s" + args.subject + f"_epos{num_epochs}.csv",
            sep='\t', encoding='utf-8', header=False, index=False)

        #print(f'Accuracy dynamique: {100 * correct_dyn / total_dyn:.2f}%')
        #cm_dyn = confusion_matrix(all_labels_dyn, all_preds_dyn)
        #print(label_encoder.classes_)

        #cm_df_dyn = pd.DataFrame(cm_dyn, index=label_encoder.classes_, columns=label_encoder.classes_)
        #cm_df_dyn.to_csv(args.output + f"epos{num_epochs}/" + "dyn_classe_s" + args.subject + f"_epos{num_epochs}_confus_mat.csv")

