import argparse
import pandas as pd
# import numpy as np
import os
import matplotlib.pyplot as plt
import random
from sklearn.feature_extraction.text import TfidfVectorizer
#from sklearn.model_selection import train_test_split
from sklearn import svm
from sklearn.metrics import accuracy_score



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui classifie a l'aide d'une SVM")
    parser.add_argument('-i', "--input",default="./output/form_test.csv", type=str,
                        help="path du fichier produit par scrap")
    parser.add_argument('-o', "--output",default="./output/SVM_res/", type=str,
                        help="path/name du fichier a produire")
    parser.add_argument('-x', "--max", default=20, type=int,
                        help="max de capteurs par activity inclus, par defaut 20 ou moins, il faut le même que celui utilisé pour form_data")
    parser.add_argument('-e', "--epoch", default=10, type=int,
                        help="nombre d'epoch n'as pas d'inpact pour la SVM mais est present pour que les arguments soit identiques à travers les differents modeles")

    parser.add_argument('-s', "--subject", default="10", type=str,
                        help="subject qu'on garde comme test dans la repartition de donnée")
    parser.add_argument('-d', "--dynam", default="", type=str,
                        help="dossier de segment fabriquée a tester")

    # parser.add_argument("--replace",type=bool,default=False, help="si on change le csv d'entrée au lieu de creer une copie dans output")

    args = parser.parse_args()

    #scrap_header =["exp","num_sensors","activity"]+[f"sensor{i}" for i in range(0,156)]
    X_train = []
    Y_train = []
    ID_train=[]
    X_test=[]
    Y_test =[]
    ID_test=[]
    dfb = pd.read_csv(args.input,delimiter="\t",header=0)

    num_ligne=dfb["exp"].size

    for i in range(num_ligne):
        exp=dfb['exp'][i]
        activity=dfb['activity'][i]
        num_cap=int(dfb["num_sensors"][i])
        subject=exp.split('_Ep')[0]
        if subject == ("s"+args.subject):
            x_out = X_test
            y_out = Y_test
            id_out= ID_test
        else:
            x_out = X_train
            y_out = Y_train
            id_out = ID_train

        list_sensor=[]
        for cap in range(num_cap):
            list_sensor.append(dfb[f"sensors{cap}"][i])
        x_out.append(list_sensor)
        y_out.append(activity)
        id_out.append(exp)

    X_train_str=[" ".join(cap) for cap in X_train]
    X_test_str = [" ".join(cap) for cap in X_test]
    vectorizer = TfidfVectorizer()
    X_train_v = vectorizer.fit_transform(X_train_str)
    X_test_v = vectorizer.transform(X_test_str)

    clf = svm.SVC(kernel='linear', C=1.0)
    clf.fit(X_train_v,Y_train)

    Y_pred= clf.predict(X_test_v)

    accuracy = accuracy_score(Y_test, Y_pred)
    print(f'Accuracy: {accuracy * 100:.2f}%')

    #print("nombre d'activité total "+ str(act_total))
    #print("nombre de capteur par activité moyen "+str(cap_total/act_total))

    #lst=[[1,2],["test1","test2"]]
    results=[[ID_test[i],Y_pred[i],Y_test[i]]+X_test[i] for i in range(len(ID_test))]
    results=[["exp","prediction","activité"]+[f"sensor{i}" for i in range(0,args.max)]]+results
    data=pd.DataFrame(results)
    data.to_csv(args.output+"classe_s"+args.subject+f"_{accuracy * 100:.0f}.csv", sep='\t', encoding='utf-8', header=False, index=False)


    if args.dynam != "":
        dyn = pd.read_csv(args.dynam, delimiter="\t", header=0)
        X_test_dyn = []
        Y_test_dyn = []
        ID_test_dyn = []
        num_ligne = dyn["exp"].size

        for i in range(num_ligne):
            exp = dyn["exp"][i]
            activity = dyn['activity'][i]
            num_cap = int(dyn["num_sensors"][i])
            subject = exp.split('_Ep')[0]
            if subject == ("s" + args.subject):
                list_sensor = []
                for cap in range(num_cap):
                    list_sensor.append(dyn[f"sensors{cap}"][i])
                X_test_dyn.append(list_sensor)
                Y_test_dyn.append(activity)
                ID_test_dyn.append(exp)
        #print(X_test_dyn)
        X_test_dyn_str = [" ".join(cap) for cap in X_test_dyn]
        #vectorizer = TfidfVectorizer()
        #print(X_test_dyn_str)
        X_test_dyn_v = vectorizer.transform(X_test_dyn_str)

        Y_pred_dyn = clf.predict(X_test_dyn_v)

        accuracy = accuracy_score(Y_test_dyn, Y_pred_dyn)
        print(f'Accuracy dynamique: {accuracy * 100:.2f}%')

        results = [[ID_test_dyn[i], Y_pred_dyn[i], Y_test_dyn[i]] + X_test_dyn[i] for i in range(len(ID_test_dyn))]
        results = [["exp", "prediction", "activité"] + [f"sensor{i}" for i in range(0, args.max)]] + results
        data = pd.DataFrame(results)
        data.to_csv(args.output + "dynam/classe_dyn_s" + args.subject + f"_{accuracy * 100:.0f}.csv", sep='\t', encoding='utf-8',
                    header=False, index=False)