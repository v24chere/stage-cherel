import argparse
import pandas as pd
import numpy as np
import os
import torch
import torch.nn as nn
import torch.optim as optim
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from torch.utils.data import Dataset, DataLoader

from pathlib import Path

#Manipuler les Dataset
class SensorDataset(Dataset):
    def __init__(self, X, y):
        self.X = X
        self.y = y

    def __len__(self):
        return len(self.X)

    def __getitem__(self, idx):
        return self.X[idx], self.y[idx]


# Modèle LSTM
class LSTMModel(nn.Module):
    def __init__(self, vocab_size, embedding_dim, hidden_dim):
        super(LSTMModel, self).__init__()
        self.embedding = nn.Embedding(vocab_size, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim, hidden_dim, batch_first=True)
        self.fc = nn.Linear(hidden_dim, 1)
        self.sigmoid = nn.Sigmoid()

    def forward(self, x):
        embedded = self.embedding(x)
        lstm_out, (hidden, cell) = self.lstm(embedded)
        output = self.fc(hidden[-1])
        output = self.sigmoid(output)
        return output,lstm_out


# Fonction pour prédire dynamiquement
def predict_sequence(model, tokenizer, sequence):
    model.eval()
    sequence = tokenizer.texts_to_sequences([sequence])
    sequence = pad_sequences(sequence, maxlen=X_train_pad.shape[1], padding='post')
    sequence_tensor = torch.tensor(sequence, dtype=torch.long).to(device)

    with torch.no_grad():
        output, lstm_out = model(sequence_tensor)
        confidence = output.item()
        prediction = confidence >= 0.5
        lstm_activations = lstm_out.squeeze().cpu().numpy()
        return prediction, confidence, lstm_activations


# Prédiction dynamique
def dynamic_prediction(model, tokenizer, sequence):
    model.eval()
    detected_segments = []
    current_sequence = []
    results =[]
    res_confidence=[]
    res_activ=[]

    #tokens = sequence.split()
    for capt in sequence:
        current_sequence.append(capt)
        current_text = ' '.join(current_sequence)
        prediction,confidence,lstm_activ = predict_sequence(model, tokenizer, current_text)
        if prediction:  # Detected more than one activity
            if len(results)>=2 and results[-1]==True and results[-2]==True:
                detected_segments.append(current_sequence[:-3])  # Add sequence before the True prediction
                current_sequence = [current_sequence[-2],current_sequence[-1],capt]  # Reset the sequence starting with the current token
                #results[-2] = True
                results[-1] = False
                results.append(False)

            else:
                results.append(True)
        else:
            if len(results)>=2:
                results[-2] = False
                results[-1] = False
            results.append(False)
        res_confidence.append(confidence)
        res_activ.append(lstm_activ)



    if current_sequence:  # Add any remaining sequence
        detected_segments.append(' '.join(current_sequence))

    return detected_segments,results,res_confidence,res_activ

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui prend de données mis en forme s'entraine puis segmente")
    parser.add_argument('-i', "--input", default="./output/form_pair_test", type=str,
                        help="path du fichier de pairs normals produit par form_pair_data. Mettre \"NO\" pour ne pas s'entrainer sur les pairs normals")
    parser.add_argument('-c', "--combi", default="./output/form_pair_noS10_c3_m6_b3.csv", type=str,
                        help="path du fichier de pairs combinées produit par form_pair_combi_data. Mettre \"NO\" pour ne pas s'entrainer sur les pairs combinées")
    parser.add_argument('-o', "--output", default="./output/LSTM_segment_res/", type=str,
                        help="path/name du dossier ou mettre les resultats.")
    #parser.add_argument('-x', "--max", default=20, type=int,
    #                    help="max de capteurs par activity inclus, par defaut 20 ou moins, il faut le même que celui utilisé pour form_data")
    parser.add_argument('-e', "--epoch", default=10, type=int,
                        help="nombre d'epoch")

    parser.add_argument('-s', "--subject", default="10", type=str,
                        help="subject qu'on garde comme test dans la repartition de donnée")
    parser.add_argument('-d', "--dynam", default="./output/form_test.csv", type=str,
                        help="dossier à partir duquel on vas reformer la liste continue des capteurs pour la tester. normalement produit par form_data")

    # parser.add_argument("--replace",type=bool,default=False, help="si on change le csv d'entrée au lieu de creer une copie dans output")

    args = parser.parse_args()

    X_train = []
    Y_train = []
    ID_train = []

    X_test_dyn = []
    Y_test_dyn = []
    ID_test_dyn = []


    if args.input != "" and args.input != "NO" :
        dfb = pd.read_csv(args.input, delimiter="\t", header=0)
        num_ligne = dfb["num_sensors"].size
        for i in range(num_ligne):
            exp = dfb["exp"][i]
            subject=exp.split("_")[0]
            if subject!="s"+args.subject:
                activity = dfb["activity2"][i]
                num_cap = int(dfb["num_sensors"][i])
                subject = exp.split('_Ep')[0]
                list_sensor = []
                for cap in range(num_cap):
                    list_sensor.append(dfb[f"sensors{cap}"][i])
                X_train.append(list_sensor)
                Y_train.append(int(activity != "Noact"))
                ID_train.append(exp)

    if args.combi != "" and args.combi != "NO":
        dfb = pd.read_csv(args.combi, delimiter="\t", header=0)
        num_ligne = dfb["num_sensors"].size
        for i in range(num_ligne):
            exp = dfb["exp"][i]
            activity = dfb["activity2"][i]
            num_cap = int(dfb["num_sensors"][i])
            subject = exp.split('_Ep')[0]
            list_sensor = []
            for cap in range(num_cap):
                list_sensor.append(dfb[f"sensors{cap}"][i])
            X_train.append(list_sensor)
            Y_train.append(int(activity != "Noact"))
            ID_train.append(exp)


    if args.dynam != "":
        dyn = pd.read_csv(args.dynam, delimiter="\t", header=0)
        num_ligne = dyn["exp"].size
        for i in range(num_ligne):
            exp = dyn["exp"][i]
            activity = dyn["activity"][i]
            num_cap = int(dyn["num_sensors"][i])
            subject = exp.split('_Ep')[0]
            if subject == ("s" + args.subject):
                for cap in range(num_cap):
                    X_test_dyn.append(dyn[f"sensors{cap}"][i])
                    Y_test_dyn.append(activity)
                    ID_test_dyn.append(exp)

    X_train_str=[" ".join(caps) for caps in X_train]
    #X_test_str = [" ".join(caps) for caps in X_test]

    tokenizer = Tokenizer(filters='')
    tokenizer.fit_on_texts(X_train) # + X_test)
    X_train_seq = tokenizer.texts_to_sequences(X_train_str)
    #X_test_seq = tokenizer.texts_to_sequences(X_test_str)


    #print("Exemples de séquences après token :")
    #print(X_train_seq)

    X_train_pad = pad_sequences(X_train_seq, padding='post')
    #X_test_pad = pad_sequences(X_test_seq, padding='post')
    #X_test_dyn_pad = pad_sequences(X_test_dyn_seq, padding='post')

    #print("Exemples de séquences après padding :")
    #print(X_train_pad)

    X_train_tensor = torch.tensor(X_train_pad, dtype=torch.long)
    y_train_tensor = torch.tensor(Y_train, dtype=torch.float32)
    #X_test_tensor = torch.tensor(X_test_pad, dtype=torch.long)
    #y_test_tensor = torch.tensor(Y_test_e, dtype=torch.long)
    #X_test_dyn_tensor = torch.tensor(X_test_dyn_pad, dtype=torch.long)


    train_dataset = SensorDataset(X_train_tensor, y_train_tensor)
    #test_dataset = SensorDataset(X_test_tensor, y_test_tensor)


    train_loader = DataLoader(train_dataset, batch_size=32, shuffle=True)
    #test_loader = DataLoader(test_dataset, batch_size=32, shuffle=False)


    vocab_size = len(tokenizer.word_index) + 1
    embedding_dim = 128
    hidden_dim = 256
    #output_dim = len(label_encoder.classes_)

    model = LSTMModel(vocab_size, embedding_dim, hidden_dim)

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model.to(device)

    criterion = nn.BCELoss()
    optimizer = optim.Adam(model.parameters(), lr=0.001)

    num_epochs = args.epoch

    for epoch in range(num_epochs):
        model.train()
        for X_batch, y_batch in train_loader:
            X_batch, y_batch = X_batch.to(device), y_batch.to(device)

            optimizer.zero_grad()
            output,_ = model(X_batch)
            outputs=output.squeeze()
            loss = criterion(outputs, y_batch)
            loss.backward()
            optimizer.step()

        print(f'Epoch [{epoch + 1}/{num_epochs}], Loss: {loss.item():.4f}')

    #cm = confusion_matrix(all_labels, all_preds)
    #print(label_encoder.classes_)

    #cm_df = pd.DataFrame(cm, index=label_encoder.classes_, columns=label_encoder.classes_)
    #cm_df.to_csv(args.output+f"epos{num_epochs}/"+"s"+args.subject+f"_epos{num_epochs}_segment_confus_mat.csv")

######section dynamique
    #Y_test_dyn_e = label_encoder.transform(Y_test_dyn)
    #X_test_dyn_str = [" ".join(cap) for cap in X_test_dyn]
    #X_test_dyn_seq = tokenizer.texts_to_sequences(X_test_dyn_str)
    #y_test_dyn_tensor = torch.tensor(Y_test_dyn_e, dtype=torch.long)
    #test_dyn_dataset = SensorDataset(X_test_dyn_tensor, y_test_dyn_tensor)
    #test_dyn_loader = DataLoader(test_dyn_dataset, batch_size=32, shuffle=False)


    predicted_segments,bool_predict,conf_predict,activ_predict = dynamic_prediction(model, tokenizer, X_test_dyn)

    #print("Segments détectés :")
    #print(predicted_segments)
    #print(bool_predict)

    os.makedirs(args.output + f"epos{num_epochs}", exist_ok=True)
    results_dyn = [[ID_test_dyn[i],bool_predict[i],conf_predict[i],activ_predict[i],Y_test_dyn[i],X_test_dyn[i]]for i in range(len(Y_test_dyn))]
    results_dyn = [["exp","predi_new_act","confidence","activation","activity", "sensor"]] + results_dyn
    data = pd.DataFrame(results_dyn)
    spe=""
    if args.input != ""and args.input != "NO":
        spe+="_base"
    if args.combi != ""and args.combi != "NO":
        spe+="_combi"
    data.to_csv(
        args.output + f"epos{num_epochs}/" + "raw_segment_s" + args.subject +".csv",
        sep='\t', encoding='utf-8', header=False, index=False)
