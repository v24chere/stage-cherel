import argparse
import pandas as pd
# import numpy as np
import os
import matplotlib.pyplot as plt
import random
from sklearn.feature_extraction.text import TfidfVectorizer
#from sklearn.model_selection import train_test_split
from sklearn import svm
from sklearn.metrics import accuracy_score



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui lit la base IOT et donne des info dessus")
    parser.add_argument('-i', "--input",default="./output/form_pair_test.csv", type=str,
                        help="path du fichier produit par scrap")
    parser.add_argument('-o', "--output",default="./output/SVM_segment_res/", type=str,
                        help="path/name du fichier a produire")
    parser.add_argument('-x', "--max", default=60, type=int,
                        help="max de capteurs par activity inclus, par defaut 20 ou moins, il faut le même que celui utilisé pour form_data")
    parser.add_argument('-e', "--epoch", default=10, type=int,
                        help="nombre d'epoch")

    parser.add_argument('-s', "--subject", default="10", type=str,
                        help="subject qu'on garde comme test dans la repartition de donnée")
    parser.add_argument('-d', "--dynam", default="", type=str,
                        help="subject qu'on garde comme test dans la repartition de donnée")

    # parser.add_argument("--replace",type=bool,default=False, help="si on change le csv d'entrée au lieu de creer une copie dans output")

    args = parser.parse_args()

    #scrap_header =["exp","num_sensors","activity1","activity2"]+[f"sensor{i}" for i in range(0,args.max)]
    X_train = []
    Y_train = []
    ID_train=[]
    X_test=[]
    Y_test =[]
    ID_test=[]
    X_test_dyn = []
    Y_test_dyn = []
    ID_test_dyn = []
    dfb = pd.read_csv(args.input,delimiter="\t",header=0)
    #print(dfb)
    start=0
    num_ligne=dfb["exp"].size
    #print(num_ligne)
    compteur=0
    for i in range(num_ligne):
        #print(dfb["exp"][i])
        #print(dfb["activity2"][i])
        #print(dfb["num_sensors"][i])
        exp=dfb["exp"][i]
        activity=dfb["activity2"][i]
        num_cap=int(dfb["num_sensors"][i])
        subject=exp.split('_Ep')[0]
        #print(f"{exp}  {activity}  {num_cap}")
        if subject == ("s"+args.subject):
            x_out = X_test
            y_out = Y_test
            id_out= ID_test
        else:
            x_out = X_train
            y_out = Y_train
            id_out = ID_train

        list_sensor=[]
        for cap in range(num_cap):
            list_sensor.append(dfb[f"sensors{cap}"][i])
        #print(list_sensor)
        x_out.append(list_sensor)
        y_out.append(activity!="Noact")
        id_out.append(exp)

        if(args.dynam=="" and activity=="Noact" and subject == ("s" + args.subject)):
            X_test_dyn.append(list_sensor[-1])
            Y_test_dyn.append(dfb["activity1"][i])
            ID_test_dyn.append(exp)
            #compteur+=1
        #if i+1<num_ligne: # on  récupere que les derniere lignes (ie a la prochaine ligne c'est plus la même activity mais c'est tjr la même exp)
        #    if subject == ("s" + args.subject) and activity=="Noact" and dfb["activity2"][i+1]!="Noact":
        #        X_test_dyn.append(list_sensor)
        #        Y_test_dyn.append(compteur)
        #        ID_test_dyn.append(exp)
        #        compteur=0
    #print(X_test_dyn)

    X_train_str=[" ".join(cap) for cap in X_train]
    X_test_str = [" ".join(cap) for cap in X_test]
    num_test=[len(cap) for cap in X_test]
    vectorizer = TfidfVectorizer()
    X_train_v = vectorizer.fit_transform(X_train_str)
    X_test_v = vectorizer.transform(X_test_str)

    clf = svm.SVC(kernel='linear', C=1.0)
    clf.fit(X_train_v,Y_train)

    Y_pred= clf.predict(X_test_v)
    accuracy = accuracy_score(Y_test, Y_pred)
    print(f'Accuracy: {accuracy * 100:.2f}%')

    current_sensors = []
    predictions = []

    X_test_dyn_res=[]
    num_test_dyn=[]
    #print(len(X_test_dyn))
    #print(len(Y_test_dyn))
    compteur=0
    last_exp=ID_test_dyn[compteur]
    for sensor in X_test_dyn:
        exp=ID_test_dyn[compteur]

        current_sensors.append(sensor)
        combined = ' '.join(current_sensors)
        combined_vect=vectorizer.transform([combined])
        predicted_multiple_act = clf.predict(combined_vect)[0]
        X_test_dyn_res.append(current_sensors.copy())
        num_test_dyn.append(len(current_sensors))
        if exp==last_exp:
            if predicted_multiple_act :  #on valide un bloc quand on a 3 true d'affilée, et on ne garde que le premier True
                if predictions[-1] and predictions[-2]:
                    predictions[-1]=False
                    predicted_multiple_act=False
                    num_test_dyn[-3] = 1
                    num_test_dyn[-2] = 2
                    num_test_dyn[-1] = 3
                    X_test_dyn_res[-3] = [current_sensors[-3]]
                    X_test_dyn_res[-2] = [current_sensors[-3],current_sensors[-2]]
                    X_test_dyn_res[-1] = [current_sensors[-3],current_sensors[-2],current_sensors[-1]]
                    current_sensors=[current_sensors[-3],current_sensors[-2],current_sensors[-1]]
            else: # si on est false on met les 2 precedent en false
                if len(current_sensors)>3:
                    predictions[-1] = False
                    predictions[-2] = False
        else:
            last_exp=exp
            num_test_dyn[-1] = 1
            X_test_dyn_res[-1]= [current_sensors[-1]]
            current_sensors = [current_sensors[-1]]
            predicted_multiple_act = True
        predictions.append(predicted_multiple_act)
        compteur+=1
    predictions[-1] = False
    predictions[-2] = False


    accuracy_dyn = accuracy_score(Y_test_dyn, predictions)
    print(f'Accuracy dyn : {accuracy_dyn * 100:.2f}%')

    results_dyn = [[ID_test_dyn[i], num_test_dyn[i],predictions[i], Y_test_dyn[i]] + X_test_dyn_res[i] for i in range(len(ID_test_dyn))]
    results_dyn = [["exp","num_sensors", "prediction", "activity"] + [f"sensors{i}" for i in range(0, 156)]] + results_dyn
    data_dyn = pd.DataFrame(results_dyn)
    data_dyn.to_csv(args.output + "dyn_segment_s" + args.subject + f"_{accuracy_dyn * 100:.0f}.csv", sep='\t', encoding='utf-8',
                header=False, index=False)

    #print("nombre d'activité total "+ str(act_total))
    #print("nombre de capteur par activité moyen "+str(cap_total/act_total))

    #lst=[[1,2],["test1","test2"]]
    results=[[ID_test[i],num_test[i],Y_pred[i],Y_test[i]]+X_test[i] for i in range(len(ID_test))]
    results=[["exp","num_sensors","prediction","activities"]+[f"sensors{i}" for i in range(0,args.max)]]+results
    data=pd.DataFrame(results)
    data.to_csv(args.output+"segment_s"+args.subject+f"_{accuracy * 100:.0f}.csv", sep='\t', encoding='utf-8', header=False, index=False)



    #fig, ax = plt.subplots()

    #plt.title("Quantitée d'activité en fonction du nombre d'activations de capteurs")
    #ax.set_xlabel("Nombre d'activations de capteur au cours de l'activitée")
    #ax.set_ylabel("Nombre d'activitées")
    #ax.plot(X, Y)
    #plt.bar(X, Y, 0.5, color='b')

    #plt.title("Quantitée d'activités par type d'activitée")
    #ax.set_ylabel("Nom d'activitée")
    #ax.set_xlabel("Nombre d'activitées")
    #ax.plot(list_act,num_par_act)
    #plt.barh(list_act,num_par_act,0.5, color='b')

    #plt.show()