# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
#import torch
import argparse

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="script main test ")
    parser.add_argument('-t', "--test", default=False, action="store_true",
                        help="est ce que on entraine avec sans puis uniquement avec combi les combinaison")
    args = parser.parse_args()

    print(f"etat de test :{args.test}")

    #print(torch.cuda.is_available())
    #print(f"int(False):{int(False)}")
    #print(f"int(True):{int(True)}")

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
