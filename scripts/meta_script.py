import argparse
#import pandas as pd
# import numpy as np
import os
import sys
import matplotlib.pyplot as plt
import subprocess

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="script pour lancer les differents autres scripts automatiquement ")
    parser.add_argument('-i', "--input", default="./IOT/" ,type=str,
                        help="path du dossier IOT mettre NO si on ne veut pas re generer")
    parser.add_argument('-s', "--scrap", default="./output/test.csv", type=str,
                        help="path/name du fichier a produire mettre NO si on ne veut pas re generer")

    parser.add_argument('-f', "--form", default="./output/form_test.csv", type=str,
                        help="path/name du fichier a produire mettre NO si on ne veut pas re generer")

    parser.add_argument('-n', "--min", default=2, type=int,
                        help="min de capteurs par activity inclus, par defaut 3 ou plus")
    parser.add_argument('-x', "--max", default=20, type=int,
                        help="max de capteurs par activity inclus, par defaut 20 ou moins")
    parser.add_argument('-b', "--bord", default=3, type=int,
                        help="pour les activitée a plus de max capteurs on garde forcement bord capteurs au debut et  a la fin (par defaut les 3 premiers et 3 derniers sont gardée quoi qu'il arrive)")

    parser.add_argument('-d', "--data", default="./output/form_test.csv", type=str,
                        help="path/name du fichier a fournir a l'IA garder vide pour prendre le même que form")

    parser.add_argument('-a', "--ia", default="./output/LSTM_res/", type=str,
                        help="path/name du fichier a produire")
    parser.add_argument('-e', "--epoch", default="5_10", type=str,
                        help="nombre d'epoch,, separé par des _ si on veut plusieurs test avec plusieurs nombres d'epoch")
    parser.add_argument('-j', "--subject", default="1_2_3_5_6_7_8_9_10", type=str,
                        help="subjects qu'on test comme test dans la repartition de donnée, separé par des _ si on veut plusieurs test sur different sujet")

    args = parser.parse_args()

    #subprocess.run(["python","main.py"]) # test

    if (args.input != "NO" and args.scrap != "NO"):
        subprocess.run([sys.executable,"./scripts/mise_en_forme/scrap_data.py","-i",f"{args.input}","-o",f"{args.scrap}"],env=os.environ)
        #on ecrit pas "python" mais sys.executable pour s'assurer qu'on utilise la venv dans laquel on est
    if (args.form != "NO" and args.scrap != "NO"):
        subprocess.run([sys.executable,"./scripts/mise_en_forme/form_data.py","-i",f"{args.scrap}","-o",f"{args.form}",'-n',f"{args.min}",'-x',f"{args.max}",'-b',f"{args.bord}"],env=os.environ)

    if ((args.form != "NO" or args.data != "") and args.ia != ""):
        input = args.data
        if args.data=="":
            input = args.form
        for model in args.ia.split:
            for epo in args.epoch.split("_"):
                for str in args.subject.split("_"):
                    print(f"on test le sujet {str} en entrainant le LTSM sur les autres")
                    subprocess.run(
                        [sys.executable, f"./scripts/analyse_IA/ia_classifier_{model}_data.py", "-i", f"{input}", "-o", f"./output/{model}_res/",'-x',
                         f"{args.max}", '-e', f"{epo}", '-s', f"{str}"], env=os.environ)
