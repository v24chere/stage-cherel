import argparse
#import pandas as pd
# import numpy as np
import os
import sys
import matplotlib.pyplot as plt
import subprocess

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="script pour lancer les differents autres scripts automatiquement ")
    parser.add_argument('-i', "--input", default="./IOT/" ,type=str,
                        help="path du dossier IOT mettre NO si on ne veut pas re generer")


    parser.add_argument('-s', "--scrap", default="./output/test.csv", type=str,
                        help="path/name du fichier ou mettre le resultat de scrap")

    parser.add_argument('-f', "--form", default="./output/form_test.csv", type=str,
                        help="path/name du fichier de mise en forme a produire ")
    parser.add_argument('-n', "--min", default=3, type=int,
                        help="min de capteurs par activity inclus, par defaut 3 ou plus")
    parser.add_argument('-x', "--max", default=20, type=int,
                        help="max de capteurs par activity inclus, par defaut 20 ou moins")

    parser.add_argument('-p', "--pair", default="./output/form_pair_test.csv", type=str,
                        help="path/name du fichier à paire mettre \"NO\" si on ne veux pas les paires de base ")

    parser.add_argument('-c', "--combi", default="./output/combi/", type=str,
                        help="path/name du fichier à combinaison de pair mettre \"NO\" si on ne veux pas les paires combinées  ")
    parser.add_argument('-b', "--num_combi", default="3", type=str,
                        help="nombre de combinaison aleatoire à faire")

    parser.add_argument('-t', "--test", default=False, action="store_true",
                        help="mettre sur True si on veut tester avec/sans combinaisons puis uniquement avec combi les combinaison. Dans ce cas bien renseigner args.pair et args.combi. si False s'entrainera selon les pair/combi donnée ou pas")




    parser.add_argument('-o', "--output", default="./output/LSTM_segment_res/", type=str,
                        help="path/name du dossier ou mettre les segments produits")
    parser.add_argument( "--output_class", default="./output/LSTM_class_res/", type=str,
                        help="path/name du dossier ou mettre les segments produits")

    parser.add_argument('-e', "--epoch", default="2", type=str,
                        help="nombre d'epoch utilisée par le segmenteur, separé par des _ si on veut plusieurs test avec plusieurs nombres d'epoch")
    parser.add_argument( "--epo_class", default="", type=str,
                        help="nombre d'epoch utilisée par le classifieur si laissée vide prend la même que le segmenteur")
    parser.add_argument('-j', "--subject", default="10", type=str,
                        help="subjects qu'on test comme test dans la repartition de donnée, separé par des _ si on veut plusieurs test sur different sujet, 1_2_3_5_6_7_8_9_10 pour tout le monde")

    parser.add_argument( "--input_class", default="", type=str,
                        help="le dossier à classifier laisser vide pour prendre automatiquement args.output , si ce champ est remplie le script ne fera que la classification")


    args = parser.parse_args()

    #subprocess.run(["python","main.py"]) # test => utiliser python ne marche pas car il n'utilise pas la venv actuel
    #et donc on ecrit pas "python" mais sys.executable pour s'assurer qu'on utilise la venv dans laquel on est

    if args.input_class=="":
        # on genere les paquet de capteurs à partir de scrap
        print(f"on scrap les donnée de {args.input} dans {args.scrap}")
        subprocess.run([sys.executable,"./scripts/mise_en_forme/scrap_data.py","-i",f"{args.input}","-o",f"{args.scrap}"],env=os.environ)

        #on met en forme les paquet de capteurs à l'aide de form
        print(f"on met en form les donnée de {args.scrap} dans {args.form}")
        subprocess.run([sys.executable,"./scripts/mise_en_forme/form_data.py","-i",f"{args.scrap}","-o",f"{args.form}",'-n',f"{args.min}",'-x',f"{args.max}"],env=os.environ)

        # on met en forme les paquet de capteurs en paire d'activité à l'aide de form_pair
        if args.pair != "NO":
            print(f"on met en form les donnée de {args.form} en pair dans {args.pair}")
            subprocess.run(
                [sys.executable, "./scripts/mise_en_forme/form_pair_data.py", "-i", f"{args.form}", "-o", f"{args.pair}"], env=os.environ)

        # on combine les paquet de capteurs en paire d'activité à l'aide de form_pair
        if args.combi != "NO":
            print(f"on utilise les donnée de {args.form} pour faire {args.num_combi} paire-s combinée par activitée dans {args.combi}")
            os.makedirs(args.combi, exist_ok=True)
            for sujet in args.subject.split("_"):
                subprocess.run(
                    [sys.executable, "./scripts/mise_en_forme/form_pair_combi_data.py", "-i", f"{args.form}", "-o", f"{args.combi}",
                     '-b', f"{args.num_combi}","-s",f"{sujet}"], env=os.environ)

        #on commence par segmenter
        os.makedirs(f"{args.output}combi{args.num_combi}/", exist_ok=True)
        os.makedirs(f"{args.output}base/", exist_ok=True)
        os.makedirs(f"{args.output}base_and_combi{args.num_combi}/", exist_ok=True)
        if args.combi != "NO" and args.input != "NO":
            spe = f"base_and_combi{args.num_combi}"
        elif args.combi != "NO" and args.input == "NO": # condidion redondante laissée pour la comprehension
            spe = f"combi{args.num_combi}"
        elif args.combi =="NO" and args.input != "NO":  # condidion redondante laissée pour la comprehension
            spe = f"base"
        else:
            sys.exit("Fournit ni de quoi faire des paires basiques ni paires combinées")
        for epo in args.epoch.split("_"):
            for sujet in args.subject.split("_"):
                print(f"on segmente en s'entrainant sur {args.pair} et {args.combi}form_pair_noS{sujet}_combi{args.num_combi}.csv")
                subprocess.run(
                    [sys.executable, "./scripts/analyse_IA/ia_segmentor_LSTM_data.py",
                     "-i", f"{args.pair}",
                     "-c", f"{args.combi}form_pair_noS{sujet}_combi{args.num_combi}.csv",
                     "-o", f"{args.output}{spe}/",
                     "-e", f"{epo}",
                     "-s", f"{sujet}",
                     '-d', f"{args.form}" ], env=os.environ)
                print(f"on met en forme les segment dans ./output/LSTM_segment_res/{spe}/epos{epo}/form_segment_s{sujet}.csv")
                subprocess.run(
                    [sys.executable, "./scripts/mise_en_forme/form_from_segment_data.py",
                     "-i", f"{args.output}{spe}/epos{epo}/raw_segment_s{sujet}.csv",
                     "-o", f"{args.output}{spe}/epos{epo}/form_segment_s{sujet}.csv"], env=os.environ)


                # si on est en mode test on entraine juste avec a base puis juste avec les combinaison, on ne repete pas combi plus base qui est deja fait
                if args.test and args.pair != "NO" and args.combi != "NO":
                    print(
                        f"on segmente en s'entrainant sur {args.combi}form_pair_noS{sujet}_combi{args.num_combi}.csv")
                    subprocess.run(
                        [sys.executable, "./scripts/analyse_IA/ia_segmentor_LSTM_data.py",
                         "-i", f"NO",
                         "-c", f"{args.combi}form_pair_noS{sujet}_combi{args.num_combi}.csv",
                         "-o", f"{args.output}combi{args.num_combi}/",
                         "-e", f"{epo}",
                         "-s", f"{sujet}",
                         '-d', f"{args.form}"], env=os.environ)
                    print(
                        f"on met en forme les segment dans ./output/LSTM_segment_res/combi{args.num_combi}/epos{epo}/form_segment_s{sujet}.csv")
                    subprocess.run(
                        [sys.executable, "./scripts/mise_en_forme/form_from_segment_data.py",
                         "-i", f"{args.output}combi{args.num_combi}/epos{epo}/raw_segment_s{sujet}.csv",
                         "-o", f"{args.output}combi{args.num_combi}/epos{epo}/form_segment_s{sujet}.csv"], env=os.environ)

                    print(f"on segmente en s'entrainant sur {args.pair} ")
                    subprocess.run(
                        [sys.executable, "./scripts/analyse_IA/ia_segmentor_LSTM_data.py",
                         "-i", f"{args.pair}",
                         "-c", f"NO",
                         "-o", f"{args.output}base/",
                         "-e", f"{epo}",
                         "-s", f"{sujet}",
                         '-d', f"{args.form}", ], env=os.environ)
                    print(
                        f"on met en forme les segment dans ./output/LSTM_segment_res/base/epos{epo}/form_segment_s{sujet}.csv")
                    subprocess.run(
                        [sys.executable, "./scripts/mise_en_forme/form_from_segment_data.py",
                         "-i", f"{args.output}base/epos{epo}/raw_segment_s{sujet}.csv",
                         "-o", f"{args.output}base/epos{epo}/form_segment_s{sujet}.csv"], env=os.environ)
    input_class=args.output
    if args.input_class!= "":
        input_class=args.input_class
    # on genere les metriques des segments
    subprocess.run(
        [sys.executable, "./scripts/traitement_resultats/metrique_segment_dyn_local.py",
         "-i", f"{input_class}"], env=os.environ)

    # on lance le classifier sur tout le dossier input_class si donnée ou output sinon
    epo_class=args.epoch
    if args.epo_class!="":
        epo_class=args.epoch_class

    subprocess.run(
        [sys.executable, "./scripts/analyse_IA/ia_classifier_LSTM_directory.py",
         "-i", f"{args.form}",
         "-o", f"{args.output_class}/",
         "-e", f"{epo_class}",
         "-s", f"{args.subjet}",
         '-d', f"{input_class}"], env=os.environ)

    # on genere les metriques des segments classée
    subprocess.run(
        [sys.executable, "./scripts/traitement_resultats/metrique_class_dynl.py",
             "-i", f"{args.output_class}"], env=os.environ)


