import argparse
#import pandas as pd
# import numpy as np
import os
import sys
import matplotlib.pyplot as plt
import subprocess
from datetime import datetime

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="script pour lancer les differents autres scripts automatiquement ")
    parser.add_argument('-i', "--input", default="./videos/s10/" ,type=str,
                        help="path du dossier des vidéos à parcourir")

    parser.add_argument('-o', "--output", default="./output/Tracking/", type=str,
                        help="path/name du dossier ou mettre les segments produits")

    parser.add_argument('-c', "--confidence", default="5_10", type=str,
                        help="treshold de confiance pour la detection, separé par des _ si on veut plusieurs test avec plusieurs niveau de confiance")


    args = parser.parse_args()

    #subprocess.run(["python","main.py"]) # test => utiliser python ne marche pas car il n'utilise pas la venv actuel
    #et donc on ecrit pas "python" mais sys.executable pour s'assurer qu'on utilise la venv dans laquel on est

    #on commence par segmenter

    print(f"On parcourt {args.input}")
    for conf in args.confidence.split("_"):
        print(f"On track au niveau de confiance : {conf}%")
        for root, dirs, files in os.walk(args.input):
            for file in files:
                if file.endswith('.mp4'):
                    datetime.now().strftime('date : %Y-%m-%d %H:%M:%S \n')
                    subprocess.run(
                        [sys.executable, "./scripts/skeletons_script/track_video_DeepSort.py",
                         "-i", f"{os.path.join(root, file)}",
                         "-o", f"{args.output}","-w",
                         "-c",f"{conf}"], env=os.environ)






