import argparse
import pandas as pd
# import numpy as np
import os

# sensor dict
name_sensor_dict = {
    #0:area
    "sf_1": "floor_entrance", #1
    "sf_2": "floor_bedroom", #2
    "sf_3": "floor_kitchen", #3
    "sf_4": "floor_bathroom", #4
    "sf_5": "floor_dining_room",#5
    "sf_6": "floor_livingroom",#6
    # "motion1": "motion_livingroom",
    # "motion2": "motion_bathroom",
    # "motion3": "motion_dining_room",
    # "motion4": "motion_kitchen",
    #1:door
    "door1": "door_entrance",#0
    "door2": "door_cabinet_kitchen",#1
    "door4": "door_fridge",#2
    "door5": "door_cabinet_bedroom",#3
    "drawer1": "drawer_utensils_kitchen",
    "drawer2": "drawer_baking_tools_kitchen",
    "drawer3": "drawer_tableware_kitchen",
    #2:power
    "green_3p": "television",#0
    "pmeter_1": "oven",
    #3:light
    "lamp_entree": "light_entrance",#0
    "lamp_couloir": "light_bedroom",#1
    "lamp_cuisine": "light_kitchen",#2
    "lamp_sdb": "light_bathroom",#3
    "lamp_salle": "light_dining_room",#4
    "lamp_salon": "light_livingroom",#5
    #4:button
    "btn_wc_light": "light_switch_bathroom",#0
    "btn_wc_small_flush": "wc_fush",#1
    "btn_wc_big_flush": "wc_fush",#1
    #5:flow
    "dtx_robinet1": "tap_kitchen",#0
    "dtx_robinet2": "tap_batheroom",#1
    #6:detected
    "dtx_lit": "presure_bed",#0
    "dtx_fauteuil": "presure_armchair",#1



    # "relay_spot": "lamp_spot",
    # "lamp_ambiance": "lamp_ambiance",
    # humidity? :
    # "rh_aquara1": "humidity_location1",
    # "rh_aquara2": "humidity_location2",
    # "rh_omw": "humidity_location3",
    # "rh_netatmo": "humidity_location4",

}

# unused sensor list, can be part of the name ie motion to get rid of motion1, motion2
list_unused = [
    'activity button',
    'multi_pmeter',
    "green_1p",
    "green_2p",
    "green_4p",
    "green_5p",
    "green_6p",
    "rh_owm",  # -> humidity
    "pmeter_robot",
    "pmeter_2",
    "pmeter_3",  # oubli ?
    "motion",
    "pmeter_bouilloire",
    "shutter",
    "light_switch",
    "Bruit Cuisine",
    "Niveau Wifi NetAtmo",
    "Pression Cuisine",
    "temp_netatmo",
    "co2_1",
    "Niveau Wifi NetAtmo",
    "co2_2",
    "Niveau radio NA Salon",
    "T° SR",
    "co2_3",
    "Niveau radio NA SR",
    "Niveau radio NA Ext",
    "RH SR",
    "temp_owm",
    "temp_aqara2",
    "rh_aqara2",  # humidity
    "T° Salon",
    "RH Salon",
    "T°Extérieur",
    "temp_aqara1",
    "rh_aqara1",  # humidity
    "press_aqara1",
    "rh_netatmo",  # humidity
    "RH Ext",
    "pmeter_spot",
    "Batterie NA SR",
    "relay_qtrobot",
    "pmeter_qtrobot",
    "lamp_spot",
    "lamp_ambiance",
    "humidity"

]


def clean_one_csv(path: str, arg):
    df = pd.read_csv(
        path,
        header=0,
        names=["ind", "datetime", "sensor", "event_type", "value", "activity"]
    )
    df = df.drop(['ind'], axis=1)  # on retire les id d'evenement

    #print(df.index[(df.event_type=="xaal-lab.hmi.basic.state")][0])
    num_bs=len(df.index[(df.event_type=="xaal-lab.hmi.basic.state")])
    if num_bs>2:
        df.sensor.values[df.index[(df.event_type=="xaal-lab.hmi.basic.state")][2]]="camera_start"
    else:
        if num_bs>0:
            df.sensor.values[df.index[(df.event_type == "xaal-lab.hmi.basic.state")][num_bs-1]] = "camera_start"
        else:
            print("******** \n \n \n \n Le csv :"+path +" n'as pas de basic state \n \n \n \n \n **********")
    df.dropna(subset=["sensor"], inplace=True)  # on retire les capteur sans nom

    df = df.reset_index(drop=True)

    # on enleve l'avant et l'apres experience
    start_index = df.activity.eq('start_experiment').idxmax()
    stop_index = df.activity.eq('stop_experiement').idxmax()
    experiment = df[start_index + 2:stop_index]

    # error correction section
    if arg.error != "":
        err = pd.read_csv(
            arg.error,
            header=0,
            names=["datetime", "sensor", "event_type", "value", "activity", "s_num", "e_num", "instruction", "info"]
        )
        # we keep only the error related to the experiment we are processing
        path_split = path.replace("\\", "/")  # some write path with / others with \
        path_split = path.split("/")
        actual_s_num = path_split[-3].split("s")[1]
        actual_e_num = path_split[-2].split("Experiment")[1]
        err_s = experiment[err("s_num") == actual_s_num or err("s_num") < 0]
        err_s_e = experiment[err_s("s_num") == actual_e_num or err_s("s_num") < 0]
        # err_s_e == le sub set of correction concerning the csv we are proccessing
        miss = pd.DataFrame()

        for i in range(err_s_e.shape[0]):
            if err.instruction[i] == "missing":
                miss = pd.concat([miss,
                                  pd.DataFrame([[err.datetime, err.sensor, err.event_type, err.value, err.activity]],
                                               columns=["datetime", "sensor", "event_type", "value", "activity"])],
                                 ignore_index=True)
            elif err.instruction[i] == "timechange":
                experiment = experiment.replace({"datetime": err_s_e.datetime[i]}, err_s_e.info[i])
            elif err.instruction[i] == "remove":
                to_delete = experiment.datetime.eq(err_s_e.datetime[i])
                experiment.event_type[to_delete] = "DELETE"
            elif err.instruction[i] == "changelabel":
                experiment = experiment.replace({"activity": err_s_e.activity[i]}, err_s_e.info[i])
            elif err.instruction[i] == "translation_sensor":
                name_sensor_dict.update({err_s_e.sensor[i]: err_s_e.info[i]})
            elif err.instruction[i] == "unused":
                list_unused.append(err.info[i])
            else:
                print("error instruction not recognised : " + err.instruction[i])

        experiment["datetime"] = pd.to_datetime(experiment["datetime"])
        experiment = pd.concat([miss, experiment], ignore_index=True)
        experiment = experiment.sort_values(by="datetime")
        experiment = experiment.reset_index(drop=True)

    exp = experiment

    # we give sensors comprehensible name
    exp["sensor"].replace(name_sensor_dict, inplace=True)
    exp["value"].replace({"True":10,"False":0},inplace=True)
    # We ensure that the data are in the right order
    exp = exp.sort_values(by="datetime")
    # We transform float value in ON/OFF, OPEN/CLOSE ...
    types = exp.event_type.values
    names = exp.sensor.values
    values = exp.value.values.astype(float)
    activity = exp.activity.values.copy()
    predictions = exp.activity.values.copy()
    prediction = "debut"
    before_first_label = []

    for i, type in enumerate(types):
        if type.endswith("presence") or type.endswith("detected") or type.endswith("light") or type.endswith(
                "click") or type.endswith("tog") or type.endswith("state"):
            if values[i] >= 1.0:
                activity[i] = "ON"
            else:
                activity[i] = "OFF"
        elif type.endswith("power") or type.endswith("status") or type.endswith("pow"):
            if values[i] >= 5.0:
                activity[i] = "ON"
            else:
                activity[i] = "OFF"
        elif type.endswith("humidity"):
            # if values[i] >= 65.0:
            #    activity[i] = "WET"
            # else:
            #    activity[i] = "DRY"
            activity[i] = "DELETE"

        elif type.endswith("delete") or type.endswith("temperature") or type.endswith("position") or type.endswith(
                "co2") or type.endswith(
            "pressure") or type.endswith("sound"):  # "useless" sensors
            activity[i] = "DELETE"
        else:
            print(str(i) + "is not detected :" + type)

        if pd.isnull(predictions[i]) or predictions[i] == "":
            if prediction == "debut":  # if we detect things before the first activity button was pressed
                before_first_label.append(i)  # we stock those to give them the label of the first activity we encounter
            else:
                predictions[i] = prediction
        else:
            if prediction == "debut":  # case where the activity button was pressed a bit late
                for j in before_first_label:
                    predictions[j] = predictions[i]
            prediction = predictions[i]

    etat_dict = {}
    # print(exp.sensor.unique())
    for sensor in exp.sensor.unique():
        etat_dict[sensor] = "NULL"

    for i, name in enumerate(names):
        if name.startswith("door") or name.startswith("drawer"):
            if values[i] >= 1.0:
                activity[i] = "OPEN"
            else:
                activity[i] = "CLOSE"
        for unused in list_unused:
            if name.startswith(unused):
                activity[i] = "DELETE"
                break
        # we delete redondant information to only keep state change ie for one sensor ON , ON ,ON ,OFF , OFF , ON -> ON,OFF,ON
        if etat_dict[name] == "NULL":  # init
            etat_dict[name] = activity[i]
        elif etat_dict[name] == activity[i]:  # same state twice in a row -> we delete
            activity[i] = "DELETE"
        else:  # state change we update our state dictionnary
            etat_dict[name] = activity[i]

    exp.value = activity
    exp.activity = predictions

    exp_no_unused = exp[~(exp["value"] == "DELETE")]  # delete all unused/repeated sensor

    sortie = arg.output

    sortie += path


    os.makedirs(os.path.dirname(sortie), exist_ok=True)
    exp_no_unused.to_csv(sortie, sep='\t', encoding='utf-8', header=False, index=False)
    # exp_buttons.to_csv(sortie.split(".csv")[0] + "_buttons.csv")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui clean les csv brut d'experience")
    parser.add_argument('-i', "--input", type=str,
                        help="path du dossier à traiter integralement ou directement .csv à traiter")
    # parser.add_argument("--replace",type=bool,default=False, help="si on change le csv d'entrée au lieu de creer une copie dans output")
    parser.add_argument("-o", "--output", default="./output/", type=str,
                        help="output path by default is /cleaned/path/to/csv , we repeat the input path")

    parser.add_argument('-e', "--error", default="", type=str,
                        help="path of error.csv who contains errors of the experiments feature non complété, a ignorer")

    args = parser.parse_args()

    pd.options.mode.chained_assignment = None
    # on verifie si on a un csv a traiter ou tout un fichier à parcourir
    if args.input.endswith(".csv"):
        print("on traite :" + args.input)
        print("vers :" + args.output + args.input)
        clean_one_csv(args.input, args)
    else:
        for root, dirs, files in os.walk(args.input):
            for file in files:
                if file.endswith('.csv') and not file.startswith('.'):
                    print("on traite :" + os.path.join(root, file))
                    clean_one_csv(os.path.join(root, file), args)
