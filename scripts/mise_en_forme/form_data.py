import argparse
import pandas as pd
# import numpy as np
import os
import matplotlib.pyplot as plt
import random

def combine(cap,max):   #on fait une combinaison de max parmis cap sous forme d'un tableau avec exactement max True
    res = [False] * (cap-max) +[True]*max
    random.shuffle(res)
    return res

def decoupe(cap,max):
    palier=cap//max
    res=[]
    return res

activities=[]
main_location=[]
locations=["floor_livingroom","floor_dining_room","floor_bathroom","floor_bedroom","floor_kitchen","floor_entrance"]
near_location=[["floor_dining_room","floor_bedroom","floor_entrance"],["floor_livingroom","floor_bathroom","floor_bedroom","floor_kitchen"],
               ["floor_dining_room","floor_kitchen"],["floor_livingroom","floor_dining_room","floor_kitchen","floor_entrance"],
               ["floor_livingroom","floor_bathroom","floor_bedroom","floor_kitchen"],["floor_livingroom","floor_dining_room","floor_bedroom","floor_entrance"]]

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui lit le fichier des capteurs regroupée par activité et le met en forme.")
    parser.add_argument('-i', "--input",default="./output/test.csv", type=str,
                        help="path/name.csv du fichier des capteurs regroupée par activité, produit par scrap")
    parser.add_argument('-o', "--output",default="./output/form_test.csv", type=str,
                        help="path/name.csv du fichier a produire")
    parser.add_argument('-n', "--min",default=3, type=int,
                        help="min de capteurs pour qu'une activité soit gardé, par defaut 3 ou plus")
    parser.add_argument('-d', "--double", default=True, action="store_false",
                        help="si laissée activée on tente de doubler le premier et dernier capteur pour passer au dessus de la barre args.min")
    parser.add_argument('-x', "--max", default=20, type=int,
                        help="max de capteurs par activity inclus, par defaut 20 ou moins. On retire aléatoirement du surplus en cas de depassement")


    args = parser.parse_args()

    #scrap_header =["exp","num_sensors","activity"]+[f"sensor{i}" for i in range(0,156)]
    results=[["exp","num_sensors","activity","start","end","duree_secondes"]+[f"sensors{i}" for i in range(0,args.max)]]
    dfb = pd.read_csv(args.input,delimiter="\t",header=0)


    num_ligne=dfb["exp"].size

    for i in range(num_ligne):
        #print(dfb["exp"][i]+" "+str(dfb["num_sensors"][i])+" "+dfb["activity"][i])
        activity=dfb['activity'][i]
        debut_act=dfb['start'][i]
        end_act = dfb['end'][i]
        duree_act = dfb['duree_secondes'][i]
        num_cap=int(dfb["num_sensors"][i])
        new_line = [dfb["exp"][i]]
        if activity != "start_experiment"  and activity != "take medicine" : #on filtre les start_experiment et les activitées trop courte
            if num_cap>=args.min:
                if num_cap>args.max:
                    new_line.append(args.max)
                    new_line.append(activity)
                    new_line.append(debut_act)
                    new_line.append(end_act)
                    new_line.append(duree_act)
                    cap_to_keep=combine(num_cap,args.max)
                    for select in range(num_cap):
                        if cap_to_keep[select] :
                            new_line.append(dfb[f"sensors{select}"][i])

                else:
                    new_line.append(num_cap)
                    new_line.append(activity)
                    new_line.append(debut_act)
                    new_line.append(end_act)
                    new_line.append(duree_act)
                    for cap in range(num_cap):
                        new_line.append(dfb[f"sensors{cap}"][i])
                #print(new_line[0] + " " + str(new_line[1]) + " " + new_line[2])
                results.append(new_line)
            else:
                if args.double and num_cap+2>=args.min: # on dedouble le premier et dernier capteur pour les activité trop petite en nombre de capteur
                    new_line.append(num_cap+2)
                    new_line.append(activity)
                    new_line.append(debut_act)
                    new_line.append(end_act)
                    new_line.append(duree_act)
                    new_line.append(dfb[f"sensors{0}"][i])
                    for cap in range(num_cap):
                        new_line.append(dfb[f"sensors{cap}"][i])
                    new_line.append(dfb[f"sensors{num_cap-1}"][i])
                    results.append(new_line)

    #lst=[[1,2],["test1","test2"]]
    data=pd.DataFrame(results)
    data.to_csv(args.output, sep='\t', encoding='utf-8', header=False, index=False)

