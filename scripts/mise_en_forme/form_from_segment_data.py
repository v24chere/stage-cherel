import argparse
import pandas as pd
# import numpy as np
import os
import matplotlib.pyplot as plt
import random

def combine(cap,max):   #on fait une combinaison de max parmis cap sous forme d'un tableau avec exactement max True
    res = [False] * (cap-max) +[True]*max
    random.shuffle(res)
    return res

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui met en form un fichier de \"segment\" produit par segmenteur. Ce sont des liste de capteurs avec un true là on a été predit le changement d'activité. On regroupe en activitée selon ce marquage")
    parser.add_argument('-i', "--input",default="output/LSTM_segment_res/epos10/dyn_segment_s10_epos10.csv", type=str,
                        help="path du fichier des segments")
    parser.add_argument('-o', "--output",default="./output/form_dyn_s10_lstm.csv", type=str,
                        help="path/name du fichier a produire")
    # parser.add_argument("--replace",type=bool,default=False, help="si on change le csv d'entrée au lieu de creer une copie dans output")

    args = parser.parse_args()



    #dyn_header =["exp","predi_new_act","conficende","activation","activity","sensor"]
    results=[["exp","num_sensors","activities"]+[f"sensors{i}" for i in range(0,156)]]
    dfb = pd.read_csv(args.input,delimiter="\t",header=0)

    num_ligne=dfb["exp"].size
    new_line=[dfb["exp"][0],0,[]]
    new_activ=[]

    for i in range(num_ligne):
        exp=dfb["exp"][i]
        activity=dfb['activity'][i]
        predi=dfb['predi_new_act'][i]
        cap=dfb['sensor'][i]

        if exp==new_line[0]:
            if predi:
                results.append(new_line)
                new_line=[exp,1,[activity],cap]
            else:
                new_line.append(cap)
                new_line[1]+=1
                new_line[2].append(activity)
        else:
            new_line=[exp,1,[activity],cap]
    results.append(new_line)

    data=pd.DataFrame(results)
    data.to_csv(args.output, sep='\t', encoding='utf-8', header=False, index=False)
