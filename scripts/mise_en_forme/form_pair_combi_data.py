import argparse
import pandas as pd
# import numpy as np
import os
import matplotlib.pyplot as plt
import random

def combine(cap,max):   #on fait une combinaison de max parmis cap sous forme d'un tableau avec exactement max True
    res = [False] * (cap-max) +[True]*max
    random.shuffle(res)
    return res
def coherance(start,end):
    if start==end:
        return False
    if end=="enter_home" or start == "leave_home":
        return False
    return True
def roll(cut,num):
    res = []
    if num>cut*2:
        num_cut=(cut+1)//2
        for i,test in enumerate(combine(cut+1,num_cut)):
            if test:
                res.append(i)
    else:
        res.append(0)
    return res


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui produit des paires d'activitées coherantes en utilisant n'importe quel activité parmis toute la base de données")
    parser.add_argument('-i', "--input",default="./output/form_test.csv", type=str,
                        help="path du fichier produit par form_data contenant les capteurs groupées par activitées")
    parser.add_argument('-o', "--output",default="./output/combi/", type=str,
                        help="path/name du dossier mettre le fichier produit ")

    parser.add_argument('-s', "--subject", default="10", type=str,
                        help="sujet a exclure,en general celui que nous voudrons tester. Laisser vide si on veut tout le monde")

    parser.add_argument('-c', "--cut", default=3, type=int,
                        help="vas aléatoirement cut les args.cut premier capteur")
    parser.add_argument('-m', "--max", default=6, type=int,
                        help="max de capteurs dans le deuxième membre de la paire")
    parser.add_argument('-b', "--combi", default=3, type=int,
                        help="max de combinaison pour chaque activité mettre à -1 pour pas de limite (on fait toute les paire possible")


    # parser.add_argument("--replace",type=bool,default=False, help="si on change le csv d'entrée au lieu de creer une copie dans output")

    args = parser.parse_args()

    list_act=[] # format [exp,num_cap,activity, sensors0 ...]



    #scrap_header =["exp","num_sensors","activity"]+[f"sensor{i}" for i in range(0,156)]
    results=[["exp","exp_bis","num_sensors","activity1","activity2"]+[f"sensors{i}" for i in range(0,20+args.max)]]
    dfb = pd.read_csv(args.input,delimiter="\t",header=0)
    num_ligne=dfb["exp"].size

    for i in range(num_ligne):
        exp=dfb["exp"][i]
        num_cap = int(dfb["num_sensors"][i])
        activity = dfb['activity'][i]
        if exp.split("_")[0]!=("s"+args.subject):
            new_line=[exp,num_cap,activity]
            for cap in range(num_cap):
                new_line.append(dfb[f"sensors{cap}"][i])
            list_act.append(new_line.copy())

    # format [0:exp,1:num_cap,2:activity,3+cap: sensors0 ...]
    list_act_pair=list_act.copy()
    for j in range(len(list_act)):
        random.shuffle(list_act_pair)  # a chaque nouvel donnée on shuffle les membres seconds pour faire varier les combinaisons
        exp_start=list_act[j][0]
        num_start=list_act[j][1]
        act_start = list_act[j][2]
        if act_start != "leave_home":
            compteur=0
            for act_end in list_act_pair:
                if compteur==args.combi:
                    break
                if coherance(act_start,act_end[2]):
                    for cut in roll(args.cut,num_start):
                        new_line=[exp_start,act_end[0],0,act_start,"Noact"]
                        for cap in range (cut,num_start):
                            new_line.append(list_act[j][3+cap])
                            new_line[2]+=1
                            results.append(new_line.copy())
                        new_line[4] = act_end[2]
                        for cap in range (min(args.max,act_end[1])):
                            new_line.append(act_end[3 + cap])
                            new_line[2] += 1
                            results.append(new_line.copy())
                    compteur+=1



    os.makedirs(args.output , exist_ok=True)

    data=pd.DataFrame(results)                           #_c{args.cut}_m{args.max}
    data.to_csv(args.output+f"form_pair_noS{args.subject}_combi{args.combi}.csv", sep='\t', encoding='utf-8', header=False, index=False)

    fig, ax = plt.subplots()

