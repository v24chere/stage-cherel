import argparse
import pandas as pd
# import numpy as np
import os
import matplotlib.pyplot as plt
import random

def combine(cap,max):   #on fait une combinaison de max parmis cap sous forme d'un tableau avec exactement max True
    res = [False] * (cap-max) +[True]*max
    random.shuffle(res)
    return res

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui parcourt les paquet de cpateurs groupées par activitées et forme des paires adjacentes que nous utiliserons pour s'entrainer à detecter les changements d'activité")
    parser.add_argument('-i', "--input",default="./output/form_test.csv", type=str,
                        help="path du fichier produit par form_data")
    parser.add_argument('-o', "--output",default="./output/form_pair_test.csv", type=str,
                        help="path/name du fichier a produire")

    parser.add_argument('-m', "--max", default=6, type=int,
                        help="max de capteurs dans le deuxième membre de la paire")


    # parser.add_argument("--replace",type=bool,default=False, help="si on change le csv d'entrée au lieu de creer une copie dans output")

    args = parser.parse_args()

    #scrap_header =["exp","num_sensors","activity"]+[f"sensor{i}" for i in range(0,156)]
    results=[["exp","num_sensors","activity1","activity2"]+[f"sensors{i}" for i in range(0,156+args.max)]]
    dfb = pd.read_csv(args.input,delimiter="\t",header=0)
    #print(dfb)
    start=0
    num_ligne=dfb["exp"].size

    for i in range(start,num_ligne-1):
        activity = dfb['activity'][i]
        activity_bis = dfb['activity'][i+1]
        num_cap = int(dfb["num_sensors"][i])
        num_cap_bis = int(dfb["num_sensors"][i+1])
        if activity != "start_experiment" :  # on filtre les start_experiment
            expe=dfb["exp"][i]
            if expe == dfb["exp"][i+1]:
                new_line = [(dfb["exp"][i]),0,activity,"Noact"]

                for cap in range(num_cap):
                    new_line[1] += 1
                    new_line.append(dfb[f"sensors{cap}"][i])
                    results.append(new_line.copy())

                new_line[3] = activity_bis
                for cap in range(min(num_cap_bis,args.max)):
                    new_line[1] += 1
                    new_line.append(dfb[f"sensors{cap}"][i+1])
                    results.append(new_line.copy())

    #print("nombre d'activité total "+ str(act_total))
    #print("nombre de capteur par activité moyen "+str(cap_total/act_total))

    #lst=[[1,2],["test1","test2"]]
    data=pd.DataFrame(results)
    data.to_csv(args.output, sep='\t', encoding='utf-8', header=False, index=False)

    fig, ax = plt.subplots()

    #plt.title("Quantitée d'activité en fonction du nombre d'activations de capteurs")
    #ax.set_xlabel("Nombre d'activations de capteur au cours de l'activitée")
    #ax.set_ylabel("Nombre d'activitées")
    #ax.plot(X, Y)
    #plt.bar(X, Y, 0.5, color='b')

    #plt.title("Quantitée d'activités par type d'activitée")
    #ax.set_ylabel("Nom d'activitée")
    #ax.set_xlabel("Nombre d'activitées")
    #ax.plot(list_act,num_par_act)
    #plt.barh(list_act,num_par_act,0.5, color='b')

    #plt.show()