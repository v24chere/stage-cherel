import argparse
import pandas as pd
# import numpy as np
import os
import matplotlib.pyplot as plt
from datetime import datetime
import networkx as nx

def duration(start,end):
    time_start= datetime.strptime(start, "%H:%M:%S.%f")
    time_end= datetime.strptime(end, "%H:%M:%S.%f")
    duration=time_end - time_start
    tot_sec =duration.total_seconds()
    return tot_sec

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui lit la base IOT donnée par le args.input et regroue les capteurs par activitées dans args.output")
    parser.add_argument('-i', "--input",default="./IOT/", type=str,
                        help="path du dossier IOT")
    parser.add_argument('-o', "--output", default="./output/test.csv", type=str,
                        help="path/name.csv du fichier a produire")
    parser.add_argument( "--graph", default=False,action='store_true',
                        help="produit un graph de transition")
    parser.add_argument("--tps_act", default=False,action='store_true',
                        help="produit un graph tps par act")
    # parser.add_argument("--replace",type=bool,default=False, help="si on change le csv d'entrée au lieu de creer une copie dans output")

    args = parser.parse_args()

    # on verifie si on a un csv a traiter ou tout un fichier à parcourir
    X=range(1,157)
    Y=[0]*156
    list_act=[]
    num_par_act=[]
    temps_par_act=[]

    combinaison=[]
    num_combi=[]

    list_activite_et_capteur_associe=[["exp","num_sensors","activity","start","end","duree_secondes"]+[f"sensors{i}" for i in range(0,156)]]

    cap_in_act_max = 1
    cap_total=0
    act_total=0
    exp_total=0
    for root, dirs, files in os.walk(args.input):
        for file in files:
            if file.endswith(".csv"):
                file_full_path = os.path.join(root, file)
                #print("on traite :"+file_full_path)

                dfb = pd.read_csv(file_full_path,delimiter="\t",header=None,names=[ "datetime", "sensor", "event_type", "value", "activity"])


                start=0
                if dfb["sensor"][0]=="camera_start":
                    start=1
                while dfb['activity'][start]=="start_experiment":
                    start+=1

                num_ligne=dfb["sensor"].size
                num_cap=str(num_ligne-1)
                compte_cap=1

                compte_act=1
                #capteur = dfb['sensor'][start] + "_" + dfb['value'][start]

                activity=dfb['activity'][start]
                new_line=[file,activity,dfb['datetime'][start].split(" ")[1],dfb['sensor'][start] + "_" + dfb['value'][start]]
                #print(new_line)

                if activity in list_act:
                    num_par_act[list_act.index(activity)] += 1
                else:
                    list_act.append(activity)
                    num_par_act.append(1)
                    temps_par_act.append(0)
                for i in range(start+1,num_ligne):
                    if dfb['sensor'][i] != "core_metadb" and dfb["activity"][i]!="start_experiment":
                        capteur = dfb['sensor'][i] + "_" + str(dfb['value'][i])

                        new_act=dfb['activity'][i]
                        if dfb['activity'][i].split("_")[0] == "eat":  # groupe breakfast/lunch et dinner
                            new_act="eat"
                        if dfb['activity'][i].split("_")[0] == "cook": # groupe breakfast/lunch et dinner
                            new_act="cook"
                        if dfb['activity'][i].split("_")[0] == "sleep": # transform sleep_in_bed into sleep
                            new_act="sleep"
                        if new_act == activity:
                            compte_cap+=1
                            new_line.append(capteur)
                        else:
                            compte_act+=1
                            Y[compte_cap-1]+=1
                            if compte_cap>cap_in_act_max:
                                cap_in_act_max = compte_cap
                            combi = activity + "_&_" + new_act
                            if combi in combinaison:
                                num_combi[combinaison.index(combi)]+=1
                            else:
                                combinaison.append(combi)
                                num_combi.append(1)
                            if new_act in list_act:
                                num_par_act[list_act.index(new_act)]+=1
                            else :
                                list_act.append(new_act)
                                num_par_act.append(1)
                                temps_par_act.append(0)
                            new_line.insert(1, compte_cap)
                            new_line.insert(4, dfb['datetime'][i].split(" ")[1])
                            new_line.insert(5, duration(new_line[3],new_line[4]))

                            temps_par_act[list_act.index(activity)]+=new_line[5]

                            compte_cap=1
                            activity=new_act

                            list_activite_et_capteur_associe.append(new_line.copy())
                            new_line = [file,activity,dfb['datetime'][i].split(" ")[1], capteur]
                            #print(new_line)
                new_line.insert(1, compte_cap)
                new_line.insert(4, dfb['datetime'][num_ligne-1].split(" ")[1])
                new_line.insert(5, duration(new_line[3], new_line[4]))
                temps_par_act[list_act.index(activity)] += new_line[5]
                list_activite_et_capteur_associe.append(new_line)
                Y[compte_cap - 1] += 1
                if compte_cap > cap_in_act_max:
                    cap_in_act_max = compte_cap
                #print(file_full_path+" possède "+num_cap +" activations de capteurs pour "+str(compte_act)+" activité")
                cap_total+= num_ligne-1
                act_total+=compte_act
                exp_total+=1
    #print("maximum de capteur dans une activité:" + str(cap_in_act_max))
    #print("nombre d'activité total "+ str(act_total)+" parmis "+ str(exp_total)+" experiences")
    #print("nombre de capteur par activité moyen "+str(cap_total/act_total))
    #print(list_act)
    #print(num_par_act)
    #print(temps_par_act)
    tpsmoy_par_act=[temps_par_act[i]/num_par_act[i] for i in range(len(num_par_act))]
    #print(tpsmoy_par_act)

    #print(combinaison)
    #print(num_combi)

    # lst=[[1,2],["test1","test2"]]
    data = pd.DataFrame(list_activite_et_capteur_associe)
    data.to_csv(args.output, sep='\t', encoding='utf-8', header=False, index=False)

    #section graph de transitions d'activités
    if args.graph:
        G = nx.DiGraph()
        G.add_nodes_from(list_act)

        transitions=[(combinaison[i].split("_&_")[0],combinaison[i].split("_&_")[1],num_combi[i]) for i in range(len(combinaison))]
        print(transitions)

        G.add_edges_from([(start, end) for start, end, label in transitions])
        edge_labels = {(start, end): label for start, end, label in transitions}
        max_label = max(edge_labels.values())
        arrow_sizes = {edge: (label / max_label) * 5 for edge, label in edge_labels.items()}

        pos = nx.spring_layout(G, k=2, iterations=100)
        plt.figure(figsize=(8, 6))
        nx.draw(G, pos, with_labels=True, node_size=1500, node_color="skyblue", font_size=10, font_color="black")
        for (start, end), size in arrow_sizes.items():
            nx.draw_networkx_edges(G, pos, edgelist=[(start, end)],width=size ,arrowsize=15)

        #nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels, font_size=12, font_color="red")

        plt.title("Graphe de transition d'activitées")
        plt.show()

    # Section GRAPHIQUES


    #fig, ax = plt.subplots()

    #plt.title("Quantitée d'activité en fonction du nombre d'activations de capteurs")
    #ax.set_xlabel("Nombre d'activations de capteur au cours de l'activitée")
    #ax.set_ylabel("Nombre d'activitées")
    #ax.plot(X, Y)
    #plt.bar(X, Y, 0.5, color='b')

    #plt.title("Quantitée d'activités par type d'activitée")
    #ax.set_ylabel("Nom d'activitée")
    #ax.set_xlabel("Nombre d'activitées")

    #plt.barh(list_act,num_par_act,0.5, color='b')

    if args.tps_act:
        fig, ax = plt.subplots()
        plt.title("Durée d'activitée moyen par type d'activitée")
        ax.set_ylabel("Nom d'activitée")
        ax.set_xlabel("Durée d'activitées en secondes")
        ax.xaxis.set_major_locator(plt.MultipleLocator(60))

        plt.barh(list_act,tpsmoy_par_act,0.5, color='b')

        plt.show()