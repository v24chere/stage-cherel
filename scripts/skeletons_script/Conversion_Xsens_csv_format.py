import numpy as np
import json
import scipy.interpolate
import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
from pprint import pprint
import argparse
import re
import pandas as pd
######################################################################################################
##This script converts the data of the Xsens which are in xml format to a json format in a txt file.##
##Only the positions are taken.

parser = argparse.ArgumentParser(description='Writing a Video from frames')
parser.add_argument('-i','--input', type=str, default='./Skeletons/s1/Experiment21/s1_Ep21_001.mvnx',
                        help="path/name.mvnx du fichier a lire")
parser.add_argument('-o', "--output", default="./output/xsens_time.csv", type=str,
                        help="path/name.csv du fichier a produire")

args = parser.parse_args()

#Path of the file :
file_name=args.input
print(file_name)

#Getting the data
tree = ET.parse(file_name)
root = tree.getroot()

position={}

common_body_parts=['Head', 'lAnkle', 'lElbow', 'lHip', 'lKnee', 'lShoulder', 'lWrist', 'mShoulder', 'rAnkle', 'rElbow', 'rHip', 'rKnee', 'rShoulder', 'rWrist']

body_parts={"T8":5,"Head":7,"RightShoulder":8,"RightUpperArm":9,"RightForeArm":10,"RightHand":11,"LeftShoulder":12,"LeftUpperArm":13,"LeftForeArm":14,"LeftHand":15,"RightUpperLeg":16,"RightLowerLeg":17,"RightFoot":18,"LeftUpperLeg":20,"LeftLowerLeg":21,"LeftFoot":22}

data={'positions':{}} #New format of data
positions=data['positions']	#Matching the data in the same format of the Kinect, only the positions are taken
list_body_parts=list(body_parts.keys())
Indexs=[]
Times=[]

results=[["frame","time_sec"]+list_body_parts]

for frame in root[2][2][2:]:
    Indexs.append(frame.get("index"))
    time=frame.get("time")
    Times.append(time)
    positions[time]={}
    All_frame_positions=frame[1].text.split()
    newline =[time,int(float(time)/60)]
    for b_part in list_body_parts:
        part_id=int(body_parts[b_part])
        x=All_frame_positions[3*part_id-3]
        y=All_frame_positions[3*part_id-2]
        z=All_frame_positions[3*part_id-1]
        #positions[time][b_part]=[x,y,z]
        newline.append((x,y))
    results.append(newline.copy())

#Save the data in a txt file format
new_file_name=re.sub(".mvnx",".txt",file_name)

dataframe=pd.DataFrame(results)
dataframe.to_csv(args.output, sep='\t', encoding='utf-8', header=False, index=False)

#with open(new_file_name, 'w') as outfile:
#     json.dump(data, outfile, sort_keys = False, indent = 4,
#               ensure_ascii = False)
