import argparse
import pandas as pd
import sys
import os


#from utils.tools import generate_detections as gdet
import matplotlib.pyplot as plt
from datetime import datetime
import networkx as nx
import cv2
import numpy as np
import time


def duration(start,end):
    time_start= datetime.strptime(start, "%H:%M:%S.%f")
    time_end= datetime.strptime(end, "%H:%M:%S.%f")
    duration=time_end - time_start
    tot_sec =duration.total_seconds()
    return tot_sec
#format: location:(top_left X Y)(down_right X_Y) coordonnée en nombre de carrée on fait dans l'approximatif
dico_locations={"floor_livingroom":((-16.0,13.0),(-4.0,0.0)), "floor_dining_room":((-4.0,13.0),(10.0,0.0)), "floor_bathroom":((-28.0,13.0),(-16.0,5.0))
    , "floor_bedroom":((-16.0,0.0),(0.0,-8.0)), "floor_kitchen":((-28.0,5.0),(-16.0,-8.0)), "floor_entrance":((0.0,0.0),(6.0,-8.0))}

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui lit une video et track la personne dessus")
    parser.add_argument('-i', "--input",default="./videos/s10/experiment_1/s10_Ep1_salon_capture-000.mp4", type=str,
                        help="path du dossier IOT")
    parser.add_argument('-o', "--output", default="./videos/s10/experiment_1/first_frame_s10_Ep21.PNG", type=str,
                        help="path/name.csv du fichier a produire")

    args = parser.parse_args()
    # on verifie si on a un csv a traiter ou tout un fichier à parcourir


    # Charger la vidéo
    video_path = args.input
    cap = cv2.VideoCapture(video_path)

    # Vérifiez si la vidéo est ouverte correctement
    if not cap.isOpened():
        print(f"Erreur: Impossible d'ouvrir la vidéo : {args.input}")
        exit()
    else :
        print(f"On traite la vidéo {args.input}")

    ret, frame = cap.read()
    if not ret:
        raise ValueError("Impossible de lire la première frame de la vidéo")

    # Sauvegarder la première frame en tant qu'image
    cv2.imwrite(args.output, frame)

    # Libérer l'objet VideoCapture
    cap.release()

    print(f"Première frame extraite et sauvegardée à {args.output}")


