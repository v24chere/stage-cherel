import argparse
import pandas as pd
import numpy as np
import os
import cv2
from pytesseract import image_to_string

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui extrait la premiere frame, decoupe et lit la date et l'heure il a besoin de tesseract : https://github.com/UB-Mannheim/tesseract/wiki puis pip install pytesseract (penser a ajouter tesseract au Path)")
    parser.add_argument('-i', "--input", type=str,
                        help="path du dossier contenant les videos à traiter integralement ")
    parser.add_argument('-a', "--apart", default="",type=str,
                        help="donner un path de dossier pour enregistrer les temps de debut apart dans ce dossier(avec la même arborescence que l'input)")
    # parser.add_argument("--replace",type=bool,default=False, help="si on change le csv d'entrée au lieu de creer une copie dans output")

    args = parser.parse_args()



    for root, dirs, files in os.walk(args.input):
        for file in files:
            if file.endswith("salon_capture-000.mp4") and not file.startswith('.'):
                file_full_path = os.path.join(root, file)
                vidcap = cv2.VideoCapture(file_full_path)
                success,image=vidcap.read()
                if success:
                    print("on traite "+file)
                    image_date=image[30:68,952:1133]
                    txt_date = image_to_string(image_date, config="--psm 7")
                    txt_date=txt_date.strip()
                    print("on lit la date : " + txt_date)

                    image_time=image[30:68,1137:1283]
                    txt_time = image_to_string(image_time, config="--psm 7")
                    txt_time = txt_time.strip().replace(" ","").replace(":","-")
                    print("on lit le temps : " + txt_time+"\n")

                    if args.apart == "":
                        #cv2.imwrite(file_full_path.split(".mp4")[0]+"_firstframe.jpg", image)
                        cv2.imwrite(file_full_path.split(".mp4")[0] + "_firstframe_date_"+txt_date+".jpg", image_date)
                        cv2.imwrite(file_full_path.split(".mp4")[0] + "_firstframe_time_"+txt_time+".jpg", image_time)
                    else:
                        os.makedirs(args.apart+root)
                        cv2.imwrite(args.apart+file_full_path.split(".mp4")[0] + "_firstframe_date_" + txt_date + ".jpg",
                                    image_date)
                        cv2.imwrite(args.apart+file_full_path.split(".mp4")[0] + "_firstframe_time_" + txt_time + ".jpg",
                                    image_time)
                #print("on verifie :"+file+" dans " + file_full_path )


