import argparse
import pandas as pd
import sys
import os
from math import sqrt
import cv2
import numpy as np
import matplotlib.pyplot as plt

from datetime import datetime
import networkx as nx
import cv2
import numpy as np
import time
from time import sleep


def duration(start,end):
    time_start= datetime.strptime(start, "%H:%M:%S.%f")
    time_end= datetime.strptime(end, "%H:%M:%S.%f")
    duration=time_end - time_start
    tot_sec =duration.total_seconds()
    return tot_sec
#format: location:(top_left X Y)(down_right X_Y) coordonnée en nombre de carrée on fait dans l'approximatif
dico_locations={"floor_livingroom":((-16.0,13.0),(-4.0,0.0)), "floor_dining_room":((-4.0,13.0),(10.0,0.0)), "floor_bathroom":((-28.0,13.0),(-16.0,5.0))
    , "floor_bedroom":((-16.0,0.0),(0.0,-8.0)), "floor_kitchen":((-28.0,5.0),(-16.0,-8.0)), "floor_entrance":((0.0,0.0),(6.0,-8.0))}

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui lit une video et track la personne dessus")
    parser.add_argument('-i', "--input",default="./videos/s10/experiment_1/first_frame_s10_Ep21.PNG", type=str,
                        help="path du dossier IOT")
    parser.add_argument('-o', "--output", default="./output/Tracking/", type=str,
                        help="path/name.csv du fichier a produire")
    parser.add_argument('-p', "--plan", default="./videos/extrait_exemple_visualisation_redim.png", type=str,
                        help="path/name/ plan sur lequel afficher les points")

    parser.add_argument('-w', "--show", default=True, action="store_false",
                        help="activer pour ne pas qu'on montre le resultat en temps réel")
    parser.add_argument('-t', "--hauteur", default=482, type=int,
                        help="hauteur d'arrivée")
    parser.add_argument('-l', "--largeur", default=861, type=int,
                        help="largeur d'arrivée")
    parser.add_argument('-c', "--coordinate",
                        default="output/Tracking/coord_csv/confidence_5/s10_Ep1_salon_capture-000_coord.csv", type=str,
                        help="dossier de coordonnées à traiter")
    parser.add_argument('-r', "--results",
                        default="output/Tracking/form_coord/confidence_5/", type=str,
                        help="dossier ou mettre le resultat NO pour pas enregistrer de resultat")
    args = parser.parse_args()
    # Charger l'image

    image_path = args.input
    image = cv2.imread(image_path)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    plan_path = args.plan
    plan = cv2.imread(plan_path)
    plan = cv2.cvtColor(plan, cv2.COLOR_BGR2RGB)
    # Dimensions de l'image
    h, w = image.shape[:2]
    # w=w*(38/21) # ratio hauteur largeur ~1.8
    diff = (h * (17 / 38)) / 2

    # Fonction pour enregistrer les points cliqués
    points = []


    def onclick(event):
        if event.xdata and event.ydata:
            points.append((event.xdata, event.ydata))
            plt.plot(event.xdata, event.ydata, 'ro')
            plt.axvline(x=event.xdata, color='red', linestyle=':')
            plt.axhline(y=event.ydata, color='red', linestyle=':')
            plt.draw()


    # Afficher l'image et enregistrer les clics
    fig, ax = plt.subplots()
    fig.suptitle('Pointer les coins et milieu de mur de la piece. ', fontsize=16)
    plt.title("dans le sens horaire et en commençant en haut à gauche", fontsize=14)
    plt.xlabel('fermer la fenêtre une fois finit', fontsize=16)
    ax.imshow(image)
    plt.axvline(x=w/2, color='blue', linestyle='--')
    #plt.axhline(y=h / 2, color='blue', linestyle='--')

    #plt.axhline(y=diff, color='black', linestyle='--')
    #plt.axhline(y=h-diff, color='black', linestyle='--')

    cid = fig.canvas.mpl_connect('button_press_event', onclick)
    plt.show()

    # Afficher les points marqués
    # points = (coin sup gauche,mur sup,coin sup droit,mur droit,coin inf droit,mur inf,coin inf gauche,mur gauche)

    #print("Points marqués:", points)

    # Points idéaux dans l'image de base
    ideal_points_orig = [
        (0, diff),
        (w / 2, diff),  # Milieu du mur supérieur
        (w, diff),
        (w, h / 2),  # Milieu du mur droit
        (w, h-diff),
        (w / 2, h-diff),  # Milieu du mur inférieur
        (0, h-diff),
        (0, h / 2)  # Milieu du mur gauche
    ]
    ideal_center_orig = (w / 2, h / 2)
    # Points idéaux que l'on veut atteindre
    ideal_points = [
        (0, 0),
        (args.largeur / 2, 0),  # Milieu du mur supérieur
        (args.largeur, 0),
        (args.largeur, args.hauteur / 2),  # Milieu du mur droit
        (args.largeur, args.hauteur ),
        (args.largeur / 2, args.hauteur ),  # Milieu du mur inférieur
        (0, args.hauteur),
        (0, args.hauteur)  # Milieu du mur gauche
    ]
    # on calcul le centre comme centre du rectangle former par les quatre coins
    centre = ((points[0][0]+points[2][0]+points[4][0]+points[6][0])/4,(points[0][1]+points[2][1]+points[4][1]+points[6][1])/4)
    ideal_center= (args.largeur/2,args.hauteur/2)
    def centrer(center,dot):
        (xc, yc) = center
        (xa, ya) = dot
        return (xa-xc,ya-yc)
    def decentrer(center,dot):
        (xc, yc) = center
        (xa, ya) = dot
        return (xa+xc,ya+yc)

    ideal_points_centrer = [centrer(ideal_center,dot) for dot in ideal_points]
    points_centrer = [centrer(centre, dot) for dot in points]
    #on aligne les points au milieu des murs avec le centre
    points_centrer[1] = (0, points_centrer[1][1])
    points_centrer[3] = (points_centrer[3][0], 0)
    points_centrer[5] = (0, points_centrer[5][1])
    points_centrer[7] = (points_centrer[7][0], 0)
    #on aligne les coins
    y_haut = (points_centrer[0][1]+points_centrer[2][1]) / 2
    x_droit = (points_centrer[2][0]+points_centrer[4][0]) / 2
    y_bas = (points_centrer[4][1] + points_centrer[6][1]) / 2
    x_gauche = (points_centrer[6][0] + points_centrer[0][0]) / 2
    points_centrer[0] = (x_gauche,y_haut)
    points_centrer[2] = (x_droit,y_haut)
    points_centrer[4] = (x_droit,y_bas)
    points_centrer[6] = (x_gauche,y_bas)
    def centre_bis_rayon(verti,coin_centrer,mur_centrer):
        (xc,yc)=(0,0) #centre
        (xa,ya)=coin_centrer
        (xm,ym)=mur_centrer
        if verti:
            xcBis=xc
            # ycBis à déterminer
            AC=(xa)**2
            MC=(xm)**2
            ycBis=(ya**2-ym**2+AC-MC)/(2*(ya-ym))
            return (xcBis,ycBis),sqrt((xm-xcBis)**2 + (ym-ycBis)**2)
        else:
            # xcBis à déterminer
            ycBis = yc
            AC = (ya)**2
            MC = (ym)**2
            xcBis = (xa**2 - xm**2 + AC - MC) / (2 * (xa - xm))
            return (xcBis, ycBis), sqrt((xm - xcBis)**2 + (ym - ycBis)**2)

    cBis_haut,R_haut=centre_bis_rayon(True,points_centrer[0],points_centrer[1])
    cBis_bas, R_bas = centre_bis_rayon(True, points_centrer[4], points_centrer[5])
    cBis_droit, R_droit = centre_bis_rayon(False, points_centrer[2], points_centrer[3])
    cBis_gauche, R_gauche = centre_bis_rayon(False, points_centrer[6], points_centrer[7])


    def find_intersections(cBis, r, start_seg, end_seg):
        (cx,cy)=cBis
        (x1,y1)=start_seg
        (x2, y2) = end_seg
        # Direction vector of the segment
        dx, dy = x2 - x1, y2 - y1

        # Quadratic coefficients
        A = dx ** 2 + dy ** 2
        B = 2 * (dx * (x1 - cx) + dy * (y1 - cy))
        C = (x1 - cx) ** 2 + (y1 - cy) ** 2 - r ** 2

        # Discriminant
        discriminant = B ** 2 - 4 * A * C

        if discriminant < 0:
            return []  # No intersection

        # Calculate the two solutions of the quadratic equation
        sqrt_discriminant = np.sqrt(discriminant)
        t1 = (-B + sqrt_discriminant) / (2 * A)
        t2 = (-B - sqrt_discriminant) / (2 * A)

        # Determine the intersection points
        points = (x1 + t1 * dx,y1 + t1 * dy)

        if 0 <= t2 <= 1:
            points = (x1 + t2 * dx,y1 + t2 * dy)

        return points
    def extension(dot,point_courbe,point_obj):#à partir d'un point
        #, du point sur le rectangle objectif et sur la courbe de base
        (x_obj,y_obj)=point_obj
        (x_courbe,y_courbe)=point_courbe
        (x,y)=dot
        return (x*x_obj/x_courbe,y*y_obj/y_courbe)

    def circle_to_rect(dot_centrer):
        (xd,yd)=dot_centrer
        (xCoinhg,yCoinhg)=points_centrer[0]
        (xCoinhd, yCoinhd) = points_centrer[2]
        (_,y_haut_obj) = points_centrer[1]
        (x_droit_obj,_) = points_centrer[3]
        (_,y_bas_obj) = points_centrer[5]
        (x_gauche_obj,_) = points_centrer[7]
        if yd-(xd*yCoinhg/xCoinhg)<0: #on place par cadran
            if yd-(xd*yCoinhd/xCoinhd)<0:#en dessous des deux droites donc en haut du schema
                # droite : x=x_ideal_haut et cercle : y=y_cercle(x,cBis_haut,R_haut )
                # à intersecter avec y=x*(yd/xd)
                point_rect=(y_haut_obj*(xd/yd),y_haut_obj)
                points_courbe=find_intersections(cBis_haut,R_haut,point_rect,(0,0))
                return extension(dot_centrer,points_courbe,point_rect)
            else:#en dessous de la premiere diagonale mais au dessus de l'autre donc à droite
                point_rect = (x_droit_obj, x_droit_obj*(yd/xd))
                points_courbe =find_intersections(cBis_haut,R_haut,point_rect,(0,0))
                return extension(dot_centrer,points_courbe,point_rect)
        else:
            if yd-(xd*yCoinhd/xCoinhd)<0: #au dessus de la premiere diagonale mais en desous de l'autre donc à gauche
                point_rect = (x_gauche_obj, x_gauche_obj * (yd / xd))
                points_courbe =find_intersections(cBis_haut,R_haut,point_rect,(0,0))
                return extension(dot_centrer,points_courbe,point_rect)
            else:#au dessus des deux droites donc en bas du schema
                point_rect = (y_bas_obj * (xd / yd), y_bas_obj)
                points_courbe =find_intersections(cBis_bas, R_bas,point_rect,(0,0))
                return extension(dot_centrer,points_courbe,point_rect)





    def etirer_comprimer(dot_rect):
        (_,ymhaut)=points_centrer[1]
        (xmdroit, _) = points_centrer[3]
        (_, ymbas) = points_centrer[5]
        (xmgauche, _) = points_centrer[7]
        (_, ideal_ymhaut) = ideal_points_centrer[1]
        (ideal_xmdroit, _) = ideal_points_centrer[3]
        (_, ideal_ymbas) = ideal_points_centrer[5]
        (ideal_xmgauche, _) = ideal_points_centrer[7]
        (xd,yd)=dot_rect
        x_res,y_res=0,0
        if xd>0:
            x_res=xd*ideal_xmdroit/xmdroit
        if xd<0:
            x_res=xd*ideal_xmgauche/xmgauche
        if yd>0:
            y_res=yd*ideal_ymbas/ymbas
        if yd<0:
            y_res=yd*ideal_ymhaut/ymhaut
        return (x_res,y_res)

    def transposer_un_dot(dot):
        dot_centrer=centrer(centre,dot)
        #dot_rect=circle_to_rect(dot_centrer)
        dot_eti=etirer_comprimer(dot_centrer)#dot_rect)
        return decentrer(ideal_center,dot_eti)

    #Frame,id,X,Y,Time
    dfb = pd.read_csv(args.coordinate, delimiter=",", header=0)
    print(dfb)
    times=[float(t) for t in dfb["Time"]]
    #print(times[::100])
    compt = [0]
    pointeur = [0]
    couleurs=["green","gray","blue","black"]
    def onclick_coord(event):
        print(f"Showing time {compt[0]}sec to {compt[0]+60}sec")
        compt[0] += 60
        while int(dfb["Time"][pointeur[0]])<=compt[0]:
            x=dfb["X"][pointeur[0]]+100
            y=dfb["Y"][pointeur[0]]+100
            (x_trans,y_trans)=transposer_un_dot((x,y))
            plt.plot(x_trans,y_trans, 'o',color=couleurs[pointeur[0]%4])
            ax1.plot(dfb["X"][pointeur[0]], dfb["Y"][pointeur[0]], 'o', color=couleurs[pointeur[0] % 4])
            if (pointeur[0]+1)==len(dfb["Time"]):
                break
            pointeur[0]+=1
            plt.draw()
            sleep(0.005)
        ax2.set_title(f"up to Time:{compt[0]}sec")

    if args.results != "" and args.results != "NO":
        res = [["Frame","id","X","Y","Time"]]
        path_res=args.coordinate.replace("coord_csv","form_coord")#.replace("coord.csv",f"h{args.hauteur}_l{args.largeur}.csv")
        print(path_res)
        for i in range(len(dfb["Time"])):
            x=(float(dfb["X_top_left"][i])+float(dfb["X_bot_right"][i]))/2
            y=(float(dfb["Y_top_left"][i])+float(dfb["Y_bot_right"][i]))/2
            (x_tr,y_tr)=transposer_un_dot((x,y))
            res.append([f"{dfb["Frame"][i]}",f"{dfb["id"][i]}",f"{x_tr}",f"{y_tr}",f"{dfb["Time"][i]}"])
        data = pd.DataFrame(res)
        data.to_csv(path_res, sep='\t', encoding='utf-8', header=False, index=False)

    if args.show:
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 6))
        ax1.imshow(image)
        ax1.set_title('Image originale')
        ax2.imshow(cv2.cvtColor(plan, cv2.COLOR_BGR2RGB))
        ax2.set_title('Image transformée (Cercle à Carré)')
        compteur=0
        for dot in points:
            color_sep="red"
            if compteur>7:
                color_sep="blue"
            compteur+=1
            (x,y)=dot
            ax1.plot(x, y, 'o',color=color_sep)
            (x, y) = transposer_un_dot(dot)
            ax2.plot(x, y, 'o', color=color_sep)
        cid = fig.canvas.mpl_connect('button_press_event', onclick_coord)


        plt.show()

