import argparse
import pandas as pd
import sys
import os

import cv2
import numpy as np
import matplotlib.pyplot as plt

from datetime import datetime
import networkx as nx
import cv2
import numpy as np
import time


def duration(start,end):
    time_start= datetime.strptime(start, "%H:%M:%S.%f")
    time_end= datetime.strptime(end, "%H:%M:%S.%f")
    duration=time_end - time_start
    tot_sec =duration.total_seconds()
    return tot_sec
#format: location:(top_left X Y)(down_right X_Y) coordonnée en nombre de carrée on fait dans l'approximatif
dico_locations={"floor_livingroom":((-16.0,13.0),(-4.0,0.0)), "floor_dining_room":((-4.0,13.0),(10.0,0.0)), "floor_bathroom":((-28.0,13.0),(-16.0,5.0))
    , "floor_bedroom":((-16.0,0.0),(0.0,-8.0)), "floor_kitchen":((-28.0,5.0),(-16.0,-8.0)), "floor_entrance":((0.0,0.0),(6.0,-8.0))}

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui lit une video et track la personne dessus")
    parser.add_argument('-i', "--input",default="./videos/s10/experiment_1/first_frame_s10_Ep21.PNG", type=str,
                        help="path du dossier IOT")
    parser.add_argument('-o', "--output", default="./output/Tracking/", type=str,
                        help="path/name.csv du fichier a produire")
    parser.add_argument('-m', "--models", default="./models/", type=str,
                        help="path/name/ du dossier conteannt les models, poids et autre")
    parser.add_argument('-s', "--skip", default=0, type=int,
                        help="nombre de frame à sauter")
    parser.add_argument('-w', "--show", default=True, action="store_false",
                        help="activer pour ne pas qu'on montre le resultat en temps réel")
    parser.add_argument('-c', "--confidence", default=20, type=int,
                        help="nombre de frame à sauter")
    args = parser.parse_args()
    # Charger l'image

    image_path = args.input
    image = cv2.imread(image_path)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    # Dimensions de l'image
    h, w = image.shape[:2]
    # w=w*(38/21)
    diff = (h * (17 / 38)) / 2

    # Fonction pour enregistrer les points cliqués
    points = []


    def onclick(event):
        if event.xdata and event.ydata:
            points.append((event.xdata, event.ydata))
            plt.plot(event.xdata, event.ydata, 'ro')
            plt.draw()


    # Afficher l'image et enregistrer les clics
    fig, ax = plt.subplots()
    fig.suptitle('Pointer les coins et milieu de mur de la piece. ', fontsize=16)
    plt.title("dans le sens horaire et en commençant en haut à gauche", fontsize=14)
    plt.xlabel('fermer la fenêtre une fois finit', fontsize=16)
    ax.imshow(image)
    plt.axvline(x=w/2, color='blue', linestyle='-')
    plt.axhline(y=h / 2, color='blue', linestyle='-')

    plt.axhline(y=diff, color='black', linestyle='-')
    plt.axhline(y=h-diff, color='black', linestyle='-')

    cid = fig.canvas.mpl_connect('button_press_event', onclick)
    plt.show()

    # Afficher les points marqués
    print("Points marqués:", points)



    # Points idéaux (coins de l'image)
    ideal_points = np.float32([
        [0, diff],
        [w / 2, diff],  # Milieu du mur supérieur
        [w, diff],
        [w, h / 2],  # Milieu du mur droit
        [w, h-diff],
        [w / 2, h-diff],  # Milieu du mur inférieur
        [0, h-diff],
        [0, h / 2]  # Milieu du mur gauche
    ])

    # Convertir les points marqués en format numpy
    src_points = np.float32(points)

    # Calculer la matrice de transformation
    homography_matrix, _ = cv2.findHomography(src_points, ideal_points)
    # Sauvegarder la matrice pour une utilisation ultérieure
    np.save(args.output+'homography_matrix.npy', homography_matrix)

    undistorted_image = cv2.warpPerspective(image, homography_matrix, (w, h))

    # Afficher l'image corrigée
    plt.imshow(undistorted_image)
    plt.axhline(y=diff, color='black', linestyle='-')
    plt.axhline(y=h - diff, color='black', linestyle='-')
    plt.title("Image corrigée")
    plt.show()

    # Sauvegarder l'image transformée
    cv2.imwrite(args.output+'s10_undist_image.jpg', cv2.cvtColor(undistorted_image, cv2.COLOR_BGR2RGB))


