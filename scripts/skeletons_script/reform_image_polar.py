import argparse
import pandas as pd
import sys
import os

import cv2
import numpy as np
import matplotlib.pyplot as plt

from datetime import datetime
import networkx as nx
import cv2
import numpy as np
import time


def duration(start,end):
    time_start= datetime.strptime(start, "%H:%M:%S.%f")
    time_end= datetime.strptime(end, "%H:%M:%S.%f")
    duration=time_end - time_start
    tot_sec =duration.total_seconds()
    return tot_sec
#format: location:(top_left X Y)(down_right X_Y) coordonnée en nombre de carrée on fait dans l'approximatif
dico_locations={"floor_livingroom":((-16.0,13.0),(-4.0,0.0)), "floor_dining_room":((-4.0,13.0),(10.0,0.0)), "floor_bathroom":((-28.0,13.0),(-16.0,5.0))
    , "floor_bedroom":((-16.0,0.0),(0.0,-8.0)), "floor_kitchen":((-28.0,5.0),(-16.0,-8.0)), "floor_entrance":((0.0,0.0),(6.0,-8.0))}

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui lit une video et track la personne dessus")
    parser.add_argument('-i', "--input",default="./videos/s10/experiment_1/first_frame_s10_Ep21.PNG", type=str,
                        help="path du dossier IOT")
    parser.add_argument('-o', "--output", default="./output/Tracking/", type=str,
                        help="path/name.csv du fichier a produire")
    parser.add_argument('-m', "--models", default="./models/", type=str,
                        help="path/name/ du dossier conteannt les models, poids et autre")
    parser.add_argument('-s', "--skip", default=0, type=int,
                        help="nombre de frame à sauter")
    parser.add_argument('-w', "--show", default=True, action="store_false",
                        help="activer pour ne pas qu'on montre le resultat en temps réel")
    parser.add_argument('-c', "--confidence", default=20, type=int,
                        help="nombre de frame à sauter")
    args = parser.parse_args()
    # Charger l'image circulaire (cercle)
    image_path = args.input  # Remplacez par le chemin de votre image
    img = cv2.imread(image_path)


    # Fonction pour remapper les points d'un cercle à un carré
    def circle_to_square_mapping(src):
        h, w = src.shape[:2]
        # Créer une nouvelle image carrée
        size = max(h, w)
        dst = np.zeros((size, size, 3), dtype=src.dtype)

        # Définir le centre et le rayon du cercle
        cx, cy = w // 2, h // 2
        radius = min(cx, cy)

        for y in range(size):
            for x in range(size):
                # Normaliser les coordonnées dans le carré (-1 à 1)
                nx = 2 * (x / (size - 1)) - 1
                ny = 2 * (y / (size - 1)) - 1

                # Calculer l'angle et la distance polaire
                angle = np.arctan2(ny, nx)
                distance = np.sqrt(nx ** 2 + ny ** 2)

                # Remapper la distance du carré au cercle
                if distance <= 1:
                    src_x = int(cx + radius * nx / distance)
                    src_y = int(cy + radius * ny / distance)
                    if 0 <= src_x < w and 0 <= src_y < h:
                        dst[y, x] = src[src_y, src_x]

        return dst




    # Vérifiez que l'image est correctement chargée
    if img is None:
        raise FileNotFoundError(f"Image non trouvée à l'emplacement {image_path}")

    # Appliquer la transformation pour transformer le cercle en carré
    square_img = circle_to_square_mapping(img)

    # Afficher l'image originale et l'image transformée
    plt.figure(figsize=(12, 6))

    plt.subplot(1, 2, 1)
    plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    plt.title('Image originale')

    plt.subplot(1, 2, 2)
    plt.imshow(cv2.cvtColor(square_img, cv2.COLOR_BGR2RGB))
    plt.title('Image transformée (Cercle à Carré)')

    plt.show()

    # Sauvegarder l'image transformée
    #cv2.imwrite('/mnt/data/square_image.jpg', square_img)


