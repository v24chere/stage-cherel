import argparse
import pandas as pd
# import numpy as np
import os
import matplotlib.pyplot as plt
from datetime import datetime
import networkx as nx

def duration(start,end):
    time_start= datetime.strptime(start, "%H:%M:%S.%f")
    time_end= datetime.strptime(end, "%H:%M:%S.%f")
    duration=time_end - time_start
    tot_sec =duration.total_seconds()
    return tot_sec
#format: location:(top_left X Y)(down_right X_Y) coordonnée en nombre de carrée on fait dans l'approximatif
dico_locations={"floor_livingroom":((-16.0,13.0),(-4.0,0.0)), "floor_dining_room":((-4.0,13.0),(10.0,0.0)), "floor_bathroom":((-28.0,13.0),(-16.0,5.0))
    , "floor_bedroom":((-16.0,0.0),(0.0,-8.0)), "floor_kitchen":((-28.0,5.0),(-16.0,-8.0)), "floor_entrance":((0.0,0.0),(6.0,-8.0))}

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui lit la base IOT donnée par le args.input et regroue les capteurs par activitées dans args.output")
    parser.add_argument('-i', "--input",default="./IOT/", type=str,
                        help="path du dossier IOT")
    parser.add_argument('-o', "--output", default="./output/floor_time.csv", type=str,
                        help="path/name.csv du fichier a produire")


    # parser.add_argument("--replace",type=bool,default=False, help="si on change le csv d'entrée au lieu de creer une copie dans output")

    args = parser.parse_args()

    # on verifie si on a un csv a traiter ou tout un fichier à parcourir

    list_capteur_timecode=[["exp","activity","time","sensor"]]

    for root, dirs, files in os.walk(args.input):
        for file in files:
            if file.endswith(".csv"):
                file_full_path = os.path.join(root, file)
                #print("on traite :"+file_full_path)

                dfb = pd.read_csv(file_full_path,delimiter="\t",header=None,names=[ "datetime", "sensor", "event_type", "value", "activity"])
                start=0
                if dfb["sensor"][0]=="camera_start":
                    start=1
                while dfb['activity'][start]=="start_experiment":
                    start+=1

                num_ligne=dfb["sensor"].size
                new_line=[]
                #print(new_line)

                for i in range(start,num_ligne):
                    capteur = dfb['sensor'][i]
                    valeur = str(dfb['value'][i])
                    activity = dfb['activity'][i]
                    timecode = dfb['datetime'][i].split(" ")[1]
                    if valeur == "ON" or valeur == "OPEN":
                        new_line = [file,activity,timecode, capteur]
                        list_capteur_timecode.append(new_line)

                            #print(new_line)



    data = pd.DataFrame(list_activite_et_capteur_associe)
    data.to_csv(args.output, sep='\t', encoding='utf-8', header=False, index=False)
