import argparse
import pandas as pd
# import numpy as np
import os
import matplotlib.pyplot as plt
from datetime import datetime
import networkx as nx
import cv2
import numpy as np

def duration(start,end):
    time_start= datetime.strptime(start, "%H:%M:%S.%f")
    time_end= datetime.strptime(end, "%H:%M:%S.%f")
    duration=time_end - time_start
    tot_sec =duration.total_seconds()
    return tot_sec
#format: location:(top_left X Y)(down_right X_Y) coordonnée en nombre de carrée on fait dans l'approximatif
dico_locations={"floor_livingroom":((-16.0,13.0),(-4.0,0.0)), "floor_dining_room":((-4.0,13.0),(10.0,0.0)), "floor_bathroom":((-28.0,13.0),(-16.0,5.0))
    , "floor_bedroom":((-16.0,0.0),(0.0,-8.0)), "floor_kitchen":((-28.0,5.0),(-16.0,-8.0)), "floor_entrance":((0.0,0.0),(6.0,-8.0))}

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui lit une video et track la personne dessus")
    parser.add_argument('-i', "--input",default="./videos/s10/experiment_1/s10_Ep1_salon_capture-000.mp4", type=str,
                        help="path du dossier IOT")
    parser.add_argument('-o', "--output", default="./output/", type=str,
                        help="path/name.csv du fichier a produire")
    parser.add_argument('-s', "--skip", default=400, type=int,
                        help="nombre de frame à sauter")
    args = parser.parse_args()

    # on verifie si on a un csv a traiter ou tout un fichier à parcourir

    # Charger le modèle de détection de personnes MobileNet SSD
    model = './models/faster_rcnn_frozen_inference_graph.pb'  # chemin vers deploy.prototxt
    config = './models/faster_rcnn_inception_v2_coco_2018_01_28.pbtxt'  # chemin vers mobilenet_iter_73000.caffemodel
    net = cv2.dnn.readNetFromTensorflow(model, config)

    # Charger la vidéo
    video_path = args.input
    cap = cv2.VideoCapture(video_path)

    # Vérifiez si la vidéo est ouverte correctement
    if not cap.isOpened():
        print("Erreur: Impossible d'ouvrir la vidéo.")
        exit()

    # Obtenir le framerate de la vidéo
    fps = cap.get(cv2.CAP_PROP_FPS)
    if fps == 0:
        print("Erreur: Impossible d'obtenir le framerate de la vidéo. On met sur 60 par defaut")
        fps=60
        #exit()

    # Initialiser le tracker CSRT
    tracker = cv2.TrackerCSRT_create()

    # Variable pour stocker les coordonnées, numéro de frame et temps
    coordinates = []
    tracking = False
    frame_number = 0

    # Variable pour contrôler le saut des frames de début
    if args.skip != 0:
        skip_initial_frames = True
        frames_to_skip = args.skip  # Nombre de frames à sauter au début
    else:
        skip_initial_frames = False
    roi = None

    while True:
        ret, frame = cap.read()
        if not ret:
            break

        frame_number += 1
        time_in_seconds = frame_number / fps

        if skip_initial_frames and frame_number <= frames_to_skip:
            continue

        if not tracking:
            # Permettre à l'utilisateur de sélectionner manuellement la ROI
            if roi is None:
                cv2.imshow('Frame', frame)
                roi = cv2.selectROI('Frame', frame, fromCenter=False, showCrosshair=True)
                cv2.destroyWindow('Frame')
                tracker.init(frame, roi)
                tracking = True
        else:
            # Mettre à jour le tracker
            success, bbox = tracker.update(frame)
            if success:
                x, y, w, h = [int(v) for v in bbox]
                center_x = x + w // 2
                center_y = y + h // 2
                coordinates.append((frame_number, center_x, center_y, time_in_seconds))

                # Dessiner le rectangle de la bounding box
                cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2, 1)
                cv2.circle(frame, (center_x, center_y), 5, (0, 255, 0), -1)

        # Afficher la frame
        cv2.imshow('Tracking', frame)

        # Appuyez sur 'q' pour quitter
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()

    # Sauvegarder les coordonnées dans un fichier
    np.savetxt(args.output+'coordinates.csv', np.array(coordinates), delimiter=',', header='Frame,X,Y,Time', comments='')

    print(f"Traçage terminé. Coordonnées sauvegardées dans '{args.output}coordinates.csv'.")
