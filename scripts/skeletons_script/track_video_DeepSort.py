import argparse
import pandas as pd
import sys
import os

# Ajouter le chemin du module deep_sort_pytorch
sys.path.append(os.path.abspath("C:/Users/v24chere/PycharmProjects/IMT_stage/repo_git_test/deep_sort_pytorch"))
from utils.parser import get_config
from deep_sort import DeepSort

from absl import app, flags
from absl.flags import FLAGS
from deep_sort.sort import  nn_matching ,preprocessing
from deep_sort.sort.detection import Detection
from deep_sort.sort.tracker import Tracker
from deep_sort.deep.feature_extractor import Extractor
#from utils.tools import generate_detections as gdet
import matplotlib.pyplot as plt
from datetime import datetime
import networkx as nx
import cv2
import numpy as np
import time


def duration(start,end):
    time_start= datetime.strptime(start, "%H:%M:%S.%f")
    time_end= datetime.strptime(end, "%H:%M:%S.%f")
    duration=time_end - time_start
    tot_sec =duration.total_seconds()
    return tot_sec
#format: location:(top_left X Y)(down_right X_Y) coordonnée en nombre de carrée on fait dans l'approximatif
dico_locations={"floor_livingroom":((-16.0,13.0),(-4.0,0.0)), "floor_dining_room":((-4.0,13.0),(10.0,0.0)), "floor_bathroom":((-28.0,13.0),(-16.0,5.0))
    , "floor_bedroom":((-16.0,0.0),(0.0,-8.0)), "floor_kitchen":((-28.0,5.0),(-16.0,-8.0)), "floor_entrance":((0.0,0.0),(6.0,-8.0))}

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui lit une video et track la personne dessus")
    parser.add_argument('-i', "--input",default="./videos/s10/experiment_1/s10_Ep1_salon_capture-000.mp4", type=str,
                        help="path du dossier IOT")
    parser.add_argument('-o', "--output", default="./output/Tracking/", type=str,
                        help="path/name.csv du fichier a produire")
    parser.add_argument('-m', "--models", default="./models/", type=str,
                        help="path/name/ du dossier conteannt les models, poids et autre")
    parser.add_argument('-s', "--skip", default=0, type=int,
                        help="nombre de frame à sauter")
    parser.add_argument('-w', "--show", default=True, action="store_false",
                        help="activer pour ne pas qu'on montre le resultat en temps réel")
    parser.add_argument('-c', "--confidence", default=20, type=int,
                        help="nombre de frame à sauter")
    args = parser.parse_args()
    conf= float(args.confidence)/100
    video_name_split=args.input.replace("\\","/").split("/")
    out_coord=args.output+f"coord_csv/confidence_{args.confidence}/{video_name_split[-1].replace(".mp4","_coord.csv")}"
    out_video=args.output+f"video_box/confidence_{args.confidence}/{video_name_split[-1].replace(".mp4","_box.mp4")}"
    os.makedirs(args.output+f"coord_csv/confidence_{args.confidence}/", exist_ok=True)
    os.makedirs(args.output+f"video_box/confidence_{args.confidence}/", exist_ok=True)

    # on verifie si on a un csv a traiter ou tout un fichier à parcourir

    weights_path = args.models+'yolov4.weights'
    config_path = args.models+'yolov4.cfg'
    deep_sort_model = args.models+'mars-small128.pb'
    # Charger SSD MobileNetV3
    # Charger le modèle YOLO
    net = cv2.dnn.readNetFromDarknet(config_path, weights_path)
    # Commenter ou enlever les lignes de préférences GPU
    # net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
    # net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)

    # Charger le modèle DeepSORT
    cfg = get_config()
    cfg.merge_from_file('repo_git_test/deep_sort_pytorch/configs/deep_sort.yaml')
    extractor = Extractor(deep_sort_model, use_cuda=False)

    metric = nn_matching.NearestNeighborDistanceMetric("cosine", 0.4, None)
    tracker = Tracker(metric)

    # Charger la vidéo
    video_path = args.input
    cap = cv2.VideoCapture(video_path)

    # Configuration de l'enregistrement vidéo
    output_path = out_video
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')  # Codec pour le fichier de sortie
    fps = cap.get(cv2.CAP_PROP_FPS)
    frame_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    frame_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    out = cv2.VideoWriter(output_path, fourcc, fps, (frame_width, frame_height))

    # Vérifiez si la vidéo est ouverte correctement
    if not cap.isOpened():
        print(f"Erreur: Impossible d'ouvrir la vidéo : {args.input}")
        exit()
    else :
        print(f"On traite la vidéo {args.input}")
    # Obtenir le framerate de la vidéo
    fps = cap.get(cv2.CAP_PROP_FPS)
    if fps == 0:
        print("Erreur: Impossible d'obtenir le framerate de la vidéo.")
        exit()

    frame_number = 0
    coordinates = []

    while True:
        ret, frame = cap.read()
        if not ret:
            break

        frame_number += 1
        if frame_number>args.skip:
            time_in_seconds = frame_number / fps

            height, width, channels = frame.shape

            # Prétraiter l'image pour YOLO
            blob = cv2.dnn.blobFromImage(frame, 1 / 255.0, (416, 416), swapRB=True, crop=False)
            net.setInput(blob)
            layer_names = net.getLayerNames()

            # Correction de l'obtention des noms de couches
            try:
                output_layers = [layer_names[i - 1] for i in net.getUnconnectedOutLayers()]
            except TypeError:
                output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

            detections = net.forward(output_layers)

            boxes = []
            confidences = []
            class_ids = []

            for output in detections:
                for detection in output:
                    scores = detection[5:]
                    class_id = np.argmax(scores)
                    confidence = scores[class_id]
                    if confidence > conf and class_id == 0:  # Filtrer uniquement les personnes (class_id = 0)
                        box = detection[0:4] * np.array([width, height, width, height])
                        (centerX, centerY, w, h) = box.astype("int")
                        x = int(centerX - (w / 2))
                        y = int(centerY - (h / 2))
                        boxes.append([x, y, int(w), int(h)])
                        confidences.append(float(confidence))
                        class_ids.append(class_id)

            # Appliquer la suppression non maximale
            idxs = cv2.dnn.NMSBoxes(boxes, confidences, conf, 0.3)

            if len(idxs) > 0:
                idxs = idxs.flatten()
                detections = []
                features = extractor(frame, boxes)
                for i in idxs:
                    (x, y) = (boxes[i][0], boxes[i][1])
                    (w, h) = (boxes[i][2], boxes[i][3])
                    box = [x, y, w, h]
                    score = confidences[i]
                    label = class_ids[i]
                    feature = features[i]
                    detec=Detection(box, score,label, feature)

                    detections.append(detec)

                # Mise à jour du tracker
                tracker.predict()
                tracker.update(detections)

            # Collecter les informations de tracking
            for track in tracker.tracks:
                if not track.is_confirmed() or track.time_since_update > 1:
                    continue
                bbox = track.to_tlbr()
                track_id = track.track_id
                coordinates.append((frame_number, track_id, int(bbox[0]), int(bbox[1]),int(bbox[2]), int(bbox[3]), time_in_seconds))
                cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), (0, 255, 0), 2)
                cv2.putText(frame, f"ID: {track_id}", (int(bbox[0]), int(bbox[1]) - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                            (0, 255, 0), 2)

            # Écriture de la frame dans le fichier de sortie
            out.write(frame)

            # Afficher la frame
            if args.show:
                cv2.imshow('Tracking', frame)
                # Appuyez sur 'q' pour quitter
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break

    cap.release()
    out.release()
    cv2.destroyAllWindows()

    # Sauvegarder les coordonnées dans un fichier
    np.savetxt(out_coord,  np.array(coordinates), delimiter=',', header='Frame,id,X_top_left,Y_top_left,X_bot_right,Y_bot_right,Time', comments='')

    print(f"Traçage terminé. Coordonnées sauvegardées dans '{out_coord}'. Vidéo sauvegardée dans '{out_video}'")
