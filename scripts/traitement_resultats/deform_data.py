import argparse
import pandas as pd
# import numpy as np
import os
import matplotlib.pyplot as plt
import random

def combine(cap,max):   #on fait une combinaison de max parmis cap sous forme d'un tableau avec exactement max True
    res = [False] * (cap-max) +[True]*max
    random.shuffle(res)
    return res

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui lit la base IOT et donne des info dessus")
    parser.add_argument('-i', "--input",default="output/form_dyn_s10_test.csv", type=str,
                        help="path du fichier produit par scrap")
    parser.add_argument('-o', "--output",default="output/deform_dyn_s10_test.csv", type=str,
                        help="path/name du fichier a produire")
    # parser.add_argument("--replace",type=bool,default=False, help="si on change le csv d'entrée au lieu de creer une copie dans output")

    args = parser.parse_args()

    # on verifie si on a un csv a traiter ou tout un fichier à parcourir


    results=[["exp","activity","sensor"]]
    dfb = pd.read_csv(args.input,delimiter="\t",header=0)

    start=0
    num_ligne=dfb["exp"].size

    for i in range(num_ligne):
        #print(dfb["exp"][i]+" "+str(dfb["num_sensors"][i])+" "+dfb["activity"][i])
        exp=dfb["exp"][i]
        activity=dfb['activity'][i]
        num_cap=int(dfb["num_sensors"][i])
        new_line = [dfb["exp"][i]]
        for cap in range(num_cap):
            results.append([exp,activity,dfb[f"sensors{cap}"][i]])



    #print("nombre d'activité total "+ str(act_total))
    #print("nombre de capteur par activité moyen "+str(cap_total/act_total))

    #lst=[[1,2],["test1","test2"]]
    data=pd.DataFrame(results)
    data.to_csv(args.output, sep='\t', encoding='utf-8', header=False, index=False)

    #fig, ax = plt.subplots()

    #plt.title("Quantitée d'activité en fonction du nombre d'activations de capteurs")
    #ax.set_xlabel("Nombre d'activations de capteur au cours de l'activitée")
    #ax.set_ylabel("Nombre d'activitées")
    #ax.plot(X, Y)
    #plt.bar(X, Y, 0.5, color='b')

    #plt.title("Quantitée d'activités par type d'activitée")
    #ax.set_ylabel("Nom d'activitée")
    #ax.set_xlabel("Nombre d'activitées")
    #ax.plot(list_act,num_par_act)
    #plt.barh(list_act,num_par_act,0.5, color='b')

    #plt.show()