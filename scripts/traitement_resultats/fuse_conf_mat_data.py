import argparse
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import random

from pathlib import Path


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui lit la base IOT et donne des info dessus")
    parser.add_argument('-i', "--input",default="./output/LSTM_res/", type=str,
                        help="path du fichier produit par scrap")
    parser.add_argument('-o', "--output",default="./output/LSTM_res/all_mat/", type=str,
                        help="path/name du fichier a produire")
    parser.add_argument('-e', "--epoch", default="100", type=str,
                        help="nombre d'epoch")

    # parser.add_argument("--replace",type=bool,default=False, help="si on change le csv d'entrée au lieu de creer une copie dans output")

    args = parser.parse_args()

    csv_mat=[]
    fichier=args.input+"epos"+args.epoch+"/"

    for root, dirs, files in os.walk(fichier):
        for file in files:
            if file.endswith("confus_mat.csv"):
                csv_mat.append(file)


    mat = [pd.read_csv(fichier+csv_path, index_col=0) for csv_path in csv_mat]

    result = mat[0].copy()
    for matrix in mat[1:]:
        result+=matrix
    result.to_csv(args.output+"all_epos"+args.epoch+"_confus_mat.csv")

    #dfb = pd.read_csv(args.input,delimiter="\t",header=0,names=scrap_header)



    #cm_df = pd.DataFrame(cm, index=label_encoder.classes_, columns=label_encoder.classes_)
    #cm_df.to_csv(args.output+f"epos{num_epochs}/"+"classe_s"+args.subject+f"_epos{num_epochs}_confus_mat.csv")





    #fig, ax = plt.subplots()

    #plt.title("Quantitée d'activité en fonction du nombre d'activations de capteurs")
    #ax.set_xlabel("Nombre d'activations de capteur au cours de l'activitée")
    #ax.set_ylabel("Nombre d'activitées")
    #ax.plot(X, Y)
    #plt.bar(X, Y, 0.5, color='b')

    #plt.title("Quantitée d'activités par type d'activitée")
    #ax.set_ylabel("Nom d'activitée")
    #ax.set_xlabel("Nombre d'activitées")
    #ax.plot(list_act,num_par_act)
    #plt.barh(list_act,num_par_act,0.5, color='b')

    #plt.show()