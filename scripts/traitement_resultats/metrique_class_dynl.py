import argparse
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import random
import ast

from pathlib import Path

def add_detail_line(compact,detail):
    compact[0]+=1
    for i in range(6,len(detail)):
        compact[i]+=detail[i]
    return ()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui parcourt des csv segment pour donner des info dessus")
    parser.add_argument('-i', "--input",default="./output/LSTM_res/built_segment/", type=str,
                        help="path du dossier a lire")
    parser.add_argument('-o', "--output", default="", type=str,
                        help="path du dossier ou mettre les metriques, si laisser vide ce sera args.input/metriques/")

    parser.add_argument('-p', "--palier", default=5, type=int,
                        help="distance entre pallier")
    parser.add_argument('-t', "--test", default=False, type=bool,
                        help="test")
    # parser.add_argument("--replace",type=bool,default=False, help="si on change le csv d'entrée au lieu de creer une copie dans output")

    args = parser.parse_args()

    #
    dic_epo_plus_type={}
    results_detail=[["exp","epoch_segment","epoch_class","type_combi","recouvrement_global","recouvrement_moyen","capteur_total","capteur_correct","segment_total","precision_cumulée","num_100%","num_>=75%","num_>=50%","num_>=25%","num_>0%"]]

    results_compact = []
    compact_inter_results=["num_exp","epoch_segment","epoch_class","type_combi","recouvrement_global","recouvrement_moyen", "capteur_total","capteur_correct","segment_total","precision_cumulée","num_100%","num_>=75%","num_>=50%","num_>=25%","num_>0%"]


    for root, dirs, files in os.walk(args.input.replace("/","\\")):
        for file in files:
            if file.startswith("s") and file.endswith(".csv"):
                file_full_path = os.path.join(root, file)
                list_carac=file_full_path.split("\\")
                #print(list_carac)
                taille= len(list_carac)
                type_combi=list_carac[taille-2]
                num_epoch_class = list_carac[taille-3].split("epos")[1]
                num_epoch_segment = list_carac[taille - 4].split("epos")[1]
                sujet = list_carac[taille-1].split(".csv")[0]
                #print(f"type combi : {type_combi} ; num epoch class : {num_epoch_class} ; num epoch class : {num_epoch_segment} ; sujet : {sujet}")

                #headers : exp , num_sensors , predictions , precision , activities , sensors0 , sensors1
                class_csv = pd.read_csv(file_full_path, delimiter="\t", header=0)
                num_ligne = len(class_csv["exp"])

                inter_results = []
                if type_combi != compact_inter_results[3] or num_epoch_segment != compact_inter_results[1] or num_epoch_class != compact_inter_results[2]:
                    if compact_inter_results[1] != "epoch_segment":
                        compact_inter_results[4] = compact_inter_results[7] / compact_inter_results[6]  # recouvrement global = capteur correct/capteur total
                        compact_inter_results[5] = compact_inter_results[9] / compact_inter_results[8]  # recouvrement moyen = somme des precision/segment total
                    results_compact.append(compact_inter_results)
                    compact_inter_results = [0, num_epoch_segment,num_epoch_class, type_combi, 0,0,0,0,0,0,0,0,0,0,0]

                exp_results = [class_csv["exp"][0], num_epoch_segment,num_epoch_class, type_combi, 0,0,0,0,0,0,0,0,0,0,0]

                for i in range(num_ligne):
                    activities = ast.literal_eval(class_csv["activities"][i])
                    num_sensors = int(class_csv["num_sensors"][i])
                    precision = float(class_csv["precision"][i])

                    exp_results[6]+=num_sensors
                    exp_results[7]+=num_sensors*precision
                    exp_results[8] += 1
                    exp_results[9] += precision

                    if precision>0.0:
                        exp_results[14]+=1
                        if precision >= 0.25:
                            exp_results[13] += 1
                            if precision >= 0.50:
                                exp_results[12] += 1
                                if precision >= 0.75:
                                    exp_results[11] += 1
                                    if precision >= 1.0:
                                        exp_results[10] += 1

                    if i < num_ligne - 1:
                        if class_csv["exp"][i + 1] != exp_results[0]:
                            exp_results[4]= exp_results[7]/exp_results[6]   # recouvrement global = capteur correct/capteur total
                            exp_results[5] = exp_results[9]/exp_results[8]  # recouvrement moyen = somme des precision/segment total
                            inter_results.append(exp_results.copy())
                            exp_results = [class_csv["exp"][i+1], num_epoch_segment,num_epoch_class, type_combi, 0,0,0,0,0,0,0,0,0,0,0]

                exp_results[4] = exp_results[7] / exp_results[6]  # recouvrement global = capteur correct/capteur total
                exp_results[5] = exp_results[9] / exp_results[8]  # recouvrement moyen = somme des precision/segment total
                inter_results.append(exp_results.copy())

                for e_results in inter_results:
                    results_detail.append(e_results.copy())
                    add_detail_line(compact_inter_results, e_results.copy())

    compact_inter_results[4] = compact_inter_results[7] / compact_inter_results[6]  # recouvrement global = capteur correct/capteur total
    compact_inter_results[5] = compact_inter_results[9] / compact_inter_results[8]  # recouvrement moyen = somme des precision/segment total
    results_compact.append(compact_inter_results)

    data = pd.DataFrame(results_detail)
    out = args.input + "metriques/"
    if args.output != "":
        out = args.output
    os.makedirs(out,exist_ok=True)
    data.to_csv(out+"metrique_local_detail.csv", sep='\t', encoding='utf-8', header=False, index=False)

    data = pd.DataFrame(results_compact)
    data.to_csv(out + "metrique_local_compact.csv", sep='\t', encoding='utf-8', header=False, index=False)

    # print(f'Recouvrement global correct: {100 * correct_total / total_cap:.2f}%')
    # print(f'Recouvrement moyen correct des segments: {100 * precision_moyen / total_segment:.2f}%')
    # print(f'Pourcentage de segment recouvert correctement à au moins 50%: {100 * num_above_50 / total_segment:.2f}%')
    # print(f'Pourcentage de segment recouvert correctement à plus de 0%: {100 * num_above_0 / total_segment:.2f}%')

