import argparse
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import random

from pathlib import Path


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui parcourt des csv segment pour donner des info dessus")
    parser.add_argument('-i', "--input",default="./output/LSTM_segment_res/", type=str,
                        help="path du dossier a parcourir")
    parser.add_argument('-o', "--output", default="", type=str,
                        help="path de l'emplacement ou sauvegarder , si laisser vide ce sera input/metrique_segment.csv")

    # parser.add_argument("--replace",type=bool,default=False, help="si on change le csv d'entrée au lieu de creer une copie dans output")

    args = parser.parse_args()

    results=[["file_path","activities_total","activities_detecté","perfects","decalage_total","decalage_moyen","retard_total","retard_moyen","avance_total","avance_moyen","non_detection","erreur_isolée"]]

    for root, dirs, files in os.walk(args.input):
        for file in files:
            if file.startswith("segment_"):
                file_full_path = os.path.join(root, file)
                print(f"on traite {file_full_path}")
                segment_csv = pd.read_csv(file_full_path,delimiter="\t",header=0)
                predi=segment_csv["prediction"]
                activ=segment_csv["activities"]
                num_perfects=0
                num_retards=0
                taille_retards=0
                num_avances=0
                taille_avances=0
                erreurs=0
                num_non_detection=0
                taille_non_detection=0

                compteur=0
                first_acti_true=0
                first_predi_true=0
                for i in range(len(predi)-2):
                    compteur+=1
                    if activ[i] and first_acti_true==0:
                        first_acti_true=compteur
                    if predi[i] and first_predi_true==0:
                        if predi[i+1] and predi[i+2]:
                            first_predi_true=compteur
                        else:
                            erreurs+=1
                    if (i==len(predi)-3 or not activ[i]) and first_acti_true != 0:#on passe a l'activité suivante
                        if first_predi_true==0:
                            num_non_detection+=1
                            taille_non_detection+=first_acti_true
                        else:
                            if first_predi_true==first_acti_true:
                                num_perfects+=1
                            elif first_predi_true>first_acti_true:
                                num_retards+=1
                                taille_retards+= first_predi_true-first_acti_true
                            else:
                                num_avances+=1
                                taille_avances+= first_acti_true-first_predi_true
                        compteur = 0
                        first_predi_true=0
                        first_acti_true =0
                results.append([file_full_path,
                                num_retards + num_avances + num_perfects+num_non_detection,
                                num_retards+num_avances+num_perfects,
                                num_perfects,
                                num_avances+num_retards,
                                (taille_avances+taille_retards)/(num_retards+num_avances+num_perfects),
                                num_retards,
                                taille_retards/num_retards,
                                num_avances,
                                taille_avances/num_avances,
                                num_non_detection,
                                erreurs
                                ])


    data = pd.DataFrame(results)
    if args.output=="":
        out=args.input+"metrique_segment.csv"
    else:
        out=args.output
    data.to_csv(out,sep='\t', encoding='utf-8', header=False, index=False)

    #dfb = pd.read_csv(args.input,delimiter="\t",header=0,names=scrap_header)



    #cm_df = pd.DataFrame(cm, index=label_encoder.classes_, columns=label_encoder.classes_)
    #cm_df.to_csv(args.output+f"epos{num_epochs}/"+"classe_s"+args.subject+f"_epos{num_epochs}_confus_mat.csv")





    #fig, ax = plt.subplots()

    #plt.title("Quantitée d'activité en fonction du nombre d'activations de capteurs")
    #ax.set_xlabel("Nombre d'activations de capteur au cours de l'activitée")
    #ax.set_ylabel("Nombre d'activitées")
    #ax.plot(X, Y)
    #plt.bar(X, Y, 0.5, color='b')

    #plt.title("Quantitée d'activités par type d'activitée")
    #ax.set_ylabel("Nom d'activitée")
    #ax.set_xlabel("Nombre d'activitées")
    #ax.plot(list_act,num_par_act)
    #plt.barh(list_act,num_par_act,0.5, color='b')

    #plt.show()