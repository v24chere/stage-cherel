import argparse
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import random
import ast

from pathlib import Path

def add_detail_line(compact,detail):
    compact[0]+=1
    for i in range(3,len(detail)):
        compact[i]+=detail[i]
    return ()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui parcourt des csv segment pour donner des info dessus")
    parser.add_argument('-i', "--input",default="./output/LSTM_segment_res/", type=str,
                        help="path du dossier a lire")
    parser.add_argument('-o', "--output", default="", type=str,
                        help="path du dossier ou mettre les metriques, si laisser vide ce sera args.input/metriques/")
    parser.add_argument('-x', "--max", default=100, type=int,
                        help="borne max incluse pour les bons segments ")
    parser.add_argument('-n', "--min", default=50, type=int,
                        help="borne inf incluse pour les bons segments")
    parser.add_argument('-p', "--palier", default=5, type=int,
                        help="distance entre pallier")
    parser.add_argument('-t', "--test", default=False, type=bool,
                        help="test")
    # parser.add_argument("--replace",type=bool,default=False, help="si on change le csv d'entrée au lieu de creer une copie dans output")

    args = parser.parse_args()

    #
    dic_epo_plus_type={}
    results_detail=[["exp","epochs","type_combi","segment_total"]+[f">{p}%" for p in range(args.max,args.min-args.palier,-args.palier)]]

    results_compact = []
    compact_inter_results=["num_exp","epochs","type_combi", "segment_total"] + [f">={p}%" for p in range(args.max, args.min - args.palier, -args.palier)]

    paliers = [1000, 1000, 1000, 1000] + [p for p in range(args.max, args.min - args.palier, -args.palier)]

    for root, dirs, files in os.walk(args.input.replace("/","\\")):
        for file in files:
            if file.startswith("form_segment"):
                file_full_path = os.path.join(root, file)
                list_carac=file_full_path.split("\\")
                #print(list_carac)
                taille= len(list_carac)
                type_combi=list_carac[taille-3]
                num_epoch = list_carac[taille-2].split("epos")[1]
                sujet = list_carac[taille-1].split("form_segment_")[1].split(".csv")[0]
                #print(f"type combi : {type_combi} ; num epoch : {num_epoch} ; sujet : {sujet}")

                segment_csv = pd.read_csv(file_full_path, delimiter="\t", header=0)
                num_ligne = len(segment_csv["exp"])
                inter_results = []
                if type_combi!=compact_inter_results[2] or num_epoch!=compact_inter_results[1]:
                    results_compact.append(compact_inter_results)
                    compact_inter_results = [0,num_epoch,type_combi, 0] + [0 for p in
                                                                range(args.max, args.min - args.palier, -args.palier)]
                exp_results = [segment_csv["exp"][0],num_epoch,type_combi, 0] + [0 for p in
                                                            range(args.max, args.min - args.palier, -args.palier)]

                for i in range(num_ligne):
                    activities = ast.literal_eval(segment_csv["activities"][i])
                    taille = len(activities)
                    dico_act = {}
                    # print(activities)
                    for act in activities:
                        value = dico_act.get(act, 0)
                        dico_act[act] = value + 1
                    # print(dico_act)
                    max = ""
                    max_value = 0
                    for cle, valeur in dico_act.items():
                        if valeur > max_value:
                            max = cle
                            max_value = valeur
                    pourcent = int((max_value / taille) * 100)

                    exp_results[3] += 1
                    for j in range(4, len(exp_results)):
                        if pourcent >= paliers[j]:
                            exp_results[j] += 1

                    if i < num_ligne - 1:
                        if segment_csv["exp"][i + 1] != exp_results[0]:
                            inter_results.append(exp_results.copy())
                            exp_results = [segment_csv["exp"][i + 1],num_epoch,type_combi, 0] + [0 for p in
                                                                            range(args.max, args.min - args.palier,
                                                                                  -args.palier)]
                inter_results.append(exp_results.copy())

                for e_results in inter_results:
                    results_detail.append(e_results.copy())
                    add_detail_line(compact_inter_results,e_results.copy())

    results_compact.append(compact_inter_results)

    data = pd.DataFrame(results_detail)
    out = args.input + "metriques/"
    if args.output != "":
        out = args.output
    os.makedirs(out,exist_ok=True)
    data.to_csv(out+"metrique_local_detail.csv", sep='\t', encoding='utf-8', header=False, index=False)

    data = pd.DataFrame(results_compact)
    data.to_csv(out + "metrique_local_compact.csv", sep='\t', encoding='utf-8', header=False, index=False)


    if args.test:
        segment_csv = pd.read_csv(args.input, delimiter="\t", header=0)
        num_ligne=len(segment_csv["exp"])
        inter_results=[]


        exp_results=[segment_csv["exp"][0],0] + [0 for p in range(args.max,args.min-args.palier,-args.palier)]

        for i in range(num_ligne):
            activities=ast.literal_eval(segment_csv["activities"][i])
            taille=len(activities)
            dico_act={}
            #print(activities)
            for act in activities:
                value=dico_act.get(act,0)
                dico_act[act]=value+1
            #print(dico_act)
            max=""
            max_value=0
            for cle,valeur in dico_act.items():
                if valeur>max_value:
                    max=cle
                    max_value=valeur
            pourcent=int((max_value/taille)*100)

            exp_results[1]+=1
            for j in range(2,len(exp_results)):
                if pourcent>=paliers[j]:
                    exp_results[j]+=1

            if i<num_ligne-1:
                if segment_csv["exp"][i+1]!=exp_results[0]:
                    inter_results.append(exp_results.copy())
                    exp_results=[segment_csv["exp"][i+1],0] + [0 for p in range(args.max,args.min-args.palier,-args.palier)]
        inter_results.append(exp_results)

        for e_results in inter_results:
            results_detail.append(e_results)

        data = pd.DataFrame(results_detail)
        out=args.input+"metriques/"
        if args.output!="":
            out=args.output
        data.to_csv(out,sep='\t', encoding='utf-8', header=False, index=False)


        for e_results in inter_results:
            fig, ax = plt.subplots()

            plt.suptitle("Nombre de segment dont une activitée depasse un certain pourcentage")
            plt.title(f"{e_results[0]} total:{e_results[1]}")
            ax.set_xlabel("Pourcentage minimum de l'activitée principal")
            ax.set_ylabel("Nombre de segments")
            ax.plot(paliers[2:], [e_results[1]for i in range(len(e_results)-2)],color='r')
            plt.bar(paliers[2:], e_results[2:], 0.5, color='b')

            #plt.show()