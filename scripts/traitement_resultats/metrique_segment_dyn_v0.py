import argparse
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import random
import ast

from pathlib import Path

def repart(pourcent,paliers):
    palier_list=paliers.split("_")
    palier_top=int(palier_list[0])
    palier_mid = int(palier_list[1])
    palier_bot = int(palier_list[2])
    if pourcent >= palier_top:
        return 0
    elif pourcent >= palier_mid:
        return 1
    elif pourcent >= palier_bot:
        return 2
    else:
        return 3

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="algo qui parcourt des csv segment pour donner des info dessus")
    parser.add_argument('-i', "--input",default="./output/form_dyn_s10_lstm.csv", type=str,
                        help="path du dossier a lire")
    parser.add_argument('-o', "--output", default="./output/metrique_form_dyn_s10_lstm.csv", type=str,
                        help="path de l'emplacement ou sauvegarder , si laisser vide ce sera input/metrique_segment.csv")
    parser.add_argument('-x', "--max", default=100, type=int,
                        help="borne max incluse pour les bons segments ")
    parser.add_argument('-n', "--min", default=70, type=int,
                        help="borne inf incluse pour les bons segments")
    parser.add_argument('-p', "--palier", default=20, type=int,
                        help="distance entre pallier")

    # parser.add_argument("--replace",type=bool,default=False, help="si on change le csv d'entrée au lieu de creer une copie dans output")

    args = parser.parse_args()

    #                                  A_B_C             A%                B%                 C%
    results=[["exp","segment_total","palier","segment_bon","segment_decent","segment_mediocre","segment_mauvais"],
             ["nom de l'exp","nombre de segment d'activitée detectée","format : A_B_C","segment ou l'activitée majoritaire est à au moins A%" , "...au moins B%","...au moins C%","...moins de C%"]]

    segment_csv = pd.read_csv(args.input, delimiter="\t", header=0)
    num_ligne=len(segment_csv["exp"])
    inter_results=[]

    exp_results=[[segment_csv["exp"][0],0,f"{p}_{p-args.palier}_{p-2*args.palier}",0,0,0,0] for p in range(args.min,args.max+1,5)]

    for i in range(num_ligne):
        activities=ast.literal_eval(segment_csv["activities"][i])
        taille=len(activities)
        dico_act={}
        #print(activities)
        for act in activities:
            value=dico_act.get(act,0)
            dico_act[act]=value+1
        #print(dico_act)
        max=""
        max_value=0
        for cle,valeur in dico_act.items():
            if valeur>max_value:
                max=cle
                max_value=valeur
        pourcent=int((max_value/taille)*100)
        for ligne in exp_results:
            ligne[1]+=1
            ligne[3+repart(pourcent,ligne[2])] += 1
        if i<num_ligne-1:
            if segment_csv["exp"][i+1]!=exp_results[0][0]:
                inter_results.append(exp_results)
                exp_results=[[segment_csv["exp"][i+1],0,f"{p}_{p-args.palier}_{p-2*args.palier}",0,0,0,0] for p in range(args.min,args.max+1,5)]
    inter_results.append(exp_results)

    for e_results in inter_results:
        for ligne in e_results:
            results.append(ligne)

    data = pd.DataFrame(results)
    out=args.output
    data.to_csv(out,sep='\t', encoding='utf-8', header=False, index=False)

    #dfb = pd.read_csv(args.input,delimiter="\t",header=0,names=scrap_header)



    #cm_df = pd.DataFrame(cm, index=label_encoder.classes_, columns=label_encoder.classes_)
    #cm_df.to_csv(args.output+f"epos{num_epochs}/"+"classe_s"+args.subject+f"_epos{num_epochs}_confus_mat.csv")





    #fig, ax = plt.subplots()

    #plt.title("Quantitée d'activité en fonction du nombre d'activations de capteurs")
    #ax.set_xlabel("Nombre d'activations de capteur au cours de l'activitée")
    #ax.set_ylabel("Nombre d'activitées")
    #ax.plot(X, Y)
    #plt.bar(X, Y, 0.5, color='b')

    #plt.title("Quantitée d'activités par type d'activitée")
    #ax.set_ylabel("Nom d'activitée")
    #ax.set_xlabel("Nombre d'activitées")
    #ax.plot(list_act,num_par_act)
    #plt.barh(list_act,num_par_act,0.5, color='b')

    #plt.show()