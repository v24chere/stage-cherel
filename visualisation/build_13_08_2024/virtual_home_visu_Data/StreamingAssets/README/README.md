# Smart-Home Visualisation

## Introduction

Le but de cette partie est de produire une visualisation claire des données IOT. 
Contrairement à la vidéo fisheye prise lors de l'experience,
Une visualisation permet de se restraindre au point de vue de la machine.
Le tout en incorporant les capteurs peu ou pas visibles comme les capteurs de présence.

Les visualisations ont été produites avec le projet Unity de cette section, sous Windows.   
Pour le reprendre il suffit de télécharger l'[UnityHub](https://unity.com/fr/download) (Il va vous proposer de telecharger un editeur unity recents vous pouvez ignorer).   
Ensuite vous pouvez localiser le projet /virtual_home_visu via le bouton Add.   
Il devrais ensuite détecter automatiquement la version du projet et vous dire de la telecharger. (il y a parfois un pop-up d'autorisation si vous n'acceptez pas assez vite ça plante et il faut annuler puis relancer le telechargement) 
Si la détection échoue : la version est 2021.3.32f1.
Il faudra probablement aller la chercher dans les archives de Unity puis utiliser "Locate" dans l'onglet "Installs" d'UnityHub pour finir son installation.
 
Si vous avez des problèmes avec  [emgu.cv](https://www.emgu.com/) supprimez le dossier Plugins dans Assets ainsi que le script EmguCV_Recorder.cs.
Vous aurez alors quelques warning "missing behavior" des objets qui ne retrouvent plus le script associé mais le reste devrais fonctionner, vous ne pourrez juste pas enregistrer de vidéo.

## Entrées - Sorties (inputs - outputs)

Par defaut dans l'editeur les scripts cherchent les csv/video... dans Assets.
Si la scene menu a été ouverte un dossier dans le persistent path est crée et utilisée (les données stockée dans le projet y sont copiée).   
Un bouton permet de localiser le dossier mais normalement ç'est de cette forme là : C:\Users\<user>\AppData\LocalLow\DefaultCompany\virtual_home_visu

## Visualisation - icones

L'application unity produit une visualisation mp4 de ces données en parcourant le .csv,
les icônes/le sol s'allument quand le capteur correspondant s'active. 


![](screenshots/extrait_exemple_visualisation.png)

Dans cet frame exemple vous pouvez voir que la lumiere, le sol et le robinet de la salle de bain sont activés tandis que l'activité est "bathe"

le temps est présent pour la synchronisation mais n'as pas d'ancrage sur le réelle (par exemple il y a des scénarios de matinée réalisé à 15h).   
C'est pour cela qu'il n'y a que minutes et secondes.

## Visualisation - synchronisation 

On aimerait afficher pour comparer les vidéos fisheye à coté. Cependant 
ces dernières ne sont pas synchronisées avec les fichier csv. De plus nous n'avons pas accès au temps de depart des vidéos.   
Dans un premier temps j'ai mis un bouton de synchronisation manuel mais c'est assez fastidieux.   
Puis j'ai trouvé qu'un des capteurs "xaal-lab.hmi.basic.state" avait l'air de coincider avec le début de vidéo mais un décalage de plus ou moins une seconde persiste. C'est faible mais suffisant pour être désagreable.   
J'ai donc finis par exploiter le temps indiqué en haut à droite sur les vidéos fisheye, sur fonds noir uni donc facile à lire.
J'ai utiliser le module [tesseract](https://github.com/UB-Mannheim/tesseract/wiki) (si vous l'installez n'oubliez pas d'ajouter le dossier d'installation à votre path et de redémarrez, mais aussi de lancer "pip --install pytesseract").   
Plus de détail dans le script [extract_video_time_start.py](../scripts/skeletons_script/extract_video_time_start.py).   
En resumé il prend la première frame des vidéos fisheye et découpe la partie haute droite et lit la date/temps qu'il enregistre via un nom de dossier.


## Visualisation - curseur sujet

Lors des expériences des données Xsens , Kinect ainsi qu'une vidéo fisheye en topdown ont été enregistrées.

Et donc un plus pour la visualisation serait d'ajouter un curseur suivant la position du sujet au cours de l'expérience.


### Vidéo fish eye

Dans un premier temps j'ai utilisé les vidéos fisheye et l'algorithme de tracking DEEPSORT pour obtenir les coordonnées du sujet.

La vidéo étant prise du dessus est très deformée (grand angle pour voir toute la pièce) donc les algos de tracking ont beaucoup de mal.
Heureusement il n'y a pas ou presque pas de parasite sur les vidéos. 
Et donc avec un seuil de confiance très bas on a de très bons résultats sans trop d'abération.

Cepandant même avec un tracking réussi les coordonnées restent déformées.
Il faut donc les convertir dans le format du plan.

Ce nouveau .csv est lu en même temps que le .csv des capteurs 
pour mettre à jour la position d'un curseur symbolisant la position du sujet.


## Enregistrement

Dans un premier temps j'ai utilisé le module Unity Recorder fourni. 
Cependant, il ne fonctionne que dans l'Editeur Unity et est plus conçu
pour des cinématiques/trailer. Difficile d'enregistrer plusieurs visualisations en simultanée.

Nous allons donc utiliser https://www.emgu.com/ (qui permet d'utiliser Open CV sous C#) 
pour enregistrer des vidéos en temps réel. J'ai fait un [rapide tuto d'instalation](https://gitlab.imt-atlantique.fr/v24chere/emgucv_unity/) en anglais 
si vous travaillez en open source et que vous ne voulez pas acheter [l'asset](https://assetstore.unity.com/packages/tools/behavior-ai/emgu-cv-v4-x-144853).

J'ai donc fait un petit script qui se lie à une caméra et enregistre ce qu'elle voit.   
Open CV traite en BGR donc il faut convertir en RGB, l'axe des ordonnées est inversée (Y decroit quand on monte) donc il faut un Flip.   
Autres problèmes qui récurrent avec les langues interprétées telles que C# : on ne nous demande pas de gérer la mémoire, c'est fait automatiquement et parfois mal.
Avec 30 screenshots par secondes qui sont transformées plusieurs fois on peut facilement atteindre 120 Mo/s de remplissage memoire.   

Le script evite les fuites memoire en lançant un thread alternatif qui reçoit dans une file d'attente tous ces screenshots et est charger de libérer sa mémoire proprement.
Il y a un cap à la taille de la liste qui vas mettre en pause le proccessus principal le temps de la vider.   
On peut aussi choisir entre .avi (peu couteux en cpu mais moins compact) et .mp4 (plus compact mais plus couteux).   
Cette file d'attente néccessite une fonction qui retarde l'arrêt pour terminer le thread. 
Mais attention : quand on arrête dans l'éditeur c'est un arrêt brutal qui ne se laisse pas retarder, ce qui fait planter l'éditeur.
Et donc il faut bien faire attention à lancer la fonction end_record() du script avant de fermer.


