using UnityEngine;
using Emgu.CV;
using Emgu.CV.CvEnum;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using System.Collections.Concurrent;

// Script permmettant d'enregistrer ce que voit la camera cameraToRecord dans outputFilePath 
// tant que control.playing est True (variable dans un global_controller qui suit quand la video et/ou le csv est en train d'être parcourue)
public class EmguCV_Recorder : MonoBehaviour
{
    public global_controller control;
    public Camera cameraToRecord;
    public string outputFilePath;
    private int frameWidth;
    private int frameHeight;
    public int frameRate = 30;
    

    //public bool playing = true;

    private VideoWriter videoWriter;
    private RenderTexture renderTexture;
    private Texture2D screenTexture;
    private Thread recordingThread;
    public ConcurrentQueue<Color32[]> frameQueue;
    //private bool isRecording = true;
    
    

    private void Start()
    {
        record_ended=false;
        // Initialize video writer
        frameWidth = Screen.width;
        frameHeight = Screen.height;

        //videoWriter = new VideoWriter(outputFilePath, VideoWriter.Fourcc('M', 'J', 'P', 'G'), frameRate, new System.Drawing.Size(frameWidth, frameHeight), true);
        videoWriter = new VideoWriter(outputFilePath, VideoWriter.Fourcc('H', '2', '6', '4'), frameRate, new Size(frameWidth, frameHeight), true);

        if (!videoWriter.IsOpened)
        {
            Debug.LogError("Failed to open video writer.");
            return;
        }

        // Create a RenderTexture and a Texture2D for capturing camera output
        renderTexture = new RenderTexture(frameWidth, frameHeight, 24);
        screenTexture = new Texture2D(frameWidth, frameHeight, TextureFormat.RGB24, false);

        // Initialize the frame queue
        frameQueue = new ConcurrentQueue<Color32[]>();

        // Start the recording thread
        recordingThread = new Thread(new ThreadStart(RecordVideo));
        recordingThread.Start();

        // Start capturing frames
        //isRecording = true;
    }

    bool was_playing=false;
    private float timeSinceLastCapture = 0f;
    public float captureInterval = 1f / 30f;
    private void Update(){
        
        if (control.playing)
            {timeSinceLastCapture += Time.unscaledDeltaTime;
            if (timeSinceLastCapture >= captureInterval)
                {
                    timeSinceLastCapture = 0f;
                    CaptureFrame();
                    if (frameQueue.Count>=10){ // we pause the main process if we are overwhelmed
                        control.playing=false;
                        was_playing=true;
                        Debug.Log("nombre de frame lors de la pause:"+frameQueue.Count);
                        }
                }
                
            }
        if (was_playing && frameQueue.Count<5){
            was_playing=false;
            control.playing=true;
        }
        if (record_ended && frameQueue.IsEmpty){
            control.gameObject.SetActive(false); // we deactivate the glob controller when the recording is finished
        }
    }

    
    private void CaptureFrame()
    {
        // Capture the camera output to the RenderTexture
        cameraToRecord.targetTexture = renderTexture;
        cameraToRecord.Render();
        cameraToRecord.targetTexture = null;

        // Read the RenderTexture into the Texture2D
        RenderTexture.active = renderTexture;
        screenTexture.ReadPixels(new Rect(0, 0, frameWidth, frameHeight), 0, 0);
        screenTexture.Apply();
        RenderTexture.active = null;

        // Enqueue the captured frame
        frameQueue.Enqueue(screenTexture.GetPixels32());
    }
    private bool record_ended=false;
    public void end_record(){
        record_ended=true;
        // Wait for the recording thread to finish
        if (recordingThread != null)
        {
            recordingThread.Join();
        }
        OnApplicationQuit();
    }
    private void OnApplicationQuit()
    {

        

        if (videoWriter != null)
        {
            videoWriter.Dispose();
        }

        // Release resources
        if (renderTexture != null)
        {
            renderTexture.Release();
            Destroy(renderTexture);
        }
        if (screenTexture != null)
        {
            Destroy(screenTexture);
        }
        //Debug.Log("memory released");
    }
    void RecordVideo()
    {
        bool not_started_yet=true; // we check for not playing/empty queue only after we have properly started not directly after the thread is started
        while (control.playing || !frameQueue.IsEmpty || was_playing|| not_started_yet)
        {
            if (frameQueue.TryDequeue(out Color32[] pixelData))
            {   
                not_started_yet=false;
                GCHandle handle = GCHandle.Alloc(pixelData, GCHandleType.Pinned);

                 using (Mat frame = new Mat(frameHeight, frameWidth, DepthType.Cv8U, 4, handle.AddrOfPinnedObject(), frameWidth * 4))
                {
                    // Convert BGRA to BGR
                    using (Mat bgrFrame = new Mat())
                    {
                        CvInvoke.CvtColor(frame, bgrFrame, ColorConversion.Bgra2Bgr);
                        CvInvoke.CvtColor(bgrFrame, bgrFrame, ColorConversion.Bgr2Rgb);

                        // Flip the image vertically
                        CvInvoke.Flip(bgrFrame, bgrFrame, FlipType.Vertical);

                        // Write the frame to the video
                        videoWriter.Write(bgrFrame);
                    }
                }

                handle.Free();
            }
            else
            {
                // If there are no frames in the queue, wait briefly before checking again
                Thread.Sleep(10);
            }
        }
    }
}
