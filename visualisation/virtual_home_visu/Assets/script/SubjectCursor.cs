using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using TMPro;

public class SubjectCursor : MonoBehaviour
{
    public global_controller g_master;
    string input_path;
    public GameObject sujet;
    public GameObject top_left;
    public GameObject bot_right;

    private char[] separators = {',','\t'};
    public double time;
    int csv_pointeur;
    int taille_csv;
    

    float x_left;
    float y_top;
    float x_right;
    float y_bot;
    //input_top_left = (0,0)
    float input_y_bot=482.0f;
    float input_x_right=861.0f;

    
    List<string> frame = new List<string>();     //
    List<string> id = new List<string>();        //
    List<string> X_coord = new List<string>();   //
    List<string> Y_coord = new List<string>();   //
    List<string> timestamp = new List<string>(); //time

    Vector3 newcoord(Vector3 input){
        float prop_x = input.x/input_x_right;
        float prop_y = input.y/input_y_bot;
        float x_res = x_left*(1-prop_x)+x_right*prop_x;
        float y_res = y_top*(1-prop_y)+y_bot*prop_y;
        //Debug.Log("prop_x:"+prop_x+" x_left:"+x_left+" x_right:"+x_right+"x_res :"+x_res);
        return new Vector3(x_res, y_res, input.z);
    }

    // Start is called before the first frame update
    void Start()
    {
        input_path=PlayerPrefs.GetString("input_path","Assets");

        time = 0.0;
        csv_pointeur = 0;
        x_left=top_left.transform.position.x;
        y_top=top_left.transform.position.y;
        x_right=bot_right.transform.position.x;
        y_bot=bot_right.transform.position.y;

        int s_num=g_master.s_num;
        int e_num=g_master.e_num;

        string[] le_csv =Directory.GetFiles(Path.Combine(input_path,"csv_tracking"),"s"+s_num+"_Ep"+e_num+"_salon_capture-000_coord.csv",SearchOption.AllDirectories);
        //Debug.Log("on a trouvée:"+le_csv[0]);
        if (le_csv.Length>0)
            {using(StreamReader reader = new StreamReader(le_csv[0]))
            {
                
                var header = reader.ReadLine(); //we skip the first line containing names of columns
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] values = line.Split(separators);
                    //Debug.Log("la ligne obtenue:"+line);
                    
                    frame.Add(values[0]);
                    id.Add(values[1]);
                    X_coord.Add(values[2]);
                    Y_coord.Add(values[3]);
                    timestamp.Add(values[4]);
                    
                }
        }}else{
            frame.Add("0");
            id.Add("0");
            X_coord.Add("1000");//we put the cursor way outside of frame if we don't find a tracking file
            Y_coord.Add("1000");
            timestamp.Add("0");
        }
        
        taille_csv=timestamp.Count;
        
    }
    
    double test;
    // Update is called once per frame
    void Update()
    {
        if(g_master.playing){
            time+=Time.deltaTime*g_master.time_speed;
        }
        //time+=Time.deltaTime;
        //if (csv_pointeur<10){csv_pointeur+=1;}
        while (csv_pointeur+2<taille_csv && Convert.ToDouble(timestamp[csv_pointeur+1].Replace(".",","))<time){
            csv_pointeur+=1;}


        
        sujet.transform.position=newcoord(
            new Vector3(Convert.ToSingle(X_coord[csv_pointeur].Replace(".",",")),
                        Convert.ToSingle(Y_coord[csv_pointeur].Replace(".",",")),
                        -101-100*g_master.clone_num));
            
    }
}
