using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using TMPro;
//using UnityEditor.Recorder;
//using UnityEditor;
using UnityEngine.Video;

public class global_controller : MonoBehaviour
{
    //public ScreenRecorder camera_visu;
    public EmguCV_Recorder recorder;
    public int record_clone;
    public int clone_num = 0;
    public Multi_record master;
    public SubjectCursor cursor;

    public int s_num;
    public int e_num;
    public string videos_time_start;
    string input_path;
    string output_path;
    
    int go_through;
    public VideoPlayer experiment_video;
    public lab_icon_controller lab_icons;
    
    int csv_pointeur;
    int csv_prediction_pointeur;
    int taille_csv;
    int taille_csv_prediction;
    public float time_speed;
    public bool playing;
    float temp_time;

    public void back_menu(){
        SceneManager.LoadScene("menu"); 
    }
    public void play(){
        experiment_video.Play();
        playing=true;    
    }
    public void pause(){
        experiment_video.Pause();
        playing=false;
    }
    public void mute(){
        experiment_video.SetDirectAudioMute(0,true);
    }
    public void unmute(){
        experiment_video.SetDirectAudioMute(0,false);
    }

    //section synchronisation et deplacement temporels
    int min_to_add=0;
    int sec_to_add=0;
    public TextMeshProUGUI min_txt;
    public TextMeshProUGUI sec_txt;

    public void change_min(int i){
        min_to_add+=i;
        min_txt.text=min_to_add.ToString();
    }
    public void change_sec(int i){
        sec_to_add+=i;
        sec_txt.text=sec_to_add.ToString();
    }
    public void change_sync(){
        experiment_video.time+=(float)(60*min_to_add+sec_to_add);
        cursor.time+=(float)(60*min_to_add+sec_to_add);
    }
    public void change_global(){
        experiment_video.time+=(float)(60*min_to_add+sec_to_add);
        time+=(float)(60*min_to_add+sec_to_add);
        cursor.time+=(float)(60*min_to_add+sec_to_add);
    }
    public Scrollbar speedbar;
    public TextMeshProUGUI speed_ui;
    public void speed(){
        time_speed=(float) speedbar.value*5;
        experiment_video.playbackSpeed=time_speed;
        speed_ui.text="Speed: "+time_speed;
    }
    public void speed(float sp){
        experiment_video.playbackSpeed=sp;
    }

    // section variable et fonction pour lecture du csv
    List<string> timestamp = new List<string>(); //time
    List<string> sensor = new List<string>();    //sensor name
                                                 //sensor type unused
    List<string> state = new List<string>();    //value : ON/OFF ; OPEN/CLOSED
    List<string> activity = new List<string>();  //activity label
    List<string> prediction = new List<string>();//prediction
    List<string> prediction_timestamp = new List<string>();// prediction timestamp il peut être different du csv
                                                           // par ex si on ne garde que les changement de prediction ( t1:bath t2:bath t3:bath t4:cook t5:cook => t1:bath t4:cook)
    string start_time;
    double time;

    public double time_to_double(string s_time){
        string no_date=s_time.Split(' ')[1];
        //Debug.Log(no_date);
        return 3600*Convert.ToDouble(no_date.Split(":")[0])+60*Convert.ToDouble(no_date.Split(":")[1])+Convert.ToDouble(no_date.Split(":")[2].Replace(".",","));
    }
    
    int the_value(string on_off){
        switch(on_off){
            case "ON":
            case "OPEN":
                return 1;
            case "OFF":
            case "CLOSE":
                return 0;
        }
        return 0;
    }
    public void activate(string sensor,int etat){
        switch(sensor){
            //0: area section
            case "floor_entrance":
                lab_icons.change_sensor(0,1,etat);
                break;
            case "floor_bedroom":
                lab_icons.change_sensor(0,2,etat);
                break;
            case "floor_kitchen":
                lab_icons.change_sensor(0,3,etat);
                break;
            case "floor_bathroom":
                lab_icons.change_sensor(0,4,etat);
                break;
            case "floor_dining_room":
                lab_icons.change_sensor(0,5,etat);
                break;
            case "floor_livingroom":
                lab_icons.change_sensor(0,6,etat);
                break;
            
            //1: door section (and drawer)
            case "door_entrance":
                lab_icons.change_sensor(1,0,etat);
                break;
            case "door_cabinet_kitchen":
                lab_icons.change_sensor(1,1,etat);
                break;
            case "door_fridge":
                lab_icons.change_sensor(1,2,etat);
                break;
            case "door_cabinet_bedroom":
                lab_icons.change_sensor(1,3,etat);
                break;
            case "drawer_utensils_kitchen":
                lab_icons.change_sensor(1,4,etat);
                break;
            case "drawer_baking_tools_kitchen":
                lab_icons.change_sensor(1,5,etat);
                break;
            case "drawer_tableware_kitchen":
                lab_icons.change_sensor(1,6,etat);
                break;
            
            
            //2: power section
            case "television":
                lab_icons.change_sensor(2,0,etat);
                break;
            case "oven":
                lab_icons.change_sensor(2,1,etat);
                break;
            
            //3: light section
            case "light_entrance":
                lab_icons.change_sensor(3,0,etat);
                break;
            case "light_bedroom":
                lab_icons.change_sensor(3,1,etat);
                break;
            case "light_kitchen":
                lab_icons.change_sensor(3,2,etat);
                break;
            case "light_bathroom":
                lab_icons.change_sensor(3,3,etat);
                break;
            case "light_dining_room":
                lab_icons.change_sensor(3,4,etat);
                break;
            case "light_livingroom":
                lab_icons.change_sensor(3,5,etat);
                break;

            //4: button section
            case "light_switch_bathroom":
                lab_icons.change_sensor(4,0,etat);
                break;
            case "wc_flush":
                lab_icons.change_sensor(4,1,etat);
                break;

            //5:flow
            case "tap_kitchen":
                lab_icons.change_sensor(5,0,etat);
                break;
            case "tap_batheroom":
                lab_icons.change_sensor(5,1,etat);
                break;
            
            //6: detected
            case "presure_bed":
                lab_icons.change_sensor(6,0,etat);
                break;
            case "presure_armchair":
                lab_icons.change_sensor(6,1,etat);
                break;
        }
    }

    

    // Start is called before the first frame update
    void Start()
    { 
        input_path=PlayerPrefs.GetString("input_path",Application.streamingAssetsPath);
        output_path=PlayerPrefs.GetString("output_path",Application.streamingAssetsPath); 
        //Debug.Log("input path : "+input_path);      
        
        if (record_clone==1){
            init_clone();
        }else{
            init();
        }
        
    }

    void init(){
        csv_pointeur=0;
        csv_prediction_pointeur=0;
        playing=false;
        speed();
        char[] separators = {',','\t'};// attention au separator csv pas visible quand on ouvre simplement le csv
        s_num = PlayerPrefs.GetInt("subject",5);
        e_num = PlayerPrefs.GetInt("exp",21);
        Debug.Log("s :"+s_num+" Ep :"+e_num);
        //camera_visu.path_name="s"+s_num+"_Ep"+e_num;

        string[] le_csv =Directory.GetFiles(Path.Combine(input_path,"csv_files"),"s"+s_num+"_Ep"+e_num+".csv",SearchOption.AllDirectories);        
        string[] la_video =Directory.GetFiles(Path.Combine(input_path,"videos"),"s"+s_num+"_Ep"+e_num+"_salon*.mp4",SearchOption.AllDirectories); 
        string[] la_video_time =Directory.GetFiles(Path.Combine(input_path,"videos_time_start"),"s"+s_num+"_Ep"+e_num+"_salon_capture-000_firstframe_time*.jpg",SearchOption.AllDirectories); 
        string[] le_csv_prediction =Directory.GetFiles(Path.Combine(input_path,"csv_prediction_files"),"s"+s_num+"_Ep"+e_num+"prediction.csv",SearchOption.AllDirectories);
        
        if(la_video.Length>0){
            experiment_video.url=la_video[0];
            experiment_video.Play();//play/pause pour montrer la premiere frame
            experiment_video.Pause();
        }
        file_name.text=le_csv[0];
        
        using(StreamReader reader = new StreamReader(le_csv[0]))
        {
            //var header = reader.ReadLine(); //si on besoin de sauter la premiere ligne
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                string[] values = line.Split(separators);
                //Debug.Log("la ligne obtenue:"+line);
                
                
                timestamp.Add(values[0]);
                sensor.Add(values[1]);
                //
                state.Add(values[3]);
                activity.Add(values[4]);
                if (values.Length>5){
                    prediction.Add(values[5]);
                }else{
                    prediction.Add("NONE");
                }
            }
        }
        if (le_csv_prediction.Length>0){
            //Debug.Log("found prediction for :"+le_csv_prediction[0])
            using(StreamReader reader = new StreamReader(le_csv_prediction[0]))
                {   
                    //var header = reader.ReadLine(); //we skip the first line containing names of columns
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        string[] values = line.Split(separators);
                        prediction_timestamp.Add(values[0]);
                        prediction.Add(values[1]);
                    }
                } 
        }else{prediction_timestamp.Add("date 0:0:0");
            prediction.Add("NONE");
        }
        taille_csv=timestamp.Count;
        taille_csv_prediction=prediction_timestamp.Count;
        start_time=timestamp[0]; //we start the time at the first timestamps (= 3rd xlaab.basicstate)
        time=time_to_double(start_time);
    }
    //appelé pour initialiser les instances qui ne serve qu'à record une visu
    void init_clone(){
        csv_pointeur=0;
        csv_prediction_pointeur=0;
        //playing=false;//true; on laisse le multi record gerer l'activation des clones

        experiment_video.gameObject.SetActive(false); // pas besoin d'afficher la video pour enregistrer la visualisation
        //recorder.outputFilePath="Assets/Recordings/s"+s_num+"_Ep"+e_num+".avi";
        
        //speed();
        //time_speed=1; // on enregistre toujours a la vitesse 1
        char[] separators = {',','\t'};// attention au separator csv pas visible quand on ouvre simplement le csv

        
        string[] le_csv =Directory.GetFiles(Path.Combine(input_path,"csv_files"),"s"+s_num+"_Ep"+e_num+".csv",SearchOption.AllDirectories);
        string[] la_video_time =Directory.GetFiles(Path.Combine(input_path,"videos_time_start"),"s"+s_num+"_Ep"+e_num+"_salon_capture-000_firstframe_time*.jpg",SearchOption.AllDirectories);
        string[] le_csv_prediction =Directory.GetFiles(Path.Combine(input_path,"csv_prediction_files"),"s"+s_num+"_Ep"+e_num+"prediction.csv",SearchOption.AllDirectories);
        
        file_name.text=le_csv[0];
        
        using(StreamReader reader = new StreamReader(le_csv[0]))
        {   
            //var header = reader.ReadLine(); //we skip the first line containing names of columns
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                string[] values = line.Split(separators);
                
                timestamp.Add(values[0]);
                sensor.Add(values[1]);
                //
                state.Add(values[3]);
                activity.Add(values[4]);
            }
        }
        if (le_csv_prediction.Length>0){
            //Debug.Log("found prediction for :"+le_csv_prediction[0])
            using(StreamReader reader = new StreamReader(le_csv_prediction[0]))
                {   
                    //var header = reader.ReadLine(); //we skip the first line containing names of columns
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        string[] values = line.Split(separators);
                        prediction_timestamp.Add(values[0]);
                        prediction.Add(values[1]);
                    }
                } 
        }else{prediction_timestamp.Add("date 0:0:0");
            prediction.Add("NONE");
        }

        taille_csv=timestamp.Count;
        taille_csv_prediction=prediction_timestamp.Count;
        if(la_video_time.Length==0){ //if we don't have a start time (usually csv with no csv associated)
            start_time=timestamp[0]; //we start the time at the first timestamps 
        }else{
            start_time="date "+(la_video_time[0].Split("firstframe_time_")[1].Split(".jpg")[0].Replace("-",":")); // we had bits to have the same format
        }
        
        Debug.Log("Start time is "+start_time.Split(" ")[1].Split(".")[0].Replace(":","_"));
        //master.visu_firstframe_time_list.Add(start_time.Split(" ")[1].Split(".")[0].Replace(":","_"));
        time=time_to_double(start_time);
        
    }

    public TextMeshProUGUI clock;
    public TextMeshProUGUI clock_bis;
    public TextMeshProUGUI activity_txt;
    public TextMeshProUGUI activity_txt_bis;
    public TextMeshProUGUI prediction_txt;
    public TextMeshProUGUI file_name;
    

    // Update is called once per frame
    void Update()
    {
        if(playing){
            time+=Time.deltaTime*time_speed;// fixer time speed à 1 pour test
            string clock_txt="timex"+time_speed+": "+Convert.ToInt32((time%3600-time%60)/60)+":"+Convert.ToInt32(time%60-time%1);
            clock.text=clock_txt;
            clock_bis.text=clock_txt;
            //Debug.Log("Activity: "+activity[csv_pointeur]);
            activity_txt.text="Activity: "+activity[csv_pointeur];
            activity_txt_bis.text="Activity: "+activity[csv_pointeur];
            prediction_txt.text="Prediction: "+prediction[csv_prediction_pointeur];
            
            int debut_traitement=csv_pointeur;
            while (csv_pointeur<taille_csv && time_to_double(timestamp[csv_pointeur])<time ){
                csv_pointeur++;
            }
            while (csv_prediction_pointeur <taille_csv_prediction-1 && time_to_double(prediction_timestamp[csv_prediction_pointeur])<time ){
                csv_prediction_pointeur++;
            }
            for (int i=debut_traitement;i<csv_pointeur;i++){
                activate(sensor[i],the_value(state[i]));
            }
        }
        
        if (csv_pointeur==taille_csv){ // on a fini de parcourir le csv
            playing=false;
            csv_pointeur+=1;
            if (record_clone==1)
            {master.record_finished();
            recorder.end_record();}
            
        }
        
    }
    




}
