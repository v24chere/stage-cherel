using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using TMPro;

//script appelé par un global controlleur pour gerer l'activation des icones capteurs ainsi que des sols pour les capteurs de presences
public class lab_icon_controller : MonoBehaviour
{
    
    public List<GameObject> areas = new List<GameObject>();  //0
    public List<sensor> sensor_door = new List<sensor>();    //1
    public List<sensor> sensor_power = new List<sensor>();   //2
    public List<sensor> sensor_light = new List<sensor>();   //3
    public List<sensor> sensor_button = new List<sensor>();  //4
    public List<sensor> sensor_flow = new List<sensor>();    //5
    public List<sensor> sensor_detected = new List<sensor>();//6

    

    public void change_mode(int i){//0: normal ; 1: shadow ; 2: text; 3:shadow+text
        foreach(sensor s_ic in sensor_door){
            s_ic.mode=i;
        }
        foreach(sensor s_ic in sensor_power){
            s_ic.mode=i;
        }
        foreach(sensor s_ic in sensor_light){
            s_ic.mode=i;
        }
        foreach(sensor s_ic in sensor_button){
            s_ic.mode=i;
        }
        foreach(sensor s_ic in sensor_flow){
            s_ic.mode=i;
        }
        foreach(sensor s_ic in sensor_detected){
            s_ic.mode=i;
        }
    }

    public void change_area(int area,bool on_off){
        areas[area].SetActive(on_off);
    }
    public void change_sensor(int sensor_list_num,int sensor,int etat){
        switch(sensor_list_num){
            case 0:
                change_area(sensor,etat==1);
                break;
            case 1:
                change_sensor(sensor_door,sensor,etat);
                break;
            case 2:
                change_sensor(sensor_power,sensor,etat);
                break;
            case 3:
                change_sensor(sensor_light,sensor,etat);
                break;
            case 4:
                change_sensor(sensor_button,sensor,etat);
                break;
            case 5:
                change_sensor(sensor_flow,sensor,etat);
                break;
            case 6:
                change_sensor(sensor_detected,sensor,etat);
                break;
        }
    }
    public void change_sensor(List<sensor> sensor_list,int sensor,int etat){
        sensor_list[sensor].etat=etat;
    }


    // Start is called before the first frame update
    void Start()
    {
        for (int i=1;i<=6;i++){
            change_area(i,false);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
