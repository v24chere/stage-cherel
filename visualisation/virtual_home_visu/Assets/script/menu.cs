using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Video;

public class menu : MonoBehaviour
{
    public TextMeshProUGUI found;
    public TextMeshProUGUI explanation_path_text;
    
    int mode_one_subject;
    int mode_record;
    //scroll bar S choice
    int s_num=1;
    public TextMeshProUGUI s_txt;
    public Scrollbar s_bar;
    public void s_choice(){
        s_num=(int)(s_bar.value*9) +1;
        s_txt.text="Subject n°"+s_num;
        if (mode_one_subject != 0){
            on_mode_one_subject(s_num);
        }
        update_found();
    }
    //

    // scroll bar Ep choice
    int e_num=1;
    public TextMeshProUGUI e_txt;
    public Scrollbar e_bar;
    public void e_choice(){
        e_num=(int)(e_bar.value*30) +1;
        e_txt.text="Exp. n°"+e_num;
        update_found();
    }
    void update_found(){
        string path_csv;
        string path_v;
        //UnityEngine.Debug.Log("update_found with s"+s_num+" and Ep"+e_num);
        string[] le_csv =Directory.GetFiles(Path.Combine(streaming_path,"csv_files"),"s"+s_num+"_Ep"+e_num+".csv",SearchOption.AllDirectories);
        string[] la_video =Directory.GetFiles(Path.Combine(streaming_path,"videos"),"s"+s_num+"_Ep"+e_num+"_salon*",SearchOption.AllDirectories);
        if (le_csv.Length == 0){
            path_csv="Not Found";
        }else{path_csv=Path.GetFileName(le_csv[0]);}
        if (la_video.Length == 0){
            path_v="Not Found";
        }else{path_v=Path.GetFileName(la_video[0]);}
        found.text="Csv: "+path_csv+"\n Video: "+path_v;
        PlayerPrefs.SetInt("subject",s_num);
        PlayerPrefs.SetInt("exp",e_num);
    }
    

    public void launch(){
        UnityEngine.Debug.Log("launch with s"+s_num+" and Ep"+e_num);
        SceneManager.LoadScene("lab");            
    }
    public void quit(){
        Application.Quit();
    }
    public void on_mode_record(int i){
        PlayerPrefs.SetInt("mode_record",i);
        mode_record=i;
        update_found();
    }
    public void on_mode_one_subject(int i){
        int j=i;
        if (j!=0){
            j=s_num;
        }
        PlayerPrefs.SetInt("mode_one_subject",i);
        mode_one_subject=i;
        update_found();
    }

    public void transfer(DirectoryInfo source,DirectoryInfo target){
        foreach (DirectoryInfo dir in source.GetDirectories())
            {transfer(dir, target.CreateSubdirectory(dir.Name));}

        foreach (FileInfo file in source.GetFiles())
            {file.CopyTo(Path.Combine(target.FullName, file.Name));}
    }
    public void open_pers_path(){
        string path = Application.streamingAssetsPath;//Application.persistentDataPath;
        #if UNITY_EDITOR
        // Ouvre l'explorateur de fichiers dans l'éditeur
            UnityEditor.EditorUtility.RevealInFinder(path);
        #elif UNITY_STANDALONE_WIN
        // Ouvre l'explorateur de fichiers sur Windows
            Process.Start("explorer.exe", path.Replace("/", "\\"));
        #endif
        //
    }
    
    string persistent_path;
    string streaming_path;
    string input_path;

//
    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 30;
        persistent_path = Application.persistentDataPath;           // les données de Streaming Assets persiste dans le build on les utilise pour
        UnityEngine.Debug.Log("input/outputs created in :"+persistent_path);           // former un dossier dans persistent path 
        streaming_path= Application.streamingAssetsPath;
        PlayerPrefs.SetString("input_path",streaming_path);
        
        PlayerPrefs.SetString("output_path",streaming_path);
            
        
        explanation_path_text.text="base data can be found in"+streaming_path+"\n you can add your own, follow the readme instruction there.";

        //mode_one_subject=0;
        //PlayerPrefs.SetInt("mode_record",0);
        //PlayerPrefs.SetInt("mode_one_subject",0);
        update_found();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
