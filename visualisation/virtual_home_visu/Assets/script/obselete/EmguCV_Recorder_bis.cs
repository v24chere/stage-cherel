using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System.Drawing;
using System.Runtime.InteropServices;


public class EmguCV_Recorder_bis : MonoBehaviour
{
    public Camera cameraToRecord;
    public string outputFilePath = "Assets/output.avi";
    public int frameWidth = 1920;
    public int frameHeight = 1080;
    public int frameRate = 30;

    private VideoWriter videoWriter;
    private RenderTexture renderTexture;
    private Texture2D screenTexture;

    void Start()
    {
        videoWriter = new VideoWriter(outputFilePath, VideoWriter.Fourcc('M', 'J', 'P', 'G'), frameRate, new Size(frameWidth, frameHeight), true);
        // Initialize VideoWriter pour mp4 pas installer 
        //videoWriter = new VideoWriter(outputFilePath, VideoWriter.Fourcc('X', '2', '6', '4'), frameRate, new Size(frameWidth, frameHeight), true);

        if (!videoWriter.IsOpened)
        {
            Debug.LogError("Failed to open VideoWriter.");
        }

        // Create a RenderTexture and a Texture2D for capturing camera output
        renderTexture = new RenderTexture(frameWidth, frameHeight, 24);
        screenTexture = new Texture2D(frameWidth, frameHeight, TextureFormat.RGB24, false);
    }

    void Update()
    {
        if (videoWriter != null && videoWriter.IsOpened)
        {
            // Capture the camera output to the RenderTexture
            cameraToRecord.targetTexture = renderTexture;
            cameraToRecord.Render();
            cameraToRecord.targetTexture = null;

            // Read the RenderTexture into the Texture2D
            RenderTexture.active = renderTexture;
            screenTexture.ReadPixels(new Rect(0, 0, frameWidth, frameHeight), 0, 0);
            screenTexture.Apply();
            RenderTexture.active = null;

            // Get the raw texture data
            Color32[] pixelData = screenTexture.GetPixels32();
            GCHandle handle = GCHandle.Alloc(pixelData, GCHandleType.Pinned);

            // Create an OpenCV Mat from the raw data
            using (Mat frame = new Mat(frameHeight, frameWidth, DepthType.Cv8U, 3, handle.AddrOfPinnedObject(), frameWidth * 3))
            {
                // Flip the image vertically
                CvInvoke.Flip(frame, frame, FlipType.Vertical);

                // Convert BGR to RGB
                CvInvoke.CvtColor(frame, frame, ColorConversion.Bgr2Rgb);

                // Write the frame to the video
                videoWriter.Write(frame);
            }

            handle.Free();
        }
    }
    public void end_record(){
        OnApplicationQuit();
    }
    void OnApplicationQuit()
    {
        if (videoWriter != null)
        {
            videoWriter.Dispose();
        }
    }
}
