using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Video;

public class global_controller_obs : MonoBehaviour
{
    int mode_record;
    int mode_one_subject;
    List<int> s_list = new List<int>();
    List<int> e_list = new List<int>();

    int go_through;
    public VideoPlayer experiment_video;
    public lab_icon_controller lab_icons;
    
    int csv_pointeur;
    int taille_csv;
    public float time_speed;
    bool playing;
    float temp_time;

    public void back_menu(){
        SceneManager.LoadScene("menu"); 
    }
    public void play(){
        experiment_video.Play();
        playing=true;
        
    }
    public void pause(){
        experiment_video.Pause();
        playing=false;
    }
    public void mute(){
        experiment_video.SetDirectAudioMute(0,true);
    }
    public void unmute(){
        experiment_video.SetDirectAudioMute(0,false);
    }

    public Scrollbar speedbar;
    public TextMeshProUGUI speed_ui;
    public void speed(){
        time_speed=(float) speedbar.value*5;
        experiment_video.playbackSpeed=time_speed;
        speed_ui.text="Speed: "+time_speed;
    }
    public void speed(float sp){
        experiment_video.playbackSpeed=sp;
    }

    List<string> timestamp = new List<string>(); //time
    List<string> sensor = new List<string>();    //sensor name
                                                 //sensor type unused
    List<string> state = new List<string>();    //value : ON/OFF ; OPEN/CLOSED
    List<string> activity = new List<string>();  //activity label
    List<string> prediction = new List<string>();//prediction

    string start_time;
    double time;

    public double time_to_double(string s_time){
        string no_date=s_time.Split(' ')[1];
        //Debug.Log(no_date);
        
        return 3600*Convert.ToDouble(no_date.Split(":")[0])+60*Convert.ToDouble(no_date.Split(":")[1])+Convert.ToDouble(no_date.Split(":")[2].Replace(".",","));
    }
    

    int the_value(string on_off){
        switch(on_off){
            case "ON":
            case "OPEN":
                return 1;
            case "OFF":
            case "CLOSE":
                return 0;
        }
        return 0;
    }
    public void activate(string sensor,int etat){
        switch(sensor){
            //0: area section
            case "floor_entrance":
                lab_icons.change_sensor(0,1,etat);
                break;
            case "floor_bedroom":
                lab_icons.change_sensor(0,2,etat);
                break;
            case "floor_kitchen":
                lab_icons.change_sensor(0,3,etat);
                break;
            case "floor_bathroom":
                lab_icons.change_sensor(0,4,etat);
                break;
            case "floor_dining_room":
                lab_icons.change_sensor(0,5,etat);
                break;
            case "floor_livingroom":
                lab_icons.change_sensor(0,6,etat);
                break;
            
            //1: door section (and drawer)
            case "door_entrance":
                lab_icons.change_sensor(1,0,etat);
                break;
            case "door_cabinet_kitchen":
                lab_icons.change_sensor(1,1,etat);
                break;
            case "door_fridge":
                lab_icons.change_sensor(1,2,etat);
                break;
            case "door_cabinet_bedroom":
                lab_icons.change_sensor(1,3,etat);
                break;
            case "drawer_utensils_kitchen":
                lab_icons.change_sensor(1,4,etat);
                break;
            case "drawer_baking_tools_kitchen":
                lab_icons.change_sensor(1,5,etat);
                break;
            case "drawer_tableware_kitchen":
                lab_icons.change_sensor(1,6,etat);
                break;
            
            
            //2: power section
            case "television":
                lab_icons.change_sensor(2,0,etat);
                break;
            case "oven":
                lab_icons.change_sensor(2,1,etat);
                break;
            
            //3: light section
            case "light_entrance":
                lab_icons.change_sensor(3,0,etat);
                break;
            case "light_bedroom":
                lab_icons.change_sensor(3,1,etat);
                break;
            case "light_kitchen":
                lab_icons.change_sensor(3,2,etat);
                break;
            case "light_bathroom":
                lab_icons.change_sensor(3,3,etat);
                break;
            case "light_dining_room":
                lab_icons.change_sensor(3,4,etat);
                break;
            case "light_livingroom":
                lab_icons.change_sensor(3,5,etat);
                break;

            //4: button section
            case "light_switch_bathroom":
                lab_icons.change_sensor(4,0,etat);
                break;
            case "wc_flush":
                lab_icons.change_sensor(4,1,etat);
                break;

            //5:flow
            case "tap_kitchen":
                lab_icons.change_sensor(5,0,etat);
                break;
            case "tap_batheroom":
                lab_icons.change_sensor(5,1,etat);
                break;
            
            //6: detected
            case "presure_bed":
                lab_icons.change_sensor(6,0,etat);
                break;
            case "presure_armchair":
                lab_icons.change_sensor(6,1,etat);
                break;
        }
    }

    int min_to_add=0;
    int sec_to_add=0;
    public TextMeshProUGUI min_txt;
    public TextMeshProUGUI sec_txt;

    public void change_min(int i){
        min_to_add+=i;
        min_txt.text=min_to_add.ToString();
    }
    public void change_sec(int i){
        sec_to_add+=i;
        sec_txt.text=sec_to_add.ToString();
    }
    public void change_sync(){
        experiment_video.time+=(float)(60*min_to_add+sec_to_add);
    }
    public void change_global(){
        experiment_video.time+=(float)(60*min_to_add+sec_to_add);
        time+=(float)(60*min_to_add+sec_to_add);
    }

    // Start is called before the first frame update
    void Start()
    {
        go_through=0;
        string[] le_csv;
        string[] la_video;
        mode_record=PlayerPrefs.GetInt("mode_record",0);
        mode_one_subject=PlayerPrefs.GetInt("mode_one_subject",0);
        if (mode_record==1){
            string mos="*";
            if (mode_one_subject!=0){
                mos=Convert.ToString(mode_one_subject);}
            le_csv =Directory.GetFiles("Assets/csv_files","s"+mos+"_Ep*.csv",SearchOption.AllDirectories);
            la_video =Directory.GetFiles("Assets/video","s"+mos+"_Ep*_salon*.mp4",SearchOption.AllDirectories);
            foreach (string video_path in la_video){
                Debug.Log("test de "+video_path);
                string[] begin=video_path.Split("_salon")[0].Split("s");
                string core=begin[begin.Length-1]; // là on a *_Ep* normalement
                string[] nums=core.Split("_Ep");
                int vs_num=Convert.ToInt32(nums[0]);
                int ve_num=Convert.ToInt32(nums[1]);
                foreach (string csv_path in le_csv){
                    if (csv_path.EndsWith("s"+vs_num+"_Ep"+ve_num+".csv")){
                        s_list.Add(vs_num);
                        e_list.Add(ve_num);
                        Debug.Log("ajout de s"+vs_num+"_Ep"+ve_num);
                        break;
                    }
                }
            }
            PlayerPrefs.SetInt("subject",s_list[go_through]);
            PlayerPrefs.SetInt("exp",e_list[go_through]);
        }
        init();
    }
    void init(){
        csv_pointeur=0;
        playing=false;
        speed();
        char[] separators = {',','\t'};// attention au separator csv pas visible quand on ouvre simplement le csv
        int s_num = PlayerPrefs.GetInt("subject",1);
        int e_num = PlayerPrefs.GetInt("exp",1);

        
        string[] le_csv =Directory.GetFiles("Assets/csv_files","s"+s_num+"_Ep"+e_num+".csv",SearchOption.AllDirectories);
        
        string[] la_video =Directory.GetFiles("Assets/video","s"+s_num+"_Ep"+e_num+"_salon*.mp4",SearchOption.AllDirectories);
        
        experiment_video.url=la_video[0];



        experiment_video.Play();
        experiment_video.Pause();
        file_name.text=le_csv[0];
        
        using(StreamReader reader = new StreamReader(le_csv[0]))
        {
            
            //var header = reader.ReadLine(); //we skip the first line containing names of columns
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                string[] values = line.Split(separators);
                //Debug.Log("la ligne obtenue:"+line);
                
                
                timestamp.Add(values[0]);
                sensor.Add(values[1]);
                //
                state.Add(values[3]);
                activity.Add(values[4]);
                if (values.Length>5){
                    prediction.Add(values[5]);
                }else{
                    prediction.Add("NONE");
                }
            }
        }
        taille_csv=timestamp.Count;
        start_time=timestamp[0]; //we start the time at the first timestamps
        time=time_to_double(start_time)+0.5;
    }

    public TextMeshProUGUI clock;
    public TextMeshProUGUI clock_bis;
    public TextMeshProUGUI activity_txt;
    public TextMeshProUGUI activity_txt_bis;
    public TextMeshProUGUI prediction_txt;
    public TextMeshProUGUI file_name;

    // Update is called once per frame
    void Update()
    {
        if(playing){
            time+=Time.deltaTime*time_speed;
        }
        string clock_txt="time: "+Convert.ToInt32((time%3600-time%60)/60)+":"+Convert.ToInt32(time%60-time%1);
        clock.text=clock_txt;
        clock_bis.text=clock_txt;
        //Debug.Log("Activity: "+activity[csv_pointeur]);
        activity_txt.text="Activity: "+activity[csv_pointeur];
        activity_txt_bis.text="Activity: "+activity[csv_pointeur];
        //prediction_txt.text="Prediction: "+prediction[csv_pointeur]
        int debut_traitement=csv_pointeur;
        while (csv_pointeur<taille_csv && time_to_double(timestamp[csv_pointeur])<time ){
            csv_pointeur++;
        }
        for (int i=debut_traitement;i<csv_pointeur;i++){
            activate(sensor[i],the_value(state[i]));
        }
        if (csv_pointeur==taille_csv && mode_record==1){//on enchaine
            go_through+=1;
            PlayerPrefs.SetInt("subject",s_list[go_through]);
            PlayerPrefs.SetInt("exp",e_list[go_through]);
            init();
            play();
        }
    }
    




}
