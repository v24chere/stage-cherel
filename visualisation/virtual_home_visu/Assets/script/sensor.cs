using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Video;

// script pour les icones des capteurs on peut avoir un mode ou seul les capteurs activées sont visibles
// egalement un texte open/close : on/off eventuellement 
// fonctionnalité pas exploitée; le illuminé pour actif / sombre pour inactif a été jugée suffisant
public class sensor : MonoBehaviour
{
    public GameObject shadow;
    public TextMeshProUGUI popup_txt_s;
    public TextMeshProUGUI popup_txt;

    [HideInInspector]public int mode; //0: normal ; 1: shadow ; 2: text; 3:shadow+text
    [HideInInspector]public int etat;

    // Start is called before the first frame update
    void Start()
    {
        etat=0;
        mode=1;
        popup_txt_s.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (etat==0){
            this.GetComponent<Image>().color=Color.clear;
            if(mode==1 || mode==3){
                shadow.SetActive(true);
            }
            //if(mode==3){popup_txt_s.gameObject.SetActive(true);}
            popup_txt.gameObject.SetActive(false);
        }else{
            this.GetComponent<Image>().color=Color.white;
            shadow.gameObject.SetActive(false);
        }
    }
}
